C       Subroutine RESSD   : Created 10/01/82 ; last update 04/05/10/
C       *--------------------------------------------------------------*
C       Author    : Philipp Bucher
C       Function  : Input OF SIGNAL SEARCH PARAMETERS FROM SSD FILE       
C       Function  : Input of signal search parameters from SSD file
C       *--------------------------------------------------------------*
C                                                                        
        SUBROUTINE RESSD(NSSD,FISSD,NB1,NB2,MI,CTRC,TIT,CCTY            
     A,LS,LE,I,IRAN,LC,LD,LF,LH,NI,PR,FISSC,IRC)                          
                                                                        
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        CHARACTER*50 TIT                                                
        Real         RN
                                                                        
    1   READ(NSSD,4000,END=901,ERR=902) RC80                            
           IF(RC80(1:2).NE.'F ') GO TO  1                               
        READ(RC80,4001,ERR=903) FISSD,NB1,NB2,MI,CTRC,TIT               
                                                                        
    2   READ(NSSD,4000,END=904,ERR=905) RC80                            
           IF(RC80(1:2).NE.'S ') GO TO  2                               
        READ(RC80,4002,ERR=906) CCTY,LS,LE,I,RN,LC,LD,LF,LH,NI,PR       
        NB2=NB1+(NB2-NB1-LC+1)/LD*LD+LC-1                               
                                                                        
        IF(CCTY.NE.'U') GO TO 100                                       
    3   READ(NSSD,4000,END=907,ERR=908) RC80                            
           IF(RC80(1:2).NE.'C ') GO TO  3                               
        READ(RC80,4003,ERR=909) FISSC                                   
        BACKSPACE(NSSD,ERR=910)                                         
                                                                        
  100   RETURN                                                          
                                                                        
  910   IRC=IRC+1                                                       
  909   IRC=IRC+1                                                       
  908   IRC=IRC+1                                                       
  907   IRC=IRC+1                                                       
  906   IRC=IRC+1                                                       
  905   IRC=IRC+1                                                       
  904   IRC=IRC+1                                                       
  903   IRC=IRC+1                                                       
  902   IRC=IRC+1                                                       
  901   IRC=IRC+1                                                       
        GO TO 100                                                       
                                                                        
 4000   FORMAT(A80)                                                     
 4001   FORMAT(2X,A7,3X,I6,I6,I6,1X,A1,1X,A47)                        
 4002   FORMAT(2X,A1,3X,I3,3X,I3,3X,I6,3X,F5.4,3X,I5,3X,I5,3X,I3,3X,I3    
     A,3X,I6,3X,F6.4)                                                   
 4003   FORMAT(2X,A7)                                                 
                                                                        
        END                                                             
