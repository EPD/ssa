* Program rmxx, removes redundant XX lines from EPD flat 
* file. This program is used during EPD conversion from 
* new to old format.

* input 

        Parameter    (NINP=   5)  
        Character*80  RCIN

* output

        Character*80  RCEX

* extra 

* open files


* modify

        RCEX=' '
        NR=0
   10   Read(NINP,'(A)',End=100) RCIN
        L=Lblnk(RCIN)
        If(RCIN(1:2).EQ.'XX') then 
           If(RCEX(1:2).EQ.'XX'.OR.
     *        RCEX(1:2).EQ.'//') go to  10
        End if
        Write(6,'(80A)')(RCIN(ii1:ii1),ii1=1,L)
        RCEX=RCIN
        Go to  10

  100   Stop
	End
