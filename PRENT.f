        Program PRENT
*----------------------------------------------------------------------*
*       Prints sequence entries from a nucleotide or amino acid sequence
*       database.
*----------------------------------------------------------------------*
        Parameter        (NPTR=11)
        Parameter        (NENT=12)
        Parameter        (NERR= 0)
C       Parameter        (NERR= 7)

        Character*64      ARG
        Character*01      COPT
 
        Character*64      FDBE
        Character*64      SQID
        Character*64      FNAM
        Logical           LPTR

        Logical           LOCSQ
        Character*64      FSEQ
        Character*64      FDOC
        Character*01      FMT
        Integer           RCL
        Character         CC

        Integer*8         LSEQ
        Integer*8         IBS
        Integer*8         IBD
C 	external          Fseek

*----------------------------------------------------------------------*

        COPT=' '
        N1=iargc()
        If(N1.EQ.0) go to 901
         
        Do  99 I1=1,N1
           Call Getarg(I1,ARG)
           If(ARG(1:1).NE.'-') go to  10 
           
           If   (ARG(1:2).EQ.'-l'
     *       .OR.ARG(1:2).EQ.'-L'
     *       .OR.ARG(1:2).EQ.'-u'
     *       .OR.ARG(1:2).EQ.'-U') COPT=ARG(2:2) 

   10      Call PSQID(NPTR,ARG,LPTR,FNAM,SQID,IRC)

           If(.NOT.LPTR) go to  99

           If(.NOT.LOCSQ
     *       (NPTR,FNAM,SQID,LSEQ,FMT,FSEQ,FDOC,RCL,IBS,IBD,CC))
     *        go to  99
C          Print *,IBS,IBD

           If     (FMT.EQ.'E') then
              Open(NENT,File=FDOC,Status='OLD',Err=902)
              Call FSeek(NENT,IBD,0)
              Call PRSQE(NENT,COPT)
           Else if(FMT.EQ.'F') then  
              Open(NENT,File=FDOC,Status='OLD',Err=902)
              Call FSeek(NENT,IBD,0)
              Call PRSQF(NENT,COPT)
           Else
              L=Lblnk(FDBE)
              Write(NERR,*)
     *       'Invalid format ',FMT,' of entry ',SQID(1:L),'.'                 
           End if

   99   Continue

  100   Stop

  901   Write(NERR,*) 'Usage:    PRENT [-[lLuU] seq_entry_ref ...'
        Write(NERR,*) 'Options:  -l sequence lower case'
        Write(NERR,*) '          -L everything lower case'
        Write(NERR,*) '          -u sequence upper case' 
        Write(NERR,*) '          -U everything upper case'
        Go to 100
 
  902   Call Perror('[PRENT]:')
        Go to  100
        End
************************************************************************
        Subroutine PRSQE(NENT,COPT)

        Character*01      COPT

        Character*80      RCIN
           Byte           RBIN(80)
           Equivalence   (RCIN,RBIN)
     
        Logical           SEQ

        SEQ=.FALSE.

    1   Read(NENT,"(A)",Err=901,END=901) RCIN
        
        L1=1
        L2=lblnk(RCIN)
        If(RBIN(L1).EQ.13) L1=2
        If(RBIN(L2).EQ.13) L2=L2-1

        If(COPT.EQ.' ') go to  20
        If(COPT.EQ.'L') Call STRLO(RBIN,L1,L2) 
        If(COPT.EQ.'U') Call STRUP(RBIN,L1,L2) 
        If(.NOT.SEQ) go to  20
        If(COPT.EQ.'l') Call STRLO(RBIN,L1,L2) 
        If(COPT.EQ.'u') Call STRUP(RBIN,L1,L2) 
        Go to  20 
       
   20   Write(6,'(A)') RCIN(L1:L2)
        If(RCIN(L1:L1+1).EQ.'SQ') SEQ=.TRUE. 
        If(RCIN(L1:L1+1).NE.'//') go to   1 
        
  100   Return              

  901   Call Perror('[PRENT]:')
        Go to  100
        End
************************************************************************
        Subroutine PRSQF(NENT,COPT)

        Character*01      COPT

        Character*80      RCIN
        Byte              RBIN(80)
        Equivalence      (RCIN,RBIN)

        Read(NENT,"(A)",Err=901,END=100) RCIN
        L1=1
        L2=lblnk(RCIN)
        If(RBIN(L1).EQ.13) L1=2
        If(RBIN(L2).EQ.13) L2=L2-1
        If(COPT.EQ.'L') Call STRLO(RBIN,L1,L2) 
        If(COPT.EQ.'U') Call STRUP(RBIN,L1,L2) 
        Write(6,'(A)') RCIN(L1:L2)
        
    1   Read(NENT,"(A)",Err=901,END=100) RCIN
        L1=1
        L2=lblnk(RCIN)
        If(RBIN(L1).EQ.13) L1=2
        If(RBIN(L2).EQ.13) L2=L2-1
        If(COPT.EQ.'L'.OR.COPT.EQ.'l') Call STRLO(RBIN,L1,L2) 
        If(COPT.EQ.'U'.OR.COPT.EQ.'u') Call STRUP(RBIN,L1,L2) 
        If(RCIN(L1:L1).EQ.'>') go to 100 
        Write(6,'(A)') RCIN(L1:L2)
        Go to   1
 
  100   Return              

  901   Call Perror('[PRENT]:')
        Go to  100
        End
************************************************************************
        Subroutine STRUP(RBIN,L1,L2)

        Byte              RBIN(*)

        Do 10 I1=L1,L2
           If(RBIN(I1).GE. 97.AND.RBIN(I1).LE.122) RBIN(I1)=RBIN(I1)-32 
   10   Continue
        Return
        End
************************************************************************
        Subroutine STRLO(RBIN,L1,L2)

        Byte              RBIN(*)

        Do 10 I1=L1,L2
           If(RBIN(I1).GE. 65.AND.RBIN(I1).LE. 90) RBIN(I1)=RBIN(I1)+32 
   10   Continue
        Return
        End
************************************************************************
        Include          'PSQID.f'
        Include          'LOCSQ.f'
