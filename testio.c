#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main(){

const char fname[256] ="/home/giovanna/SSA_tests/bacteria.ptr";
int fd;
char *s = fname;
char *f = NULL;
int len = 0;
int i = 0;

  f = malloc((strlen(fname) + 1) * sizeof(char));
  while(*s != 0 && !isspace((unsigned char) *s)){
    f[i++] = *s++;
    ++len;
  }

  f[len+1] = '\0';
  printf("testIO: len %d\n", len);
  printf("testIO: file name %s\n", f);

printf("testIO: file name %s\n", fname);
printf("testIO: file len %d\n", strlen(fname));

  if ((fd = open(fname, O_RDONLY)) == -1) {
    perror("\ntestIO open");
    return -1;
  }
printf("testIO: file descriptor  %d\n", fd);

return 0;
}
