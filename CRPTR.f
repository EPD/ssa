      Program CRPTR 
*----------------------------------------------------------------------*
*     Creates SSA2 pointer files for various sequence database formats 
*----------------------------------------------------------------------*
      Parameter        (NINP=11)
      Parameter        (NSEQ=12)
      
      Character*80      RCIN
      Character*63      RCEX
      
      Character*64      ARG
      Character*64      FINP  
      Character*80      FSEQ  
      
      Logical           FCHK
      Data              FCHK/.FALSE./
*     CI - suppress header
      Logical           HDR
      Data              HDR/.TRUE./
      
      Logical           ANNO
      Data              ANNO/.FALSE./
      
      Character*64      FMT
      Data              FMT/' '/
      Character*64      DIR
      Data              DIR/' '/
      Character*64      SEQ
      Data              SEQ/' '/
      Character*64      DOC
      Data              DOC/' '/
      Character*64      KEY
      Data              KEY/' '/
      Integer           KLN
      Data              KLN/0/
      Integer           RCL
      Data              RCL/0/
      Character*64      CAS
      Data              CAS/' '/
      
      Character*03      FMTL
      Data              FMTL/'EGF'/
      Character*02      CASL
      Data              CASL/'UL'/
      
      Character*64      DIV
      
      Integer           Rindex
*----------------------------------------------------------------------*
      N1=Iargc() 
      
      If     (N1.EQ.1) then 
         Call GetArg(1,FINP)        
      Else if(N1.EQ.2) then
         Call GetArg(1,ARG)
         If(ARG.EQ.'-s') HDR=.FALSE.
         If(ARG.EQ.'-c') FCHK=.TRUE.
         If(ARG.EQ.'-a') ANNO=.TRUE.
         If(ARG.NE.'-s'.AND.ARG.NE.'-c'.AND.ARG.NE.'-a') go to   901
         Call GetArg(2,FINP)
      Else
         Go to   901
      End if
      
      Open(NINP,File=FINP,Status='OLD',Err=902)
*----------------------------------------------------------------------*
 1    Read(NINP,"(A)",End=903) RCIN
      If(Index(RCIN,'..').NE.0) go to   6
      L=Lblnk(RCIN)
      IC=Index(RCIN(1:L),'=')+1
      If(IC.EQ.1.OR.IC.GT.L) go to   1
      ARG=RCIN(IC:L)
      IC2=Lblnk(ARG)
      IC1=Rindex(ARG(1:IC2),' ')+1
      If(IC1.GT.64) IC1=1
      ARG(64:64)='0'
      
      If(Index(RCIN(1:IC),'FMT').NE.0) FMT=ARG(IC1:IC2) 
      If(Index(RCIN(1:IC),'DIR').NE.0) DIR=ARG(IC1:IC2) 
      If(Index(RCIN(1:IC),'SEQ').NE.0) SEQ=ARG(IC1:IC2)
      If(SEQ(2:4).EQ.'gbs') ANNO=.TRUE.
      If(Index(RCIN(1:IC),'DOC').NE.0) DOC=ARG(IC1:IC2) 
      If(Index(RCIN(1:IC),'KEY').NE.0) KEY=ARG(IC1:IC2)
      If(Index(RCIN(1:IC),'KLN').NE.0) Read(ARG,*,Err=904) KLN
      If(Index(RCIN(1:IC),'RCL').NE.0) Read(ARG,*,Err=905) RCL 
      If(Index(RCIN(1:IC),'CAS').NE.0) CAS=ARG(IC1:IC2) 
      Go to   1 
      
 6    If(KLN.LE.3.OR.KLN.GT.32) go to 904
      If(Index(FMTL,FMT(1:1)).EQ.0) go to 906
      If(Index(CASL,CAS(1:1)).EQ.0) go to 907
      
      If(HDR) then
         Write(RCEX,"(' 1. FMT=',A55)") FMT
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 2. DIR=',A55)") DIR
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 3. SEQ=',A55)") SEQ
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 4. DOC=',A55)") DOC
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 5. KEY=',A55)") KEY
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 6. KLN=', I4)") KLN
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 7. RCL=', I4)") RCL 
         Write(6,'(A63)') RCEX
         Write(RCEX,"(' 8. CAS=',A55)") CAS 
         Write(6,'(A63)') RCEX
      End if
*----------------------------------------------------------------------*
 11   Read(NINP,"(A)",End= 99) RCIN
      If(RCIN.EQ.' ') go to  11
      DIV=RCIN
      IC1=Lblnk(DIR)
      IC2=Lblnk(DIV)
      IC3=Lblnk(SEQ)
      FSEQ=DIR(1:IC1) // DIV(1:IC2) // SEQ(1:IC3)
      Open(NSEQ,File=FSEQ,Status='old',Iostat=IOS)
      If(IOS.NE.0) then
         Call Perror(' ')
         Go to  11
      End if
      L=Lblnk(FSEQ)
      Write(0,*) 'Reading file: ',FSEQ(1:L),' (format ',FMT(1:1),').'
      
      If(FMT(1:1).EQ.'E') 
     *     Call EMPTR(NSEQ,DIV,KEY,KLN,ANNO)
      If(FMT(1:1).EQ.'G') 
     *     Call GBPTR(NSEQ,DIV,KEY,KLN,ANNO)
      If(FMT(1:1).EQ.'F') 
     *     Call FAPTR(NSEQ,DIV,KEY,KLN,RCL,CAS,FCHK)
      Go to  11 
*----------------------------------------------------------------------*
 99   Continue
 100  Stop
*----------------------------------------------------------------------*
      
 901  Write(0,*) 'Usage:   CRPTR [-c|-s] parameterfile'
      Write(0,*) 'Options: -c check sequence format'
      Write(0,*) '         -s suppress header output'
      Write(0,*) '         -a indexes complete annotation file'
      Go to 100        
 902  Call Perror(' ')
      Go to 100 
 903  Write(6,*) 'No sequence database file specified'
      Go to 100
 904  Write(6,*) 'Illegal KLN'
      Go to 100
 905  Write(6,*) 'Illegal RCL'
      Go to 100
 906  Write(6,*) 'Illegal FMT'
      Go to 100
 907  Write(6,*) 'Illegal CAS'
      Go to 100
*----------------------------------------------------------------------*
      End
************************************************************************
      Subroutine EMPTR(NSEQ,DIV,KEY,KLN,ANNO)
      
      Character*(*)     DIV
      Character*(*)     KEY 
      
      Character*32      SQID(128) 
      Character*32      SQID2(128)
      Character*80      RCIN
      Character*63      RCEX
      Character*04      SV
      
      Logical           LACN
      Logical           ANNO
      
      Integer           Rindex
      Integer           FTell
      Integer*8         IDOC
      Integer*8         ISEQ
      Integer*8         LSEQ
      
      IB=0
      ISEQ=0
      IDOC=0
      RCEX=' '
      NRID=0
      
 1    Read(NSEQ,"(A)",End=100) RCIN
      L=Lblnk(RCIN(1:80))
      
      If(RCIN(1:2).EQ.'//') then 
         
*     write entry pointers 
         
         Do   2 I1=1,NRID
            If (KEY(1:1).EQ.'1'.OR.KEY(1:2).EQ.'ID') then
               RCEX(1:KLN)=SQID(I1)(1:KLN) 
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
            Else if(KEY(1:1).EQ.'2'.OR.KEY(1:2).EQ.'SV') then
               RCEX(1:KLN)=SQID2(I1)(1:KLN)
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
            Else if(KEY(1:1).EQ.'3') then
               RCEX(1:KLN)=SQID(I1)(1:KLN) 
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
               RCEX(1:KLN)=SQID2(I1)(1:KLN)
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
            End if
            
 2       Continue
         NRID=0
         LSEQ=0
         IB=IB+L+1
         Go to   1
      End if 
      
      If(RCIN(1:2).EQ.'ID') then 
         
*     process ID line
*     ID   AB000095; SV 1; linear; mRNA; STD; HUM; 2399 BP.
         
         If(ANNO.AND.RCIN(1:2).EQ.'ID') ISEQ=FTell(NSEQ)
         LACN=.TRUE.
         If(NRID.EQ.0) IDOC=FTell(NSEQ)-L-1 
         NRID=NRID+1
         IB=IB+L+1
         
         IC=Index(RCIN(6:80),';')+4
         SQID(NRID)=RCIN(6:IC)
         IC2=Index(RCIN(1:80),'SV')+3 
         IC3=Index(RCIN(IC2:80),';')+IC2-2 
         SV='.' // RCIN(IC2:IC3)
         NRID=1
         SQID2(NRID)=RCIN(6:IC) // SV
         IC=Rindex(RCIN(1:80),';')+1
         If(NRID.EQ.1) Read(RCIN(IC:80),*) LSEQ
         Go to   1
      End if
      
      IB=IB+L+1 
      
      If(RCIN(1:2).EQ.'AC'.AND.KEY(1:2).EQ.'AC'.AND.LACN) then
         LACN=.FALSE.
         IC=Index(RCIN(6:80),';')+4
         NRID=1
         SQID(NRID)=RCIN(6:IC)
      End if
      
      If(.NOT.ANNO.AND.RCIN(1:2).EQ.'SQ') ISEQ=FTell(NSEQ)
      Go to   1 
      
 98   Do  99 I1=1,NRID
         If(KEY(1:1).EQ.'1'.OR.KEY(1:1).EQ.'3'.OR.KEY(1:2).EQ.'ID') then
            RCEX(1:KLN)=SQID(I1)(1:KLN) 
            RCEX(KLN+1: 32)=DIV(1:32-KLN)
            Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
            Write(6,'(A63)') RCEX
         End if
         If(KEY(1:1).EQ.'2'.OR.KEY(1:2).EQ.'SV') then
            RCEX(1:KLN)=SQID2(I1)(1:KLN)
            RCEX(KLN+1: 32)=DIV(1:32-KLN)
            Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
            Write(6,'(A63)') RCEX
         End if
         If(KEY(1:1).EQ.'3') then
            RCEX(1:KLN)=SQID2(I1)(1:KLN)
            RCEX(KLN+1: 32)=DIV(1:32-KLN)
            Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
            Write(6,'(A63)') RCEX
         End if
         
 99   Continue
      
 100  Return
      End
************************************************************************
      Subroutine GBPTR(NSEQ,DIV,KEY,KLN,ANNO)
      
      Character*(*)     DIV
      Character*(*)     KEY 
      
      Character*32      SQID(128)
      Character*32      SQID2(128)
      Character*80      RCIN
      Character*63      RCEX
      Character*04      SV
      
      Logical           LACN
      Logical           ANNO
      
      Integer           Rindex
      Integer           FTell
      Integer*8         IDOC
      Integer*8         ISEQ
      Integer*8         LSEQ
      
      IB=0
      ISEQ=0
      IDOC=0
      RCEX=' '
      NRID=0
      
 1    Read(NSEQ,"(A)",End=100) RCIN
      L=Lblnk(RCIN(1:80))
      
      If(RCIN(1:2).EQ.'//') then 
         
*     write entry pointers 
         
         Do   2 I1=1,NRID
            If(KEY(1:1).EQ.'1'.OR.KEY(1:2).EQ.'ID') then
               RCEX(1:KLN)=SQID(I1)(1:KLN) 
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
            Else if(KEY(1:1).EQ.'2'.OR.KEY(1:2).EQ.'SV') then
               RCEX(1:KLN)=SQID2(I1)(1:KLN)
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
            Else if(KEY(1:1).EQ.'3') then
               RCEX(1:KLN)=SQID(I1)(1:KLN) 
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
               RCEX(1:KLN)=SQID2(I1)(1:KLN)
               RCEX(KLN+1: 32)=DIV(1:32-KLN)
               Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
               Write(6,'(A63)') RCEX
            End if

 2       Continue
         NRID=0
         LSEQ=0
         IB=IB+L+1
         Go to   1
      End if 
      
      If(RCIN(1:5).EQ.'LOCUS') then 
         
*     process ID line
*     LOCUS       NC_000001          247249719 bp    DNA     linear   CON 30-AUG-2006
         If(ANNO.AND.RCIN(1:5).EQ.'LOCUS') ISEQ=FTell(NSEQ)
         LACN=.TRUE.
         If(NRID.EQ.0) IDOC=FTell(NSEQ)-L-1 
         NRID=NRID+1
         IB=IB+L+1
         IC=Index(RCIN(13:80),' ')+11
         SQID(NRID)=RCIN(13:IC)
         IC2=Index(RCIN(1:80),'bp')-2
         IC3=Rindex(RCIN(IC+1:IC2),' ')+IC+1
         If(NRID.EQ.1) Read(RCIN(IC3:IC2+3),*) LSEQ
         Go to   1
      End if
      
      IB=IB+L+1 
      
      If(RCIN(1:7).EQ.'VERSION'.AND.LACN) then
         LACN=.FALSE.
         IC=Index(RCIN(13:80),' ')+12
         NRID=1
         SQID2(NRID)=RCIN(13:IC)
      End if
      
      If(.NOT.ANNO.AND.RCIN(1:6).EQ.'ORIGIN') ISEQ=FTell(NSEQ)
      If(.NOT.ANNO.AND.RCIN(1:4).EQ.'BASE') ISEQ=FTell(NSEQ)
      Go to   1
      
 98   Do  99 I1=1,NRID
         If(KEY(1:1).EQ.'1'.OR.KEY(1:1).EQ.'3'.OR.KEY(1:2).EQ.'ID') then
            RCEX(1:KLN)=SQID(I1)(1:KLN) 
            RCEX(KLN+1: 32)=DIV(1:32-KLN)
            Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
            Write(6,'(A63)') RCEX
         End if
         If(KEY(1:1).EQ.'2'.OR.KEY(1:2).EQ.'SV') then
            RCEX(1:KLN)=SQID2(I1)(1:KLN)
            RCEX(KLN+1: 32)=DIV(1:32-KLN)
            Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
            Write(6,'(A63)') RCEX
         End if
         If(KEY(1:1).EQ.'3') then
            RCEX(1:KLN)=SQID2(I1)(1:KLN)
            RCEX(KLN+1: 32)=DIV(1:32-KLN)
            Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
            Write(6,'(A63)') RCEX
         End if

 99   Continue
      
 100  Return
      End
************************************************************************
      Subroutine FAPTR(NSEQ,DIV,KEY,KLN,RCL,CAS,FCHK)
      
      Character*(*)     DIV
      Character*(*)     KEY 
      Integer           RCL
      Character*(*)     CAS
      Logical           FCHK 
      
      Character*32      SQID 
      Character*32      SQID2 
      
      Character*200     RCIN
      Byte              RBIN(200)
      Equivalence       (RCIN,RBIN)
      
      Character*63      RCEX
      
      Integer*8         LSEQ
      Integer*8         ISEQ
      Integer		Rindex
      
      If(CAS(1:1).EQ.'U') then 
         IBL= 65
         IBU= 90
      Else
         IBL= 97
         IBU=122
      End if
      
      LREC=(RCL-1)/2*2 
      KREC=RCL-LREC
      
      IB=0
      ISEQ=0
      IDOC=0
      RCEX=' '
      
 1    Read(NSEQ,"(A)",End= 99) RCIN
      L=Lblnk(RCIN(1:200))
      IB=IB+L+1
      If(RCIN(1:1).NE.'>') go to   1 
      Go to  5
      
 2    Read(NSEQ,"(A)",End= 99) RCIN
      L=Lblnk(RCIN(1:200))
      IB=IB+L+1
      If(RCIN(1:1).NE.'>') go to   2 
      
 3    If(KEY(1:1).EQ.'1'.OR.KEY(1:1).EQ.'3'.OR.KEY(1:2).EQ.'ID') then
         RCEX(1:KLN)=SQID(1:KLN) 
         RCEX(KLN+1: 32)=DIV(1:32-KLN)
         Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
         Write(6,'(A63)') RCEX
      End if
      If(KEY(1:1).EQ.'2'.OR.KEY(1:2).EQ.'SV') then
         RCEX(1:KLN)=SQID2(1:KLN)
         RCEX(KLN+1: 32)=DIV(1:32-KLN)
         Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
         Write(6,'(A63)') RCEX
      End if
      If(KEY(1:1).EQ.'3') then
         RCEX(1:KLN)=SQID2(1:KLN)
         RCEX(KLN+1: 32)=DIV(1:32-KLN)
         Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
         Write(6,'(A63)') RCEX
      End if
      
 5    IDOC=IB-L-1
      ISEQ=IB
*     Get Both SeqIDs, i.e.: >chr|NT_XXXXXXX|NT_XXXXXX.YY
      IC1=Index(RCIN,'|')+1
      IC2=Rindex(RCIN,'|')-1
      IC3=Rindex(RCIN,'|')+1
      IC4=Index(RCIN,' ')-1
*     Patch for different header format: >gi|152977688|ref|NC_009655.1|
*     In this case: SQID=152977688|ref|NC_009655.1 ; SQID2=''
      If(RCIN(IC1:IC2).NE.RCIN(IC3:IC4-2)) then
         If(RCIN(IC1:IC2).NE.RCIN(IC3:IC4-3)) then
            Write(0,*) 'Warning: unexpected header format : ', RCIN
	    IC5=Rindex(RCIN(IC1:IC2),'|')+5
            SQID=RCIN(IC5:IC2-2)
            SQID2=RCIN(IC5:IC2)
         Else  
            SQID=RCIN(IC1:IC2)
            SQID2=RCIN(IC3:IC4)
         End if
      Else  
         SQID=RCIN(IC1:IC2)
         SQID2=RCIN(IC3:IC4)
      End if

      LSEQ=0
      
      If(FCHK) go to  20
      
 10   Read(NSEQ,"(A)",End= 99) RCIN
      L=Lblnk(RCIN)
      IB=IB+L+1
      If(RCIN(1:1).EQ.'>') go to   3 
      LSEQ=LSEQ+L
      Go to  10
      
 20   Read(NSEQ,"(A)",End= 99) RCIN
      L=Lblnk(RCIN)
      IB=IB+L+1
      If(RCIN(1:1).EQ.'>') go to   3  
      
      Do  25 I1=1,L
         If(RBIN(I1).LT.IBL.OR.RBIN(I1).GT.IBU) go to  26
         LSEQ=LSEQ+1
 25   Continue
      
 26   K1=IB-ISEQ
      K2=(K1-1)/RCL
      K3=K2*LREC+K1-K2*RCL-KREC
      If(LSEQ.NE.K3) go to 901
      Go to  20 
      
 99   If(KEY(1:1).EQ.'1'.OR.KEY(1:1).EQ.'3'.OR.KEY(1:2).EQ.'ID') then
         RCEX(1:KLN)=SQID(1:KLN) 
         RCEX(KLN+1: 32)=DIV(1:32-KLN)
         Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
         Write(6,'(A63)') RCEX
      End if
      If(KEY(1:1).EQ.'2'.OR.KEY(1:2).EQ.'SV') then
         RCEX(1:KLN)=SQID2(1:KLN)
         RCEX(KLN+1: 32)=DIV(1:32-KLN)
         Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
         Write(6,'(A63)') RCEX
      End if
      If(KEY(1:1).EQ.'3') then
         RCEX(1:KLN)=SQID2(1:KLN)
         RCEX(KLN+1: 32)=DIV(1:32-KLN)
         Write(RCEX(33:63),"(I10,I10,I11)") LSEQ,ISEQ-IDOC,IDOC 
         Write(6,'(A63)') RCEX
      End if
      
 100  Return
      
 901  Write(0,*) 'Format error. Check record length.' 
      Print *,LSEQ,K1,K2,K3,LREC,KREC
      Go to 100
      
      End
*----------------------------------------------------------------------*
