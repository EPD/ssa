SSA - Signal Search Analysis
============================================================================

SSA is a software package for the analysis of nucleic acid sequence motifs that
are positionally correlated with a functional site (e.g a transcription or translation initiation site).


Description
============================================================================

Signal Search Analysis (SSA) is a method (published in 1984) to analyse and characterize sequence motifs that occur at characteristic distances upstream or downstream from a functional site in a DNA sequence. The method has been developed for the analysis of eukaryotic promoters but has a much broader application potential.

The central concept of signal search analysis is to find a motif present in all or a statistically significant proportion of DNA input sequences. The location of the motif within the sequences is irrelevant. In signal search analysis, the input is a list of experimentally defined functional sites, for instance transcription initiation sites, given as pointers to positions in nucleotide database sequences. The user specifies on the fly the sequence range around the site one wants to consider. In signal search analysis, not only the structure, but also the location relative to the functional site as well as the distance flexibility are of interest.

The SSA server provides access to four different computer programs as well as to a large number of precompiled functional site collections. The programs offered allow (i) the identification of non-random sequence regions under evolutionary constraint, (ii) the detection of consensus sequence-based motifs that are over- or under-represented at a particular distance from a functional site, (iii) the analysis of the positional distribution of a consensus sequence- or weight matrix-based sequence motif around a functional site, and (iv) the optimization of a weight matrix description of a locally over-represented sequence motif.

These programs can be accessed at: https://epd.expasy.org/ssa/

References
============================================================================

1. Ambrosini G, Praz V, Jagannathan V, Bucher P.. 2003. Nucleic Acids Res.
2. Bucher P, Trifonov EN.. 1986. Nucleic Acids Res.

See also:
http://seqanswers.com/wiki/SSA

Program Installation
============================================================================

For code compilation a suitable makefile is provided.

To compile the programs, type:

make

To install the binary files (default dir : bin.x86_64), type:

make install

To delete binary and object files, type:

make clean
