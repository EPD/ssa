        Integer Function  RINDEX(STR1,STR2) 

        Character*(*)     STR1
        Character*(*)     STR2

        L1=Len(STR1)
        L2=Len(STR2)
    
        Rindex=0
        If(L1.LT.L2) go to 100

        Do   9 I1=L1-L2+1,1,-1
              J2=1
           Do   8 I2=I1,I1+L2-1
              If(STR1(I2:I2).NE.STR2(J2:J2)) go to   9
    8      Continue
           Rindex=I1
           Go to 100 
    9   Continue

  100   Return

        End
