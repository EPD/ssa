*$      Subroutine FROMFPSI: Created 02/20/92 ; last update 03/06/92   *
*----------------------------------------------------------------------*
*       Author    : Philipp Bucher                                       
*       Function  : Interactive dialogue of program FROMFPS
*       Arguments : NERR (R-) : Logical unit for terminal output 
*                   NINP (R-) : Logical unit for terminal input 
*                   NFPS (R-) : Logical unit for FPS input file
*                   FTIT (-W) : Title of FPS
*                   MB1  (-W) : Left  (5'OH) border of sequence segments 
*                   MB2  (-W) : Right (3'OH) border of sequence segments 
*                   IC1  (-W) : Starting position of description 
*                   IC2  (-W) : Length of description
*                   CTRC (-W) : Translation code
*                   CFMT (-W) : Output format (GCG, EMBL, or FASTA)
*                   IRC  (-W) : Return code
*----------------------------------------------------------------------*

        Subroutine FROMFPSI
     *    (NERR,NINP,NFPS,NOUT,MB1,MB2,IC1,IC2,CTRC,CFMT,FOUT,IRC)

        Character*192      RC80
        Character*192      FFPS

        Character*10      FNAM
        Character*55      FTIT
      
        Character*01      CTRC
        Character*01      CFMT 
        Character*(*)     FOUT

        Character*04      CEXT
        Character*10      FDEF
           Data              FDEF/'FROMFPS   '/

        Integer*8         MB1
        Integer*8         MB2

* Interactive dialogue
      
        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901)
     *    'Program FROMFPS / Beginning of interactive dialogue:'
        Write(NERR,*,Err=901) 
     *    'Press <Return> for default, enter < to go back to ',
     *    'previous question.'       
        Write(NERR,*,Err=901) ' '

* Open FPS file, read title (FTIT)

   10   FFPS='Fps.Fps'
        Write(NERR,1010,Err=901) FFPS(1:7)
 1010   Format
     *   ('  1. Input file (default=',A7,'): ',$)
        Read(NINP,'(A)',Err= 15) RC80
        If(RC80.EQ.' ') go to  11
        Read(RC80,'(A)',Err= 15) FFPS
   11   Open(NFPS,File=FFPS,Status='OLD',Err= 15)
        Go to  16
   15   Write(NERR,1199,Err=901) 
        Go to  10

   16   FTIT=' ' 
   17   Read(NFPS,'(A)',Err=902,End= 18) RC80 
           If(RC80(1:2).EQ.'FP') go to  18
           If(RC80(1:2).EQ.'TI') go to  19
   18   RC80(16:70)='Unnamed functional position set'
        RC80( 6:15)=' '

   19   Rewind(NFPS,Err=901)
        FNAM=RC80( 6:15)
        FTIT=RC80(16:70)

        Write(NERR,*,Err=901) ' '
        Write(NERR,1019,Err=901) FTIT 
 1019   Format
     *   ('     ',A55)
        Write(NERR,*,Err=901) ' '

* 5' border (MB1)

   20   MB1=-99
        Write(NERR,1020,Err=901)  MB1                                                  
 1020   Format
     *   ('  2. Left (5''OH/N-terminal) border (default=',I5,'): ',$)         
        Read(NINP,'(A)',Err= 25) RC80                               
        If(RC80.EQ.' ') go to  30                                   
        If(RC80.EQ.'<') go to  10
        Read(RC80,'(I6)',Err= 25) MB1                                      
        Go to  30
   25   Write(NERR,1199,Err=901) 
        Go to  20 

* 3' border (MB2)

   30   MB2=MAX(100,MB1+100)
        Write(NERR,1030,Err=901) MB2                                                  
 1030   Format
     *   ('  3. Right (3''OH/C-terminal) border (default=',I5,'): ',$)      
        Read(NINP,'(A)',Err= 35) RC80                               
        If(RC80.EQ.' ') go to  40 
        If(RC80.EQ.'<') go to  20
        Read(RC80,*,Err= 35) MB2                                      
        If(MB2.GT.MB1) go to  40
   35   Write(NERR,1199,Err=901)
        Go to  30

* Starting position of description (IC1)

   40   IC1=6
        Write(NERR,1040,Err=901) IC1
 1040   Format
     *   ('  4. Starting position of description (Default=',I2,'): ',$)
        Read(NINP,'(A)',Err= 45) RC80
        If(RC80.EQ.' ') go to  50
        If(RC80.EQ.'<') go to  30
        Read(RC80,*,Err= 45) IC1    
        If(IC1.GE.1.AND.IC1.LE.80) go to 50 
   45   Write(NERR,1199,Err=901) 
        Go to  40

* Length of description (IC2)

   50   IC2=30
        If(IC1+IC2-1.GT.80) IC2=80-IC1+1
        Write(NERR,1050,Err=901) IC2
 1050   Format
     *   ('  5. Length of description (Default=',I2,'): ',$)
        Read(NINP,'(A)',Err= 55) RC80
        If(RC80.EQ.' ') go to  60 
        If(RC80.EQ.'<') go to  40
        Read(RC80,*,Err= 55) IC2
        Go to  60 
   55   Write(NERR,1199,Err=901)
        Go to  50

* Translation code (CTRC)

   60   CTRC='D'
        Write(NERR,1060,Err=901) CTRC
 1060   Format
     *   ('  6. Translation code (D, R, F, or P, default=',A1,'): ',$)
        Read(NINP,'(A)',Err= 65) RC80
        If(RC80.EQ.' ') go to  70 
        If(RC80.EQ.'<') go to  50 
        Read(RC80,'(A)',Err= 65) CTRC
        If(Index('drfp',CTRC).NE.0) go to  69
        If(Index('DRFP',CTRC).NE.0) go to  70 
   65   Write(NERR,1199,Err=901)
        Go to  60 
  
   69   If(CTRC.EQ.'d') CTRC='D'
        If(CTRC.EQ.'r') CTRC='R'
        If(CTRC.EQ.'f') CTRC='F'
        If(CTRC.EQ.'p') CTRC='P'

* Output format (CFMT) 

   70   CFMT='E'
        Write(NERR,1070,Err=901) CFMT 
 1070   Format
     *   ('  7. Output format (E=EMBL, F=FASTA, G=GCG, default=',A1,
     *    '): ',$)
        Read(NINP,'(A)',Err= 75) RC80
        If(RC80.EQ.' ') go to  80 
        If(RC80.EQ.'<') go to  60 
        Read(RC80,'(A)',Err= 75) CFMT 
        If(Index('efg',CFMT).NE.0) go to  79
        If(Index('EFG',CFMT).NE.0) go to  80 
   75   Write(NERR,1199,Err=901)
        Go to  70 
  
   79   If(CTRC.EQ.'e') CFMT='E'
        If(CTRC.EQ.'f') CFMT='F'
        If(CTRC.EQ.'g') CFMT='G'

* Output filename (FOUT)

   80   If     (CFMT.EQ.'E') then 
           CEXT='.dat' 
        Else if(CFMT.EQ.'F') then 
           CEXT='.seq' 
        Else if(CFMT.EQ.'G') then 
           CEXT='.fil' 
        End if

        Call FNDEF(NFPS,FNAM,CEXT,FDEF,FOUT) 

        Write(NERR,1080,Err=901) FOUT 
 1080   Format
     *   ('  8. Output filename (Default=',A14,
     *    ', no extension please): ',$)
        Read(NINP,'(A)',Err= 85) RC80
        If(RC80.EQ.' ') go to  90 
        If(RC80.EQ.'<') go to  70 
        Read(RC80,'(A)',Err= 75) FOUT
        FOUT=(FOUT(1:Lblnk(FOUT)) // CEXT)
        Open(NOUT,File=FOUT,Status='UNKNOWN',Err= 85)  
        Go to  90

   85   Write(NERR,1199,Err=901)
        Go to  80 

   90   Continue
  
  100   Return 
    
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to  100

 1199   Format('     Input not accepted; try again !')

        End
*----------------------------------------------------------------------*
        Include        'FNDEF.f'
