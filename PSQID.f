        Subroutine PSQID(NPTR,FDBE,LPTR,FNAM,SQID,IRC)

* parameters

        Character*11      DBDEF 
        Parameter        (DBDEF='SSA2DBC.def')

* arguments

        Character*(*)     FDBE
        Character*(*)     FNAM
        Character*(*)     SQID 
        Logical           LPTR

* database codes 

        Character*80      FDBF
        Logical           LDBF
        Data              LDBF/.FALSE./

        Integer           NDBC
        Data              NDBC/0/
        Integer           LDBC(128)
        Data              LDBC/128*0/
        Character*12      CDBC(128) 
        Data              CDBC/128*''/
        Character*64      CPTR(128) 
        Data              CPTR/128*''/

* work fields

        Character*80      CHRS
        Character*80      RCIN
        Equivalence      (RCIN,CHRS)

* functions
 
        Integer           Rindex

* start program execution

        IRC=0

        LPTR=.TRUE.

        Write(NERR, *) 'PSQID::'
        Write(NERR, *) 'FDBE ', FDBE
        Write(NERR, *) 'LDBF ', LDBF
        Write(NERR, *) 'NDBC ', NDBC
        Write(NERR, *) 'CPTR ', NDBC, CPTR(NDBC) 

        L=Lblnk(FDBE)
        L1=Index(FDBE,':')
        If (L1.EQ.0) then
           FNAM=FDBE
           SQID=' '
           LPTR=.FALSE.
        Else
           FNAM=FDBE(1:L1)
           SQID=FDBE(L1+1:L)
           LPTR=.TRUE.
        End if
        Write(NERR, *) 'PSQID:: FNAM ', FNAM
        Write(NERR, *) 'LPTR ', LPTR
        If (FNAM(1:1).EQ.'$') then
           L=Lblnk(FNAM)
           CHRS=FNAM(2:L-1)
           Call getenv('SSA2DBC',FNAM)
           Write(NERR, *) 'PSQID end ($):: FNAM ', FNAM
        Else if(LPTR) then 
* read database code definition 
           Write(NERR, *) 'read database code definition LDBF', LDBF
           If(.NOT.LDBF) then 
              NDBC=0
*  -  find input file
              Write(NERR, *) 'Open PTR file DBDEF ', DBDEF
              Open(21,File=DBDEF,Status='OLD',Iostat=IOS)
              Write(NERR, *) 'After Open IOS ', IOS
              If(IOS.EQ.0) go to  21

              CHRS='HOME' 
              Call getenv(CHRS,FDBF)
              L=Lblnk(FDBF)+1
              FDBF(L:L)='/'
              FDBF(L+1:80)= DBDEF
              Write(NERR, *) 'Open 1 FDBF ', FDBF
              Open(21,File=FDBF,Status='OLD',Iostat=IOS)
              If(IOS.EQ.0) go to  21

              CHRS='SSA2DBC'
              Call getenv(CHRS,FDBF)
              Write(NERR, *) 'Open 2 FDBF ', FDBF
              Open(21,File=FDBF,Status='OLD',Iostat=IOS)
              If(IOS.EQ.0) go to  21

              Open(21,File='/local/ssa/data/SSA2DBC.def',Status='OLD',
     *           Err=902)
              
*  - read input file

   21         Read(21,"(A)",End= 22) RCIN             
              L1=Index(RCIN,'=')
              L2=Lblnk(RCIN)
              If(L1.EQ.0) go to  21
              If(RCIN( 1:L1-1).EQ.' ') go to 21 
              If(RCIN(L1+2:L2).EQ.' ') go to 21 
              NDBC=NDBC+1
              CDBC(NDBC)=RCIN( 1:L1-1)
C             Read(RCIN( 1:L1-1),*) CDBC(NDBC)
              LDBC(NDBC)=Lblnk(CDBC(NDBC))      
              L3=Rindex(RCIN(L1+2:L2),' ')
              CPTR(NDBC)=RCIN(L1+L3+2:L2)
              Go to  21

   22         Close(21) 
              If(NDBC.LT.1) go to 902 
              LDBF=.TRUE.

           End if

* database-code to pointer-file-name
           Write(NERR, *) 'PSQID  database-code to pointer-file-name' 
           Write(NERR, *) 'NDBC ', NDBC
           Do  28 I1=1,NDBC
              Write(NERR, *) 'CDBC ', CDBC(I1)(1:LDBC(I1))
              Write(NERR, *) 'FNAM ', FNAM(1:LDBC(I1)) 
              If(CDBC(I1)(1:LDBC(I1)).EQ.FNAM(1:LDBC(I1))) then
                 Write(NERR, *) 'CPTR ', I1, CPTR(I1) 
                 FNAM=CPTR(I1)
                 Go to  29
              End if
   28      Continue
           Write(NERR, *) 'CPTR ', NDBC, CPTR(NDBC) 
           FNAM=CPTR(NDBC)
           Write(NERR, *) 'PSQID end (LPTR T):: FNAM ', FNAM
   29      Continue

        End if

        Write(NERR, *) 'PSQID END:: FNAM ', FNAM
  100   Return

  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 100

        End 
