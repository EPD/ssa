*       Program prseq

        Parameter        (NSEQ=11)
        Parameter        (NPTR=12)
        Parameter        (NREF=13)
C       Parameter        (NERR= 7)
        Parameter        (NERR= 0)

        Parameter        (IDM1=1048576)

        Character         CSEQ(IDM1)
        Character*64      FDBE

        Character*01      CC 
        Character*01      CB
        Character*02      COPT
        Character*01      CSTR
        Character*01      CTRC

        Integer*8         LSEQ
        Integer*8         IPOS
        Integer*8         LM
        Integer*8         N1
        Integer*8         N2

        IRC=0
        
        CC='U'
        CB='N'
        IST=1
        ITY=1
        CTRC='D'

        Call Repar
     *    (FDBE,N1,N2,COPT,IRC)
        If(IRC.NE.0) then
           Write(NERR,'(
     *      ''Usage: preseq seq-ref begin end extraction-code''
     *        )')
           Stop
        End if

        IPOS=N1
        LM=N2-N1+1
        If(COPT(1:1).EQ.'0') ITY=0 
        CSTR=COPT(2:2)

        Call RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LSEQ,ITY,IPOS,LM,CSEQ,IDM1,IST,CC,IRC)
        If(IRC.NE.0) Print *,'RETSQ',IRC

        Call STREV(CSEQ,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)
        If(IRC.NE.0) Print *,'STREV',IRC
 
        Write(6,
     *   '(''>'',A16,'' '',I8,'' -'',I8,'' ['',A2,''].'')')
     *   FDBE(1:16),N1,N2,COPT
        Write(6,'((60A))') (CSEQ(ii1),ii1=1,LM)
        Stop
        End
*----------------------------------------------------------------------*
        Subroutine Repar
     *    (FDBE,N1,N2,COPT,IRC)
 
        Character*64      FDBE
        Character*64      CPAR
        Character*02      COPT
        Integer*8         N1
        Integer*8         N2
 
        N1=1
        N2=1
        COPT='1+'

        K1=Iargc()
        If(K1.LE.3) go to 900
 
        Call GetArg(1,FDBE)

        Call GetArg(2,CPAR)
        Read(CPAR,*,Err=900) N1

        Call GetArg(3,CPAR)
        Read(CPAR,*,Err=900) N2

	If(K1.EQ.4) then
           Call GetArg(4,CPAR) 
           COPT=CPAR(1:2)
        End if

  100   Return
  900   IRC=1 
        Go to 100
        End 
*----------------------------------------------------------------------*
        Include           'RETSQ.f'
        Include           'STREV.f'
        Include           'PSQID.f'
        Include           'LOCSQ.f'
