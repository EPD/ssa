C@      MAIN PROGRAM SLIST : CREATED 05/14/86 ; LAST UPDATE 04/05/10   1
C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : LISTING OF OVER- OR UNDER-REPRESENTED SIGNALS        
C       ****************************************************************
                                                                        
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        PARAMETER    (IDM1=262144)                                        
        PARAMETER    (IDM2=524288)                                       
        PARAMETER    (IDM3=60)                                          
                                                                        
        PARAMETER    (NINP= 5)
        PARAMETER    (NERR= 0)
        PARAMETER    (NOUT= 6)
    
        PARAMETER    (NSSD=41)                                          
        PARAMETER    (ND80=51)                                          
        PARAMETER    (ND81=52)                                          
                                                                        
        INTEGER      ISSS(IDM1)                                         
        INTEGER      ISFS(IDM1)                                         
        INTEGER      ISFT(IDM2)                                         
        REAL         XMI (IDM1)                                         
        REAL         XMJ (IDM1)                                         
        REAL         XSTD(IDM1)                                         
        REAL         XNOR(IDM1)                                         
                                                                        
        EQUIVALENCE  (ISFT(1),XNOR(1)),(XMI(1),XSTD(1))                 
                                                                        
        CHARACTER    MSEQ(IDM3)                                         
                                                                        
        CHARACTER*50 TIT                                                
        Character*64 FSSD

	Real RN
                                                                        
        DATA MSEQ/60*' '/                                               
                                                                        
        FNSSD='SSD    '                                                 
        FND80='D80    '                                                 
        FND81='D81    '                                                 
                                                                        
        IRC=0                                                           
        IDEV=6                                                          
	IRAN=-123456789
                                                                        
        OPEN(ND80,FILE=FND80,ACCESS='DIRECT',RECL=80,ERR=903)           
        OPEN(ND81,FILE=FND81,ACCESS='DIRECT',RECL=80,ERR=904)           
C***********************************************************************
C       SECTION   I : Input of signal search parameters. Interactive    
C       input of additional parameters.                                 
C----------------------------------------------------------------------*
                                                                        
        Write(NERR,*) ' '
        Write(NERR,1000)                                                      
 1000   FORMAT('Program SLIST / Beginning of interactive dialogue:')    
        Write(NERR,*) ' '
                                                                        
    1   FSSD='SSD'
        Write(NERR,1011,Err=901) FSSD( 1: 7)
 1011   Format
     *   ('SSD file (default is ',A7,'): ',$)
        Read(NINP,"(A)",Err=  1) RC80
        If(RC80.EQ.' ') go to  2
        Read(RC80,"(A)",Err=  4) FSSD
    2   Open(NSSD,File=FSSD,Status='OLD',Err=  4)
        Go to   5 
    4   Write(NERR,1099,Err=901)
        Go to   1 

    5   CALL RESSD(NSSD,FISSD,NB1,NB2,
     *     MI,CTRC,TIT,CCTY,LS,LE,I,IRAN,LC,LD,LF,LH,NI,PR,FISSC,IRC)                                
        IF(IRC.EQ.0) GO TO 6                                            
        IRC=IRC+30                                                      
        GO TO 200                                                       
                                                                        
    6   Continue
        QN=RN
C       CALL PISSP(FISSD,TIT,NB1,NB2,CCTY,FISSC,LS,LE,I,IR,LC           
C    A,LD,LF,LH,PR,IDEV,IRC)                                            
C       IF(IRC.EQ.0) GO TO 10                                           
C       GO TO 905                                                       
                                                                        
   10   CALL INC12(NERR,NINP,TIT,NB1,NB2,MB1,MB2,LC,LD,N1,NJ,IRC)                 
        IF(IRC.EQ.0) GO TO 20                                           
        IF(IRC.EQ.1) IRC=0                                              
        GO TO 200                                                       
                                                                        
   20   COPT='+'                                                        
        Write(NERR,1020) COPT                                                 
 1020   FORMAT('Type of signals '
     A,'(+ = over-,',/,'- under-represented, B both, '      
     A,'default is ',A,'): ',$)                                          
        READ(5,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 30                                   
        READ(RC80,2002,ERR=28) COPT                                     
        IF(COPT.EQ.'+'.OR.COPT.EQ.'-'.OR.COPT.EQ.'B') GO TO 30          
   28   Write(NERR,1099)                                                      
        GO TO 20                                                        
                                                                        
   30   IOPT=1                                                          
        Write(NERR,1030) IOPT                                                 
 1030   FORMAT('Calculation mode (1 or 2, default is ',I1,'): ',$)
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 40                                   
        READ(RC80,2001,ERR=38) IOPT                                     
        IF(IOPT.EQ.1.OR.IOPT.EQ.2.OR.IOPT.EQ.3) GO TO 40                             
   38   Write(NERR,1099)                                                      
        GO TO 30                                                        
                                                                        
   40   XCUT=3                                                          
        Write(NERR,1040) XCUT                                                 
 1040   FORMAT('Cut-off value for signal frequencies',/         
     A,'(# of std''s above or below mean, default is ',F5.2,'): ',$)
        READ(5,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 50                                   
        CALL READX(RC80,XCUT,IRC)                                       
        IF(IRC.EQ.0) GO TO 50                                           
        Write(*,1099)                                                      
        GO TO 40                                                        
                                                                        
   50   CSPT='L'                                                        
        Write(NERR,1050) CSPT                                                 
 1050   FORMAT('Selection mode (A=all, L=local maxima,',/
     A,'G=global maxima, default is ',A,'): ',$)
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 100                                  
        READ(RC80,2002,ERR=28) CSPT                                     
        IF(CSPT.EQ.'A'.OR.CSPT.EQ.'L'.OR.CSPT.EQ.'G') GO TO 100         
   58   Write(NERR,1099)                                                      
        GO TO 50                                                        
C***********************************************************************
C       SECTION  II : Input and transposition of signal frequency       
C       matrix. Generation of signal sequence collection.               
C----------------------------------------------------------------------*
                                                                        
  100   WRITE(NOUT,4001,ERR=906) FISSD,NB1,NB2,MI,CTRC,TIT              
        WRITE(NOUT,4002,ERR=907) CCTY,LS,LE,I,QN,LC,LD,LF,LH,NI,PR      
                                                                        
           IF(IOPT.EQ.1) GO TO 105                                      
           DO 101 I2=1,I                                                
              XMI(I2)=0.0                                                 
  101      CONTINUE                                                     
              XM=0.0                                                      
                                                                        
C 105   DO 107 I1=1,N1                                                  
C 106      READ(NSSD,4000,END=908,ERR=909) RC80                         
C             IF(RC80(1:2).NE.'J ') GO TO 106                           
C 107   CONTINUE                                                        
                                                                        
  105   REWIND(NSSD)

           JD80=1                                                       
        DO 120 I1=1,NJ                                                
  111      READ(NSSD,4000,END=910,ERR=911) RC80                         
              IF(RC80(1:2).NE.'J ') GO TO 111                           
           READ(RC80,4003,ERR=912) ISSS(I1),XPOS,XMJ(I1)                
           READ(NSSD,4004,END=913,ERR=914)(ISFS(II1),II1=1,I)           
                                                                        
           DO 115 I2=1,I,20                                             
              WRITE(ND80,REC=JD80,ERR=915)(ISFS(II1),II1=I2,I2+19)      
              JD80=JD80+1                                               
  115      CONTINUE                                                     
                                                                        
           IF(IOPT.EQ.1) GO TO 119                                      
           DO 118 I2=1,I                                                
              XMI(I2)=XMI(I2)+REAL(ISFS(I2))                            
  118      CONTINUE                                                     
           XM=XM+XMJ(I1)                                                
           GO TO 120                                                    
  119      XSTD(I1)=(PR*(1-PR)*ISSS(I1))**0.5                           
  120   CONTINUE                                                        
                                                                        
           IF(IOPT.EQ.1) GO TO 122                                      
           DO  I2=1,I                                                
              XMI(I2)=XMI(I2)/NJ
           End do 
           XM=XM/NJ                                                     
                                                                        
  122   CALL TRD80(ND80,ND81,ISFT,IDM2,I,NJ,IRC)                        
        IF(IRC.EQ.0) GO TO 125                                          
        IRC=IRC+40                                                      
        GO TO 200                                                       
                                                                        
  125   IF(CCTY.NE.'U') GO TO 127                                       
        REWIND(NSSD,ERR=916)                                            
  126   READ(NSSD,4000,END=917,ERR=918) RC80                            
           IF(RC80(1:2).NE.'C ') GO TO 126                              
        BACKSPACE(NSSD,ERR=919)                                         
  127   CALL CESSC(NSSD,ND81,MSEQ,IDM3,CCTY,LS,LE,I,IRAN,IRC)             
        IF(IRC.EQ.0) GO TO 128                                          
        IRC=IRC+50                                                      
        GO TO 200                                                       
                                                                        
  128   IF(CCTY.NE.'U') GO TO 129                                       
        WRITE(RC80(22:27),'(I6)',ERR=920) I                             
        WRITE(NOUT,4000,ERR=921) RC80                                   
  129   WRITE(NOUT,4005,ERR=922)                                        
C***********************************************************************
C       SECTION III : Selection of over- and/or under-represented       
C                     signals.                                          
C----------------------------------------------------------------------*
                                                                        
           JD81=1                                                       
              JD80=1                                                    
        DO 180 I1=1,I                                                   
           READ(ND81,REC=JD81,ERR=923)(MSEQ(II1),II1=1,LE)              
           JD81=JD81+1                                                  
                                                                        
           DO 135 I2=2,NJ+1,20                                          
              READ(ND80,REC=JD80,ERR=924)(ISFS(II1),II1=I2,I2+19)       
              JD80=JD80+1                                               
  135      CONTINUE                                                     
                                                                        
           XNOR(1)=0                                                    
           XNOR(NJ+2)=0                                                 
           XB1=MB1+REAL(LC-1)/2                                         
                                                                        
           DO 140 I2=2,NJ+1                                             
              IF(IOPT.EQ.1) GO TO 139                                   
              YMJ=XMJ(I2-1)*XMI(I1)/XM
              YSTD=(YMJ*(ISSS(I2-1)-YMJ)/ISSS(I2-1))**0.5                   
  139         IF(IOPT.EQ.1) XNOR(I2)=(ISFS(I2)-ISSS(I2-1)*PR)/XSTD(I2-1)    
              IF(IOPT.EQ.2) XNOR(I2)=(ISFS(I2)-YMJ)/YSTD                
              IF(IOPT.EQ.3) XNOR(I2)=(ISFS(I2)-YMJ)/YMJ
C             Write(6,'(I8,5F10.3)')
C    *           ISFS(I2), XMJ(I2),XMI(I1),XM,YMJ,XNOR(I2)
  140      CONTINUE                                                     
                                                                        
C          IF(CSPT.NE.'G') GO TO 141                                    
C          ISWI=0                                                       
C          YNOR=0                                                       
  141      IF(COPT.EQ.'-') GO TO 160                                    
C-----------------------------------------------------------------------
           IF(CSPT.NE.'A') GO TO 146                                    
           DO 145 I2=2,NJ+1                                             
              IF(XNOR(I2).LT.XCUT) GO TO 145                            
              XPOS=XB1+(I2-2)*LD                                        
              XPC=REAL(ISFS(I2))/REAL(ISSS(I2-1))                         
              J=I2-1                                                    
              IF(XPC.EQ.1) XPC=.9999                                    
              If(IOPT.EQ.1) then 
		 WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20),J,XPOS       
     *           ,ISSS(I2-1),XMJ(I2-1),ISFS(I2),XPC,XNOR(I2)                           
	      Else
		 WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20),J,XPOS       
     *           ,ISSS(I2-1),YMJ,ISFS(I2),XPC,XNOR(I2)                           
	      End if
  145      CONTINUE                                                     
           GO TO 160                                                    
C-----------------------------------------------------------------------
  146      IF(CSPT.NE.'G') GO TO 151                                    
           ISWI=0                                                       
           YNOR=0                                                       
           DO 150 I2=2,NJ+1                                             
              IF(XNOR(I2).LT.XCUT.OR.XNOR(I2).LE.YNOR) GO TO 150        
C             IF(ISWI.EQ.1) BACKSPACE(NOUT,ERR=926)                     
              YNOR=XNOR(I2)                                             
              XPOS=XB1+(I2-2)*LD                                        
              XPC=REAL(ISFS(I2))/REAL(ISSS(I2-1))                         
              J=I2-1                                                    
              IF(XPC.EQ.1) XPC=.9999                                    
C             WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20),J,XPOS       
C    A,ISSS(I2-1),XMJ(I2-1),ISFS(I2),XPC,XNOR(I2)                           
              ISWI=1                                                    
  150      CONTINUE                                                     
           IF(ISWI.EQ.1) WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20)
     A,J,XPOS,ISSS(J),XMJ(J),ISFS(J+1),XPC,XNOR(J+1)                           
           GO TO 160                                                    
C-----------------------------------------------------------------------
  151      DO 155 I2=2,NJ+1                                             
              IF(XNOR(I2).LT.XCUT) GO TO 155                            
              IF(XNOR(I2).LT.XNOR(I2-1).OR.XNOR(I2).LE.XNOR(I2+1))      
     A GO TO 155                                                        
              XPOS=XB1+(I2-2)*LD                                        
              XPC=REAL(ISFS(I2))/REAL(ISSS(I2-1))                         
              J=I2-1                                                    
              IF(XPC.EQ.1) XPC=.9999                                    
              WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20),J,XPOS       
     A,ISSS(I2-1),XMJ(I2-1),ISFS(I2),XPC,XNOR(I2)                           
  155      CONTINUE                                                     
C-----------------------------------------------------------------------
  160      IF(COPT.EQ.'+') GO TO 180                                    
C-----------------------------------------------------------------------
           IF(CSPT.NE.'A') GO TO 166                                    
           DO 165 I2=2,NJ+1                                             
              IF(XNOR(I2).GT.-XCUT) GO TO 165                           
              XPOS=XB1+(I2-2)*LD                                        
              XPC=REAL(ISFS(I2))/REAL(ISSS(I2-1))                         
              J=I2-1                                                    
              IF(XPC.EQ.1) XPC=.9999                                    
              WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20),J,XPOS       
     A,ISSS(I2-1),XMJ(I2-1),ISFS(I2),XPC,XNOR(I2)                           
  165      CONTINUE                                                     
           GO TO 180                                                    
C-----------------------------------------------------------------------
  166      IF(CSPT.NE.'G') GO TO 171                                    
           ISWI=0                                                       
           YNOR=0                                                       
           DO 170 I2=2,NJ+1                                             
              IF(XNOR(I2).GE.-XCUT.OR.-XNOR(I2).LE.YNOR) GO TO 170      
C             IF(ISWI.EQ.1) BACKSPACE(NOUT,ERR=925)                     
              YNOR=-XNOR(I2)                                            
              XPOS=XB1+(I2-2)*LD                                        
              XPC=REAL(ISFS(I2))/REAL(ISSS(I2-1))                         
              J=I2-1                                                    
              IF(XPC.EQ.1) XPC=.9999                                    
C             WRITE(NOUT,4006,ERR=926)(MSEQ(II1),II1=1,20),J,XPOS       
C    A,ISSS(I2-1),XMJ(I2-1),ISFS(I2),XPC,XNOR(I2)                           
              ISWI=1                                                    
  170      CONTINUE                                                     
           IF(ISWI.EQ.1) WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20)
     A,J,XPOS,ISSS(J),XMJ(J),ISFS(J+1),XPC,XNOR(J+1)                           
           GO TO 180                                                    
C-----------------------------------------------------------------------
  171      DO 175 I2=2,NJ+1                                             
              IF(XNOR(I2).GT.-XCUT) GO TO 175                           
              IF(XNOR(I2).GT.XNOR(I2-1).OR.XNOR(I2).GE.XNOR(I2+1))      
     A GO TO 175                                                        
              XPOS=XB1+(I2-2)*LD                                        
              XPC=REAL(ISFS(I2))/REAL(ISSS(I2-1))                         
              J=I2-1                                                    
              IF(XPC.EQ.1) XPC=.9999                                    
              WRITE(NOUT,4006,ERR=925)(MSEQ(II1),II1=1,20),J,XPOS       
     A,ISSS(I2-1),XMJ(I2-1),ISFS(I2),XPC,XNOR(I2)                           
  175      CONTINUE                                                     
C-----------------------------------------------------------------------
                                                                        
  180      CONTINUE                                                     
                                                                        
  200   CLOSE(ND81,ERR=904)                                             
  201   CLOSE(ND80,ERR=903)                                             
  203   CLOSE(NSSD,ERR=901)                                             
                                                                        
  220   IF(IRC.NE.0) Write(NERR,1220) IRC                                     
 1220   FORMAT('Error termination (RC=',I3,').')                       
        STOP                                                            
                                                                        
  926   IRC=IRC+1                                                       
  925   IRC=IRC+1                                                       
  924   IRC=IRC+1                                                       
  923   IRC=IRC+1                                                       
  922   IRC=IRC+1                                                       
  921   IRC=IRC+1                                                       
  920   IRC=IRC+1                                                       
  919   IRC=IRC+1                                                       
  918   IRC=IRC+1                                                       
  917   IRC=IRC+1                                                       
  916   IRC=IRC+1                                                       
  915   IRC=IRC+1                                                       
  914   IRC=IRC+1                                                       
  913   IRC=IRC+1                                                       
  912   IRC=IRC+1                                                       
  911   IRC=IRC+1                                                       
  910   IRC=IRC+1                                                       
  909   IRC=IRC+1                                                       
  908   IRC=IRC+1                                                       
  907   IRC=IRC+1                                                       
  906   IRC=IRC+1                                                       
  905   IRC=IRC+5                                                       
        GO TO 200                                                       
  904   IRC=4                                                           
        GO TO 201                                                       
  903   IRC=3                                                           
        GO TO 203                                                       
  902   IRC=2                                                           
        GO TO 203                                                       
  901   IRC=1                                                           
        GO TO 220                                                       
  999   IRC=999                                                         
        GO TO 200                                                       
                                                                        
 1099   FORMAT('Input not accepted; try again !')                      
                                                                        
 2001   FORMAT(BN,I5)                                                   
 2002   FORMAT(A1)                                                      
 4000   FORMAT(A80)                                                     
 4001   FORMAT('F ',A7,3X,I6,I6,I6,1X,A1,1X,A50)
 4002   FORMAT('S ',A1,':X=',I3,' L=',I3,' I=',I6,' R=',F5.4,
     A ' W=',I5,' S=',I5,' F=',I3,' Y=',I3,' J=',I6,' P=',F6.4)
 4003   FORMAT(3X,6X,3X,I6,3X,F9.1,3X,F10.3,3X,E10.4)
 4004   FORMAT(10I8)                                                    
 4005   FORMAT('* ------->SORT * {D/A}   26--31   35-----43'
     A,'   47--52   56------65   69----76 78-82   86------95')
 4006   FORMAT('I ',20A,' J=',I6,' P=',F9.1,' N=',I6,' M=',F10.3         
     A,' F=',I8,' ',F5.4,' S=',F10.3)                                    
        END                                                             

        Include 'RESSD.f'
        Include 'INC12.f'
        Include 'READX.f'
        Include 'TRD80.f'
        Include 'CESSC.f'
        Include 'ran2.f'

