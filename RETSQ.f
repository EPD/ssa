C       Subroutine RETSQ   : Created 05/28/90 ; last update 08/01/91    
C       *--------------------------------------------------------------*
C       Author    : Philipp Bucher                                       
C       Function  : Retrieval of SQ segment from EMBL/SSA-2 database         
C       Included  : Logical Function LDBSQ
C       Arguments : NPTR (RW) : Logical unit number for pointer file
C                 : NSEQ (RW) : Logical unit number of db sequence file
C                 : NREF (RW) : Logical unit number of db reference file
C                   FDBE (R-) : Database sequence identifier
C                   LSEQ (RW) : Length of database sequence
C                   ITY  (R-) : Type of sequence (linear or circular)
C                   IPOS (RW) : Starting position of requested segment
C                               in database sequence
C                   LM   (R-) : Length of requested sequence segment
C                   CSEQ (-W) : Character array for internal sequence
C                               storage 
C                   IDM1 (R-) : Dimension of CSEQ
C                   IST  (R-) : Destination (starting position) in CSEQ
C                   CC   (-W) : Case (U/L) of sequence characters
C                   IRC  (-W) : Return code                    
C       *--------------------------------------------------------------*
C                                                                       
        Subroutine RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LSEQ,ITY,IPOS,LM,CSEQ,IDM1,IST,CC,IRC)
 
        Character         CSEQ(IDM1)
        Character*(*)     FDBE

        Character*80      FDBF
        Character*80      SQID
        Character*80      FNAM
        Logical           LPTR
      
        Logical           LOCSQ
        Character*80      FSEQ
        Character*80      FDOC


        Integer*8         IBS
        Integer*8         IBD
        Integer*8         IPOS
        Integer*8         IPOT
        Integer*8         LO
        Integer*8         LM
        Integer*8         LN
        Integer*8         LSEQ

        Logical           LDBSQ  

        Character*01      FMT
        Character*01      CC 

        Integer           RCL

	Integer           INULL	
        Character*03      Mode
        Character*02      CNULL
	Equivalence       (INULL,CNULL)
	Integer		  Istatus

        CNULL=' '
	INULL=0

        IRC=0

        If(IST.LE.0) go to 901
        If(LM.LT.0.OR.IST+LM-1.GT.IDM1) go to 902
C        If(FDBE.EQ.FDBF.AND.LPTR) go to  10

        Write(NERR, *) 'RETSQ::'

* Parse sequence reference     

        Write(NERR, *) 'FNAM Before PSQID ', FNAM
        Write(NERR, *) 'SQID Before PSQID ', SQID
        Call PSQID(NPTR,FDBE,LPTR,FNAM,SQID,IRC)
        If(.NOT.LPTR) then 
           Call RTSQF(NSEQ,FNAM,ITY,IPOS,LM,LSEQ,CSEQ,IDM1,IST,IRC)
           If(IRC.NE.0) IRC=IRC+50
           CC='U'
           Go to  101 
        End if 

* Open DB sequence file  
        Write(NERR, *) 'FNAM after PSQID ', FNAM
        Write(NERR, *) 'SQID after PSQID ', SQID
        Write(NERR,*)'Call LOCSQ from RETSQ'
        If(.NOT.LOCSQ(NPTR,FNAM,SQID,LSEQ,FMT,FSEQ,FDOC,RCL,IBS,IBD,CC))
     *     go to 903
        Write(NERR, *) 'FNAM after LOCSQ ', FNAM
        Write(NERR, *) 'SQID after LOCSQ ', SQID
	Mode='r'//CNULL
	Call cOpen(NSEQ, FSEQ, Mode, Istatus)
	If (Istatus.NE.0) goto 904

* Retrieve segment from circular sequence 

   10   If(ITY.EQ.1) go to  50  

        If(LM.EQ.0) LM=LSEQ
        N1=IST+LM-1
        If(N1.GT.IDM1) go to 905 
        
        Do 30 I1=IST,N1
           CSEQ(I1)=' '
   30   Continue
 
        IPOS=IPOS-(IPOS-1)/LSEQ*LSEQ
        If(IPOS.LE.0) IPOS=IPOS+LSEQ
 
        LN=LM
        If(LN.GT.LSEQ) LN=LSEQ
 
        LO=IPOS+LN-1-LSEQ       
        If(LO.LE.0) go to  40
        IPOT=1
        ISU=IST+LN-LO
        If(.NOT.LDBSQ(NSEQ,LPTR,FMT,RCL,IBS,IPOT,LO,CSEQ,ISU))
     *     go to 906       
        LN=LN-LO
 
   40   If(.NOT.LDBSQ(NSEQ,LPTR,FMT,RCL,IBS,IPOS,LN,CSEQ,IST))
     *     go to 907 

        If(LM.LE.LSEQ) go to  99
 
* Expand sequence with tandem repeats

        N1=(LM-1)/LSEQ*LSEQ

        Do 45 I1=LSEQ,N1-LSEQ,LSEQ
           Do 44 I2=IST,IST+LSEQ-1
              CSEQ(I1+I2)=CSEQ(I2)
   44      Continue
   45   Continue
 
        Do 49 I1=IST,IST+LM-N1-1
           CSEQ(N1+I1)=CSEQ(I1)
   49   Continue
 
        Go to  99
 
* Retrieve segment from linear sequence              
    
   50   If(LM.NE.0) go to 55
        LM=LSEQ-IPOS+1
 
   55   N1=IST+LM-1
        If(N1.GT.IDM1) go to 908 
 
        Do 60 I1=IST,N1
           CSEQ(I1)=' '
   60   Continue
 
        If(IPOS.GT.LSEQ)   go to  99
        If(IPOS+LM-1.LE.0) go to  99        
 
        LN=LM
        ISU=IST
        IPOT=IPOS
        If(IPOS.GE.1) go to 65
        IPOT=1
        ISU=IST+1-IPOS
        LN=LM-ISU+IST
 
   65   If(IPOT+LN-1.GT.LSEQ) LN=LSEQ-IPOT+1
   
        If(.NOT.LDBSQ(NSEQ,LPTR,FMT,RCL,IBS,IPOT,LN,CSEQ,ISU))
     *     go to 909
        Go to  99 

   70   Continue
 
   99   FDBF=FDBE
  100   Call cClose(NSEQ, Istatus)
        Write(NERR, *) 'FNAM end of RETSQ ', FNAM
        Write(NERR, *) 'SQID end of RETSQ ', SQID
  101   Return
 
  909   IRC=IRC+5 
  908   IRC=IRC+5 
  907   IRC=IRC+5 
  906   IRC=IRC+5 
  905   IRC=IRC+5 
        Go to 100
  904   IRC=IRC+1 
  903   IRC=IRC+1 
  902   IRC=IRC+1 
  901   IRC=IRC+1 
  
        Go to 101
 
        End
 
*----------------------------------------------------------------------*

        Subroutine RTSQF(NSEQ,FNAM,ITY,IPOS,LM,LSEQ,CSEQ,IDM1,IST,IRC)

* If LM.EQ.0 on input, RTSQF tries to read the entire sequence, i.e. from
* IPOS to the 3'end if linear, to IPOS-1 if circular. If there is no space,
* RTSQF transfers as much as possible. If the end of the sequence is not 
* reached during reading, LSEQ=0 is returned. LM always indicicates the
* end of the segment that has been transferred. 

        Character*01      CSEQ(*)
        Character*(*)     FNAM

        Character*256     RCIN

        Character         C
        Byte              B
        Equivalence      (C,B)

        Integer*8         FTell
        Integer           Fgetc
        Integer*8         IB
        Integer*8         IPOS
        Integer*8         LM
        Integer*8         LSEQ

        IRC=0
        LSEQ=0
        JST=IST
        LN=LM
        If(LN.EQ.0) LN=IDM1-IST+1
        
* find beginning of sequence: 
        Open(NSEQ,File=FNAM,Status='OLD',Err=901)
        Rewind(NSEQ)
 
C    2   Read(NSEQ,'(A)',Err=902,End=903) RCIN
    2   Read(NSEQ,'(A)',Err=902) RCIN
        
        If(RCIN( 1: 2).EQ.'SQ'.OR.RCIN( 1: 1).EQ.'>') then 
C           IB=FTell(NSEQ)
           Call cFtell(NSEQ, IB, Istatus)
           If(IB.LE.0) go to 904
           JSQ=0
        Else 
           Go to   2
        End if

        If  (ITY.EQ.1.) then 

* linear sequence

           If     (IPOS+LN-1.LE.0) then

*  - fill row with blanks
 
              Do while(JST.LT.IST+LN)
                 CSEQ(JST)=' '
                 JST=JST+1
              End do 
		 Go to  99

           Else if(IPOS.LE.0) then

*  - fill left side of row with blanks 

              Do while(JST.LE.IST-IPOS)
                 CSEQ(JST)=' '
                 JST=JST+1
              End do 

           Else if(IPOS.GT.0) then

*  - move to starting position in sequence 

              Do while(JSQ+1.LT.IPOS)
                 N1=Fgetc(NSEQ,C) 
                 If(N1.LT.0) then 
                    LSEQ=JSQ
                    If(LM.EQ.0) go to  99 
                    Go to  20
                 End if 
                 If(N1.GT.0) go to 905 
                 If(B.GE.97.AND.B.LE.122) B=B-32
                 If(B.GE.65.AND.B.LE. 90) JSQ=JSQ+1
              End do 
           End if

*  - read in sequence 

           Do while(JSQ.LT.IPOS+LN)
              N1=Fgetc(NSEQ,C) 
              If(N1.LT.0) then 
                 LSEQ=JSQ
                 If(LM.EQ.0) then 
                    LM=JSQ-IPOS+1
                    Go to  99
                 Else 
                    Go to  20
                 End if
              End if 
              If(N1.GT.0) go to 906 
              If(B.GE.97.AND.B.LE.122) B=B-32
              If(B.GE.65.AND.B.LE. 90) then
                 JSQ=JSQ+1
                 CSEQ(JST)=C
                 JST=JST+1
              End if
           End do  
           If(LM.EQ.0.AND.Fgetc(NSEQ,C).GE.0) go to 907

*  - fill right side of row with blanks
                  
   20      Do while(JST.LT.IST+LN)
              CSEQ(JST)=' '
              JST=JST+1
           End do  
 
        Else

* circular sequence

           If(IPOS.LE.0) then 

*  - determine sequence length, adjust IPOS, rewind,

              Do while(LSEQ.EQ.0)
                 N1=Fgetc(NSEQ,C) 
                 If(N1.LT.0) then 
                    LSEQ=JSQ
                    If(LM.EQ.0) then 
                       If(LN.LT.LSEQ) then
                          Go to 908
                       Else
                          LN=LSEQ
                          LM=LSEQ
                       End if
                    End if
C                    Call FSeek(NSEQ,IB,0,*909)
                    Call cFseek(NSEQ,IB,0,Istatus)
                    If (Istatus.NE.0) goto 909
                    JSQ=0
                    IPOS=IPOS-(IPOS-1)/LSEQ*LSEQ
                    If(IPOS.LE.0) IPOS=IPOS+LSEQ 
                 End if 
                 If(N1.GT.0) go to 910 
                 If(B.GE.97.AND.B.LE.122) B=B-32
                 If(B.GE.65.AND.B.LE. 90) JSQ=JSQ+1
              End do 

           End if

*  - move to starting position in sequence 


           Do while(JSQ+1.LT.IPOS)
              N1=Fgetc(NSEQ,C) 
              If(N1.LT.0) then 
                 LSEQ=JSQ
                 If(LM.EQ.0) then 
                    If(LN.LT.LSEQ) then
                       Go to 911
                    Else
                       LN=LSEQ
                       LM=LSEQ
                    End if
                 End if
C                 Call FSeek(NSEQ,IB,0,*912)
                 Call cFseek(NSEQ,IB,0,Istatus)
                 If (Istatus.NE.0) goto 912
                 JSQ=0
                 IPOS=IPOS-(IPOS-1)/LSEQ*LSEQ
                 If(IPOS.LE.0) IPOS=IPOS+LSEQ 
              End if 
              If(N1.GT.0) go to 913 
              If(B.GE.97.AND.B.LE.122) B=B-32
              If(B.GE.65.AND.B.LE. 90) JSQ=JSQ+1
           End do 

*  - read in sequence 

           Do while(JSQ.LT.IPOS+LN)
              N1=Fgetc(NSEQ,C)
              If(N1.LT.0) then 
                 LSEQ=JSQ
                 If(LM.EQ.0) then 
                    If(LN.LT.LSEQ) then
                       Go to 914
                    Else
                       LN=LSEQ
                       LM=LSEQ
                    End if
                 End if
C                 Call FSeek(NSEQ,IB,0,*915)
                 Call cFseek(NSEQ,IB,0,Istatus)
                 If (Istatus.NE.0) goto 915
              End if 
              If(N1.GT.0) go to 916 
              If(B.GE.97.AND.B.LE.122) B=B-32
              If(B.GE.65.AND.B.LE. 90) then
                 JSQ=JSQ+1
                 CSEQ(JST)=C
                 JST=JST+1
              End if
           End do 

        End if

   99   Continue  

  100   Return

  916   IRC=IRC+1
  915   IRC=IRC+1
  914   IRC=IRC+1
  913   IRC=IRC+1
  912   IRC=IRC+1
  911   IRC=IRC+1
  910   IRC=IRC+1
  909   IRC=IRC+1
  908   IRC=IRC+1
  907   IRC=IRC+1
  906   IRC=IRC+1
  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 100

        End         
*----------------------------------------------------------------------*
 
        Logical Function LDBSQ(NSEQ,LPTR,FMT,RCL,IB,IPOS,L,CSEQ,IST)
 
        Character*01      CSEQ(*)
        Character*(*)     FMT
        Integer           RCL
        Logical           LPTR
        Integer*8         IB
        Integer*8         IPOS
        Integer*8         L
        Integer*8         N1
        Integer*8         N2
        Integer*8         N3

        Character*01      JUNK(256)
        Character*256     FASTALINE
	Character*81      EMBLINE

        Integer*8         lsize
        Integer           Istatus
        Integer           eof

        If     (FMT(1:1).EQ.'E') then 
           go to  10
        Else if(FMT(1:1).EQ.'F') then
           go to  20
        Else 
           go to 900
        End if

* EMBL format

   10   N1=60
        N2=(IPOS-1)/N1
        N3=IB+N2*RCL
	N4=(N2+1)*N1-IPOS+1
        Call cFseek(NSEQ,N3,0,Istatus)
        If (Istatus.NE.0) goto 900
        lsize=81
        Call cGetline(NSEQ,EMBLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 900
	Read(EMBLINE,'(4x,6(1x,10A))')
     *       (JUNK(ii1),ii1=1,N1-N4),
     *       (CSEQ(ii1),ii1=IST,MIN(IST+N4-1,IST+L-1))
        Do I1=IST+N4-1,IST+L-1,N1
           Call cGetline(NSEQ,EMBLINE,lsize,Istatus)
           Read(EMBLINE,'(4x,6(1x,10A))')
     *       (CSEQ(ii1),ii1=I1+1,MIN(I1+N1,IST+L-1))
        End do
        LDBSQ=.TRUE.
        go to 100

* FASTA format

   20   If(RCL.GT.256) go to 900
        N1=(RCL-1)/2*2
        N2=(IPOS-1)/N1
        N3=IB+N2*RCL
	N4=(N2+1)*N1-IPOS+1
        Call cFSeek(NSEQ,N3,0,Istatus)
        If (Istatus.NE.0) goto 900
        lsize=256
        Call cGetline(NSEQ,FASTALINE,lsize,Istatus)
        If (Istatus.NE.0) goto 900
        Read(FASTALINE,'(256A)')
     *       (JUNK(ii1),ii1=1,N1-N4),
     *       (CSEQ(ii1),ii1=IST,MIN(IST+N4-1,IST+L-1))
*        Write(6, *) 'First Loop I1= ', IST+N4-1
*        Write(6, *) 'First Loop I2= ', IST+L-1
*        Write(6, *) 'Loop N1= ', N1
*        Write(6, *) 'Position Fseek= ', N3
*        Write(6, *) 'Position N4= ', N4
	Do I1=IST+N4-1,IST+L-1,N1
           Call cGetline(NSEQ,FASTALINE,lsize,Istatus)
           If (Istatus.NE.0) goto 900
           Read(FASTALINE,'(256A)')
     *       (CSEQ(ii1),ii1=I1+1,MIN(I1+N1,IST+L-1))
	End do 

        LDBSQ=.TRUE.
        go to 100

  100   Return
 
  900   LDBSQ=.FALSE.
        Write(6, *) 'Loop I1= ', I1
        Write(6, *) 'Loop L= ',  IST+L-1
        Write(6, *) 'Loop N1= ', N1
        Write(6, *) 'Position Fseek= ', N3
        Write(6, *) 'Position N4= ', N4
        Go to 100
 
        End     
*----------------------------------------------------------------------*
