C@      MAIN PROGRAM CPR: CREATED 10/01/82 ; LAST UPDATE 04/05/10      1
C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER
C       FUNCTION : CREATION OF SIGNAL SEARCH DATA FILE
C       ****************************************************************

        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        PARAMETER(IDM2=524288)                                            
                                                                        
        PARAMETER(NSSD=12)                                              
        Parameter(NOUT=21) 
                                                                        
        INTEGER      ISS (IDM2)                                         
        Real         XPOS(IDM2)
        Real         XM  (IDM2)
        Real         XV  (IDM2)
        Real         CI  (IDM2) 
        
        REAL         RN
                                                                        
        CHARACTER*50 TIT                                                
                                                                        
        FNSSD='SSD'                                                     
        FNOUT='CPR'
                                                                
        IRC=0                                                           
                                                                        
        OPEN(NSSD,FILE=FNSSD,ERR=901)                                   
        OPEN(NOUT,FIle=FNOUT,ERR=901)                                                                        
        Read(NSSD,4002,ERR=901) FIDSM,MB1,MB2,MI,CTRC,TIT              
        Read(NSSD,4003,ERR=901) CCTY,LS,LE,I,RN,LC,LD,LF,LH,NI,PR

        
        K1=1
        YMAX=0
        ISSM=0
    1   Read(NSSD,"(A)",Err=901,End= 10) RC80
           If(RC80( 1: 2).NE.'J ') go to   1  
        Read(RC80,4004,ERR=901) NJ,ISS(K1),XPOS(K1),XM(K1),XV(K1)              
           CI(K1)=REAL(ISS(K1))/(ISS(K1)-1)
           CI(K1)=CI(K1)*
     *       (XV(K1)/XM(K1)/(REAL(ISS(K1))-XM(K1))-1/(REAL(ISS(K1))))
           If(CI (K1).GT.YMAX) YMAX= CI(K1)
           If(ISS(K1).GT.ISSM) ISSM=ISS(K1)
        K1=K1+1
        Go to   1                                                                 
   10   K1=K1-1
        If(K1.EQ.0) go to 100

        Print *,'# h 0.4'
        Print *,'# u 0.4.5'
        Print *,'# r -0.1'
        Print *,'# ltx 0.5'
        Print *,'# lty 1.2'
        Print *,'# lt "Constraint Profile"'

        YMAX=YMAX*1.1
        Print *,'# x ',XPOS( 1),XPOS(K1)
        Print *,'# y 0 ',YMAX

        Do  20 I1=1,K1
           Print *,XPOS(I1),CI(I1)
   20   Continue

        Print *,'# s'
        Print *,'# h 0.2'
        Print *,'# u 0.0'
        Print *,'# r -0.1'
        Print *,'# ltx 0.5'
        Print *,'# lty 1.2'
        Print *,'# lts 1.0'
        Print *,'# lns 1.0'
        Print *,'# lt "# of sequences per window"'

        ISSM=ISSM*1.1
        Print *,'# x ',XPOS( 1),XPOS(K1)
        Print *,'# y 0 ',ISSM

        Do  30 I1=1,K1
           Print *,XPOS(I1),ISS(I1)
   30   Continue

  100   Stop

  901   Print *,'Error termination'
        Go to 100 
                                                                   
 4002   FORMAT(2x,A7,3X,I6,I6,I6,1X,A1,1X,A47)
 4003   FORMAT(2X,A1,3X,I3,3X,I3,3X,I6,3X,F5.4,3X,I5,3X,I5,3X,I3,3X,I3
     A,3X,I6,3X,F6.4)
 4004   FORMAT(3X,I6,3X,I6,3X,F9.1,3X,F10.3,3X,E10.4)
        End 
