        Subroutine FNDEF(NINP,FNAM,CEXT,FDEF,FOUT)

        Character*(*)     FNAM
        Character*(*)     FDEF
        Character*(*)     FOUT
        Character*(*)     CEXT

        Character*64      FINP

        Logical           LOLD

        Integer           Rindex

        If(FNAM.EQ.' ') then 
           Inquire(NINP,Name=FINP,Iostat=IOS)
           If(IOS.EQ.0) then
              L1=Rindex(FINP,'/')+1
              L2=Index(FINP(L1:64),'.')-1
              If(L2.LE.0) then 
                 L2=Lblnk(FINP)
              Else
                 L2=L1+L2-1
              End if  
              FOUT=FINP(L1:L2)
           Else 
              FOUT=FDEF
           End if
        Else 
           FOUT=FNAM 
        End if

        L2=Lblnk(FOUT)
        L1=MAX(1,Rindex(FOUT(1:L2),' '))
        FINP=FOUT(L1:L2)
        FINP=FINP( 1:10)
        FOUT=(FINP(1:Lblnk(FINP)) // CEXT)


        Inquire(File=FOUT,EXIST=LOLD)
        If(LOLD) FOUT=(FDEF(1:Lblnk(FDEF)) // CEXT)

        Return
        End 
