*$      Subroutine OPROFI  : Created 06/08/90 ; last update 06/09/90   *
*       *--------------------------------------------------------------*
*       Author    : Philipp Bucher                                       
*       Function  : Interactive dialogue of program OPROF        
*       Arguments : NERR (R-) : I/O unit number for terminal output 
*                   NINP (R-) : I/O unit number for terminal input 
*                   NFPS (R-) : I/O unit number for FPS input file 
*                   TDSM (-W) : Title of FPS 
*                   MB1  (-W) : Left  (5'OH) border of sequence segments 
*                   MB2  (-W) : Right (3'OH) border of sequence segments 
*                   NWMX (R-) : I/O unit number for WMX input file  
*                   WMX  (-W) : Weight matrix 
*                   IDM1 (R-) : Dimension/maximal length of WMX
*                   IWM1 (-W) : Position of leftmost column with respect 
*                               to "center" of weight matrix 
*                   LWM  (-W) : Length of weight matrix                
*                   SCMI (-W) : Minimal score of weight matrix
*                   CO   (-W) : Cut-off percentage  
*                   CBD  (-W) : Search mode  
*                   LW   (-W) : Window width
*                   LD   (-W) : Window shift 
*                   IRC  (-W) : Return code
*       *--------------------------------------------------------------*

        Subroutine OPROFI(
     *     NERR,NINP,
     *     NFPS,TDSM,MB1,MB2, 
     *     NWMX,WMX,IDM1,IWM1,LWM,SCMI,CO,    
     *     CBD,LW,LD, 
     *     IRC) 
  
        Character*192 RC80
        Character*192 FFPS
        Character*64  FWMX

        Character*55  TDSM 
 
        Character*55  TWMX
        Character*10  AWMX
        Character*02  CWMX

        Real          WMX(IDM1,4)

        Character*01  CBD

        IRC=0

* Beginning of interactive dialogue

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901)
     *    'Program OPROF / Beginning of interactive dialogue:'
        Write(NERR,*,Err=901)
     *    'Press <Return> for default, enter < to go back to ',
     *    'previous question.'       
        Write(NERR,*,Err=901) ' '

* Open FPS file, read Title (TDSM)

   11   FFPS='Fps.fps'
        Write(NERR,1011,Err=901) FFPS( 1: 7)
 1011   Format
     *   ('FPS file (default=',A7,'): ',$)
        Read(NINP,"(A)",Err= 15) RC80
        If(RC80.EQ.' ') go to 12
        Read(RC80,"(A)",Err= 15) FFPS
   12   Open(NFPS,File=FFPS,Status='OLD',Err= 15)
        Go to  16
   15   Write(NERR,1199,Err=901)
        Go to  11

   16   TDSM=' '
   17   Read(NFPS,"(A)",Err=902,End=18) RC80
           If(RC80( 1: 2).EQ.'FP') go to  18
           If(RC80( 1: 2).EQ.'TI') go to  19
           Go to  17 
   18   RC80(16:70)='Unnamed functional position set' 

   19   Rewind(NFPS,Err=903)
        TDSM=RC80(16:70)

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901) TDSM 
        Write(NERR,*,Err=901) ' '


* 5' border (MB1)

           
   21   MB1=-99
        Write(NERR,1021,Err=901) MB1
 1021   Format
     *   ('Left (5''OH) border (default=',I5,'): ',$)         
        Read(NINP,"(A)",Err= 29) RC80                               
        If(RC80.EQ.' ') go to 22                                  
        If(RC80.EQ.'<') go to 11
C       Read(RC80,"(I)",Err= 29) MB1                                      
        Read(RC80,*,Err= 29) MB1                                      

   22   Go to  31

   29   Write(NERR,1199,Err=901)
        Go to  21 

* 3' border (MB2)

   31   MB2=MB1+199
        Write(NERR,1031,Err=901) MB2
 1031   Format
     *   ('Right (3''OH) border (default =',I5,'): ',$)      
        Read(NINP,"(A)",Err= 39) RC80                               
        If(RC80.EQ.' ') go to  32
        If(RC80.EQ.'<') go to  21
C       Read(RC80,"(I)",Err= 39) MB2                                      
        Read(RC80,*,Err= 39) MB2                                      

   32   If(MB2.LE.MB1) go to  39                                        
        LM=MB2-MB1+1
        Go to  41 

   39   Write(NERR,1199,Err=901)
        Go to  31

* Weight matrix (AWMX,TWMX,CWMX,WMX,IWM1,LWM)

   41   FWMX='Wmx.wmx'
        Write(NERR,1041,Err=901) FWMX( 1: 7)
 1041   Format
     *   ('WMX file (default=',A7,' enter @ for terminal iput): ',$)
        Read(NINP,"(A)",Err= 49) RC80               
        If(RC80.EQ.' ') go to  42
        If(RC80.EQ.'<') go to  31

        If(RC80( 1: 1).EQ.'@') then
           NWMY=5
           Go to  45
        End if
        FWMX=RC80( 1:64) 

   42   NWMY=NWMX
        Open(NWMY,File=FWMX,Status='OLD',Err= 49)

   45   Call INWMX
     *    (NWMY,AWMX,TWMX,CWMX,WMX,IDM1,IWM1,LWM,SCMI,CO,XCO,IRC)
        If(IRC.EQ.0) go to  51       

   49   Write(NERR,1199,Err=901)
        Go to  41

* Cut-off % (CO)
 
   51   Write(NERR,1051,Err=901) CO
 1051   Format
     *   ('Initial cut-off % (default=',F4.1,'%): ',$)
        Read(NINP,"(A)",Err= 59) RC80               
        If(RC80.EQ.' ') go to  52
        If(RC80.EQ.'<') go to  41

        RC80(80:80)='@'
        Read(RC80,*,Err= 52) CO
   52   If(CO.LE.0.OR.CO.GT.100) go to  51
        Go to  61

   59   Write(NERR,1199,Err=901) 
        Go to  51

* Window width (LW) 

   61   LW=LWM
        Write(NERR,1061,Err=901) LW
 1061   Format
     *   ('Window width (default=',I3,'): ',$)
        Read(NINP,"(A)",Err= 69) RC80         
        If(RC80.EQ.' ') go to  62 
        If(RC80.EQ.'<') go to  51

        RC80(80:80)='@'
        Read(RC80,*,Err= 69) LW 
       
   62   If(LW.LT.LWM)      go to  69 
        if(LW.GT.LM )      go to  69
        Go to  71

   69   Write(NERR,1199,Err=901)
        Go to  61

* Window shift (LD)
 
   71   LD=(LW-LWM+2)/2 
        Write(NERR,1071,Err=901) LD 
 1071   Format
     *   ('Window shift (default=',I3,'): ',$)
        Read(NINP,"(A)",Err= 79) RC80         
        If(RC80.EQ.' ') go to  72 
        If(RC80.EQ.'<') go to  61

        RC80(80:80)='@'
        Read(RC80,*,Err= 79) LD 
   72   Go to  81
      
   79   Write(NERR,1199,Err=901)
        Go to  71

* Search mode (CBD)
 
   81   CBD='U'
        Write(NERR,1081,Err=901) CBD
 1081   Format
     *   ('Search mode (B or U, default=',A1,'): ',$) 
        Read(NINP,"(A)",Err= 89) RC80               
        If(RC80.EQ.' ') go to  82
        If(RC80.EQ.'<') go to  71

        Read(RC80,"(A)",Err= 81) CBD
   82   If(CBD.EQ.'u') CBD='U'
        If(CBD.EQ.'b') CBD='B'
        If(CBD.EQ.'U'.OR.CBD.EQ.'B') go to  91 

   89   Write(NERR,1199,Err=901)
        Go to  81

   91   Continue

* End of interactive dialogue 

  200   Return 
      
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 200
        
 1199   Format('Input not accepted. Try again !')
 
        End

*----------------------------------------------------------------------*

        Include           'INWMX.f' 
        Include           'LOCSQ.f' 
        Include           'PSQID.f' 
        Include           'lblnk.f'
