        Subroutine INWMX
     *    (NWMX,AWMX,TWMX,CWMX,WMX,IDM1,IWM1,LWM,SCMI,CO,XCO,IRC)

        Parameter        (IDM2= 64)

        Character*80      RC80

        Character*55      TWMX
        Character*10      AWMX
        Character*02      CWMX

        Real              WMX(IDM1,4)
        Real              WMY(IDM2,4)
        Integer           IPOS(IDM2)

        Logical           PERC

* Initialize variables, define defaults

        IRC=0

        PERC=.true.
        CO=50

        AWMX='WMX'
        TWMX='Unnamed weight matrix'
        CWMX='WM'

* Read Input file (-> WMY)

           K1=1
    1   Read(NWMX,"(A)",Err=901,End= 10) RC80

* TI-line

        If     (RC80( 1: 2).EQ.'TI') then

            Read(RC80,"(5x,A10,A55,A2)",Err=902) AWMX,TWMX,CWMX
            If(AWMX.EQ.' ') AWMX='WMX'
            If(TWMX.EQ.' ') TWMX='Unnamed weight matrix'
            If(CWMX.EQ.' ') CWMX='WM'
            K1=1
* WM-line

        Else if(RC80( 1: 2).EQ.'WM') then

           If(K1.GT.IDM2) go to 903
           RC80(80:80)='@'
           Read(RC80( 3:80),*,Err=  2)
     *        IPOS(K1),(WMY(K1,ii1),ii1=1,4)
    2      Continue 
           K1=K1+1

* CO-line

        Else if(RC80( 1: 2).EQ.'CO') then 

           RC80(80:80)='@'
           IP=Index(RC80,'%')    
           If(IP.EQ.0) then 
              Read(RC80( 3:80),*,Err=  3) XCO
              PERC=.false.
           Else
              RC80(IP:IP)=' '
              Read(RC80( 3:80),*,Err=  3) CO
              PERC=.true.
           End if
    3      Continue 

* End of file

        Else if(RC80( 1: 2).EQ.'//'.OR.
     *          RC80( 1: 2).EQ.'YZ'.OR.
     *          RC80( 1: 1).EQ.'.' ) then
           Go to  10
        Else
           Go to   1 
        End if
           Go to   1

   10   If(K1.LE.1) go to 904 
        K1=K1-1

* Define range of weight matrix

           IWM1=IPOS(1) 
           IWM2=IPOS(1) 

        Do  19 I1=2,K1        
           If(IPOS(I1).LT.IWM1) IWM1=IPOS(I1)  
           If(IPOS(I1).GT.IWM2) IWM2=IPOS(I1)  
   19   Continue

        LWM=IWM2-IWM1+1
        If(LWM.GT.IDM1) go to 905

* re-initialize WMX

        Do  29 I1=1,K1
           Do  28 I2=1,4
              WMX(I1,I2)=0
   28      Continue
   29   Continue
 
* Move matrix WMY -> WMX  

        Do  39 I1=1,K1
           J1=IPOS(I1)-IWM1+1
           Do  38 I2=1,4
              WMX(J1,I2)=WMY(I1,I2)
   38      Continue
   39   Continue

* Normalize weight matrix

        SCMI=0
        Do  49 I1=1,LWM
           SCMI=SCMI+MIN(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4)) 
           Y=MAX(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4)) 
           SCMI=SCMI-Y 
           Do  48 I2=1,4
              WMX(I1,I2)=WMX(I1,I2)-Y
   48      Continue
           XCO=XCO-Y
   49   Continue

* Calculate CO from XCO

        If(PERC) then
 
           If(CO.LT.0.OR.CO.GT.100) CO=50
           XCO=SCMI-(SCMI*CO/100)

* Calculate XCO from CO

        Else

           If(XCO.LT.SCMI.OR.XCO.GT.0) XCO=SCMI/2
           CO=100-(XCO/SCMI)*100

        End if 

* Done 

  100   Return

  905   IRC=IRC+1 
  904   IRC=IRC+1 
  903   IRC=IRC+1 
  902   IRC=IRC+1 
  901   IRC=IRC+1 
        Go to 100     
        
        End
