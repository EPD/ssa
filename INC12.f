C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : INTERACTIVE INPUT OF MB1 & MB2 FOR SIGNAL SEARCH DATA
C       ****************************************************************
                                                                        
        SUBROUTINE INC12(NERR,NINP,TIT,NB1,NB2,MB1,MB2,LC,LD,N1,NJ,IRC)           
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        CHARACTER*50 TIT                                                
                                                                        
        IRC=0                                                           
        NB2=NB1+(NB2-NB1-LC+1)/LD*LD+LC-1                               
                                                                        
   10   MB1=NB1                                                         
        Write(NERR,1010) MB1                                                  
 1010   FORMAT('Left  (5''OH) border (Default is',I5,'): ',$)     
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 20                                   
        READ(RC80,2000,ERR=18) MB1                                      
        IF(MB1.GE.NB1.AND.MB1.LE.NB2-LC+1) GO TO 20                     
   18   PRINT 1099                                                      
        GO TO 18                                                        
                                                                        
   20   MB2=NB2                                                         
        Write(NERR,1020) MB2                                                  
 1020   FORMAT('Right (3''OH) border (Default is',I5,'): ',$)    
        READ(NINP,4000,ERR=999,END=999) RC80                               
        IF(RC80.EQ.' ') GO TO 30                                   
        READ(RC80,2000,ERR=28) MB2                                      
        IF(MB2.LE.NB2.AND.MB2.GE.(NB1+LC-1)) GO TO 30                   
   28   PRINT 1099                                                      
        GO TO 20                                                        
   30   IF(MB2.LT.(MB1+LC-1)) GO TO 18                                  
                                                                        
        N1=(MB1-NB1)/LD                                                 
        MB1=NB1+N1*LD                                                   
        NJ=(MB2-MB1-LC+1)/LD+1                                          
        MB2=MB1+(NJ-1)*LD+LC-1                                          
  100   RETURN                                                          
                                                                        
 1099   FORMAT(                                                         
     A 'Input not accepted. Try again ! Remember: input file ',/       
     A,'contains data from pos. ',I5,' to ',I5,', window width'        
     A,'is ',I3,'.')                                                   
                                                                        
  901   IRC=1                                                           
        GO TO 100                                                       
  999   IRC=999                                                         
        GO TO 100                                                       
                                                                        
 2000   FORMAT(BN,1I6)                                                  
 4000   FORMAT(A80)                                                     
                                                                        
        END                                                             
