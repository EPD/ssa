C@      MAIN PROGRAM CRSSD : CREATED 10/01/82 ; LAST UPDATE 04/05/10   1
C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : CREATION OF SIGNAL SEARCH DATA FILE                  
C       ****************************************************************
                                                                        
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        PARAMETER(IDM1=201326592)                                           
        PARAMETER(IDM2=524288)                                            
        PARAMETER(IDM3=64)                                              
        PARAMETER(IDMV=64)                                              
                                                                        
        PARAMETER(NERR= 0)                                              
        PARAMETER(NINP= 5)                                              
        PARAMETER(NOUT= 6)                                              

        PARAMETER(NLSQ=11)                                              
        PARAMETER(NPTR=12)                                              
        PARAMETER(NFPS=13)                                              
        PARAMETER(NSEQ=14)                                              
        PARAMETER(NREF=15)                                              
        PARAMETER(NDSM=16)                                              
        PARAMETER(NSSC=17)                                              
        PARAMETER(ND80=18)                                              
        PARAMETER(ND81=19)                                              

                                                                        
        CHARACTER    IDSM(IDM1)                                         
        INTEGER      ISF1(IDM1)                                         
        INTEGER      ISF2(IDM1)                                         
        INTEGER      ISSS(IDM2)                                         
        CHARACTER    MSEQ(IDM3)                                         
        REAL         VPR (IDMV)                                         
        REAL         RN

	Integer*8    LN
                                                                        
        CHARACTER*55 TIT                                                
                                                                        
        FNDSM='DSM'                                                     
        FNSSC='SSC'                                                     
        FND80='D80'                                                     
        FND81='D81'                                                     
                                                                        
        IRC=0                                                           
	IRAN=-123456789
	CC='L'
                                                                        
        OPEN(NSSC,FILE=FNSSC,ERR=902)                                   
        OPEN(ND80,
     *     STATUS='SCRATCH',ACCESS='DIRECT',RECL=80,ERR=904)           
        OPEN(ND81,
     *     STATUS='SCRATCH',ACCESS='DIRECT',RECL=80,ERR=905)           
                                                                        
C***********************************************************************
C       SECTION   I : Input of DSM data-definition lines and signal     
C       search parameters.                                              
C----------------------------------------------------------------------*
                                                                        
        CALL INSSP(NINP,NERR,NDSM,NSSC,MB1,MB2,CCTY,LS,LE,I,RN,LC,LD,
     *    LF,LH,PR,VPR,IDM3,IRC)                                                    
        IF(IRC.EQ.0) GO TO 2                                            
        IF(IRC.EQ.1) IRC=0                                              
        IF(IRC.GT.1) IRC=IRC+30                                         
        GO TO 100                                                       
                                                           
    2   LM=MB2-MB1+1 
        QN=RN
    3   READ(NDSM,4000,END=  4,ERR=908) RC80
           IF(RC80( 1: 2).EQ.'FP') THEN 
              Go to   4 
           ELSE IF(RC80(1:2).EQ.'TI') THEN
              READ(RC80,"(5x,A7,3x,A55)",ERR=909) FIDSM,TIT
              Go to 7
           ELSE 
              GO TO 3          
           END IF 
 
    4   FIDSM='FPS'
        TIT='Unnamed functional position set'
    7   MI=0                                            
        CH='F'
        NB1=MB1
        NB2=MB2
        REWIND(NDSM,ERR=906)
    8   READ(NDSM,4000,END=9,ERR=910) RC80                              
           IF(RC80(1:2).NE.'FP') GO TO 8                                
        MI=MI+1                                                         
        GO TO 8                                                         
                                                                        
    9   REWIND(NDSM)
        IF(IDM1.LT.LC*MI) GO TO 911                                     
        NI=1+(LM-LC)/LD                                                 
                                                                        
C***********************************************************************
C       SECTION  II : Generation of signal search data by sequential    
C       processing of column portions of DSM matrix.                    
C----------------------------------------------------------------------*

   10   IOPT=1                                                          
        XPOS=MB1+REAL(LC-1)/2                                           
        N1=(((IDM1/MI)-LC)/LD)*LD                                       
        LN=N1+LC                                                        
        ND=N1+LD                                                        
        NJ=1+(LN-LC)/LD                                                 
                                                                        
        N1=MB1-ND                                                       
        NK=0-NJ                                                         
   11   N1=N1+ND                                                        
        NK=NK+NJ                                                        
        IF(N1+LC-1.GT.MB2) GO TO 100                                    
        IF(N1+LN+1.LE.MB2) GO TO  12                                    
        LN=MB2-N1+1                                                     
        NJ=1+(LN-LC)/LD                                                 
                                                                        
   12   I1=N1+LN-1                                                      
        CTRC='D'
        CALL CEDSM(NLSQ,NDSM,NPTR,NSEQ,NREF,IDSM,IDM1,MI,LN,N1,CC,IRC)                    
        IF(IRC.EQ.0) GO TO 14                                           
        IRC=IRC+40                                                      
        GO TO 100                                                       
                                                                        
   14   JD81=1                                                          
        Write(NERR,*) ' '
        IF(QN.EQ.-1) I=0                                                
        IF(CCTY.EQ.'U') REWIND(NSSC,ERR=913)                            
        CALL CESSC(NSSC,ND81,MSEQ,IDM3,CCTY,LS,LE,I,IRAN,IRC)             
        IF(IRC.EQ.0) GO TO 15                                           
        IRC=IRC+70                                                      
        GO TO 100                                                       
                                                                        
   15   DO 20 I1=1,(MI*LN),LN                                           
           DO 19 I2=I1,I1+LN-LC,LD                                      
              ISF2(I2)=-1                                               
              DO 18 I3=I2,I2+LC-1                                       
                 IF(IDSM(I3).EQ.'N') GO TO 19                           
   18         CONTINUE                                                  
              ISF2(I2)=0                                                
   19      CONTINUE                                                     
   20   CONTINUE                                                        
                                                                        
        JD80=1                                                          
	J1=0
        DO 30 I1=1,LN-LC+1,LD                                           
           J1=J1+1                                                      
           ISSS(J1)=0                                                   
           DO 29 I2=I1,(MI*LN),LN                                       
              IF(ISF2(I2).NE.-1) ISSS(J1)=ISSS(J1)+1                    
   29      CONTINUE                                                     
   30   CONTINUE                                                        
                                                                        
        DO 40,I1=1,I                                                    
                                                                        
           READ(ND81,REC=I1,ERR=914)(MSEQ(II1),II1=1,LE)                
           Write(NERR,1030) ' ',(MSEQ(II1),II1=1,LE)                          
 1030   FORMAT(80A)                                                     
                                                                        
           CALL SESSQ(IDM1,IDSM,ISF1,ISF2,IDM3,MSEQ,MI,LN,NJ,LS,LE      
     *,LC,LD,LF,LH,IOPT,IRC)                                            

           IF(IRC.EQ.0) GO TO 31                                        
           IRC=IRC+100                                                  
           GO TO 100                                                    
                                                                        
   31         J3=1                                                      
           DO 35 I2=1,NJ                                                
              ISF1(I2)=ISF1(J3)                                         
              DO 34 I3=LN,(MI-1)*LN,LN                                  
                 ISF1(I2)=ISF1(I2)+ISF1(J3+I3)                          
   34         CONTINUE                                                  
              J3=J3+LD                                                  
   35      CONTINUE                                                     
                                                                        
           DO 39 I2=1,NJ,20                                             
              WRITE(ND80,REC=JD80,ERR=915)(ISF1(II1),II1=I2,I2+19)      
              JD80=JD80+1                                               
   39      CONTINUE                                                     
   40   CONTINUE                                                        
                                                                        
        CALL TRD80(ND80,ND81,ISF1,IDM1,NJ,I,IRC)                        
        IF(IRC.EQ.0) GO TO 41                                           
        IRC=IRC+110                                                     
        GO TO 100                                                       
                                                                        
   41   IF(N1.NE.MB1) GO TO 45                                          
        WRITE(NOUT,4002,ERR=912) FIDSM,MB1,MB2,MI,CTRC,TIT( 1:50)              
        WRITE(NOUT,4003,ERR=916) CCTY,LS,LE,I,QN,LC,LD,LF,LH,NI,PR      
        IF(CCTY.NE.'U') GO TO 45                                        
        REWIND(NSSC,ERR=917)                                            
   42   READ(NSSC,4000,END=918,ERR=919) RC80                            
           IF(RC80(1:2).NE.'C ') GO TO 42                               
        WRITE(RC80(22:27),'(I6)',ERR=920) I                             
        WRITE(NOUT,4000,ERR=921) RC80                                   
   43   READ(NSSC,4000,END=45,ERR=922) RC80                             
        IF(RC80(1:2).NE.'I ') GO TO 43                                  
        WRITE(NOUT,4000,ERR=923) RC80                                   
        GO TO 43                                                        
                                                                        
   45   JD80=1                                                          
        K1=J1-NJ                                                        
        DO 50 I1=1,NJ                                                   
           K1=K1+1                                                      
           DO 47 I2=1,I,20                                              
              READ(ND80,REC=JD80,ERR=924)(ISF1(II1),II1=I2,I2+19)       
              JD80=JD80+1                                               
   47      CONTINUE                                                     
           CALL MEVAR(ISF1,IDM1,I,XM,XV)                                
           WRITE(NOUT,4004,ERR=925) K1,ISSS(K1),XPOS,XM,XV              
           XPOS=XPOS+LD                                                 
           WRITE(NOUT,4005,ERR=926)(ISF1(II1),II1=1,I)                  
   50   CONTINUE                                                        
                                                                        
        REWIND(NDSM,ERR=927)
        GO TO 11                                                        
                                                                        
  100   CLOSE(ND81,ERR=905)                                             
  101   CLOSE(ND80,ERR=904)                                             
  103   CLOSE(NSSC,ERR=902)                                             
  104   CLOSE(NDSM,ERR=901)                                             
                                                                        
  120   IF(IRC.NE.0) Write(NERR,1120) IRC                                     
 1120   FORMAT(/,'Error termination (RC=',I3,').')                       
        STOP                                                            
                                                                        
  927   IRC=IRC+1                                                       
  926   IRC=IRC+1                                                       
  925   IRC=IRC+1                                                       
  924   IRC=IRC+1                                                       
  923   IRC=IRC+1                                                       
  922   IRC=IRC+1                                                       
  921   IRC=IRC+1                                                       
  920   IRC=IRC+1                                                       
  919   IRC=IRC+1                                                       
  918   IRC=IRC+1                                                       
  917   IRC=IRC+1                                                       
  916   IRC=IRC+1                                                       
  915   IRC=IRC+1                                                       
  914   IRC=IRC+1                                                       
  913   IRC=IRC+1                                                       
  912   IRC=IRC+1                                                       
  911   IRC=IRC+1                                                       
  910   IRC=IRC+1                                                       
  909   IRC=IRC+1                                                       
  908   IRC=IRC+1                                                       
  907   IRC=IRC+1                                                       
  906   IRC=IRC+1                                                       
  905   IRC=IRC+5                                                       
        GO TO 101                                                       
  904   IRC=4                                                           
        GO TO 103                                                       
  903   IRC=3                                                           
        GO TO 103                                                       
  902   IRC=2                                                           
        GO TO 104                                                       
  901   IRC=2                                                           
        GO TO 120                                                       
                                                                        
 2002   FORMAT(BN,1I5)                                                  
 4000   FORMAT(1A80)                                                    
 4002   FORMAT('F ',A7,3X,I6,I6,I6,1X,A1,1X,A47)                        
 4003   FORMAT('S ',A1,':X=',I3,' L=',I3,' I=',I6,' R=',F5.4,
     * ' W=',I5,' S=',I5,' F=',I3,' Y=',I3,' J=',I6,' P=',F6.4)

 4004   FORMAT('J =',I6,' N=',I6,' P=',F9.1,' M=',F10.3,' V=',E10.4)      
 4005   FORMAT(10I8)                                                    
                                                                        
        END                                                             

        Include          'INSSP.f'
        Include          'CEDSM.f'
        Include          'CESSC.f'
        Include          'TRD80.f'
        Include          'SESSQ.f'
        Include          'MEVAR.f'
        Include          'RETSQ.f'
        Include          'PSQID.f'
        Include          'LOCSQ.f'
        Include          'lblnk.f'
        Include          'rindex.f'
        Include          'INB12.f'
        Include          'PISSP.f'
        Include          'STREV.f'
	Include          'ran2.f'

