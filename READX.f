C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : CONVERSION OF CHARACTER DATA INTO REAL NUMBER        
C       ****************************************************************
                                                                        
        SUBROUTINE READX(RC80,X,IRC)                                    
                                                                        
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        CHARACTER     CR16(16)                                          
        CHARACTER*16  RC16                                              
        EQUIVALENCE   (CR16,RC16)                                       
                                                                        
        IRC=0                                                           
                                                                        
        RC16=RC80(1:16)                                                 
                                                                        
        DO 10 I1=1,16                                                   
            IF(CR16(I1).EQ.'.') GO TO 20                                
   10   CONTINUE                                                        
        READ(RC16,2001,ERR=901) I                                       
        X=FLOAT(I)                                                      
        GO TO 100                                                       
                                                                        
   20   N1=I1-9                                                         
        IF(N1.EQ.0) GO TO 50                                            
        IF(N1.GT.1) GO TO 30                                            
        DO 25 I1=16,1-N1,-1                                             
           CR16(I1)=CR16(I1+N1)                                         
   25   CONTINUE                                                        
        DO 29 I1=1,-N1                                                  
           CR16(I1)=' '                                                 
   29   CONTINUE                                                        
        GO TO 50                                                        
   30   DO 35 I1=1,16-N1                                                
           CR16(I1)=CR16(I1+N1)                                         
   35   CONTINUE                                                        
        DO 39 I1=17-N1,16                                               
           CR16(I1)=' '                                                 
   39   CONTINUE                                                        
                                                                        
   50   READ(RC16,2002,ERR=902) X                                       
                                                                        
  100   RETURN                                                          
                                                                        
  901   IRC=IRC+1                                                       
  902   IRC=IRC+1                                                       
        GO TO 100                                                       
                                                                        
 2001   FORMAT(BN,I16)                                                  
 2002   FORMAT(F16.7)                                                   
                                                                        
        END                                                             
