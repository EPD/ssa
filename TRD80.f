C       ****************************************************************TRD00020
C       AUTHOR   : PHILIPP BUCHER                                       TRD00030
C       FUNCTION : TRANSPOSITION OF INTEGER MATRIX ON DISK FILE D80     TRD00040
C       ****************************************************************TRD00050
                                                                        TRD00060
        SUBROUTINE TRD80(ND80,ND81,ISQS,IDM1,I,J,IRC)                   TRD00070
                                                                        TRD00080
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) TRD00090
                                                                        TRD00100
        INTEGER      ISQS(IDM1)                                         TRD00110
                                                                        TRD00120
        IF(IDM1.LT.8400) GO TO 901                                      TRD00150
        IRC=0                                                           TRD00160
                                                                        TRD00170
        M1=(I-1)/20+1                                                   TRD00180
        M2=(J-1)/20+1                                                   TRD00190
        M3=(I-1)/400                                                    TRD00200
        N1=20                                                           TRD00210
        N2=20                                                           TRD00220
        N3=I-M3*400                                                     TRD00230
        N4=M1-M3*20                                                     TRD00240
        N5=M1*400                                                       TRD00250
        N6=M2*20                                                        TRD00260
        N7=M1*20                                                        TRD00270
                                                                        TRD00280
        DO 1 I1=1,8400                                                  TRD00290
           ISQS(I1)=0                                                   TRD00300
    1   CONTINUE                                                        TRD00310
                                                                        TRD00320
        LR=1                                                            TRD00330
                                                                        TRD00340
        J1=1                                                            TRD00350
        DO 70 I1=1,J,20                                                 TRD00360
           IF(I1+19.GT.J) N2=J-I1+1                                     TRD00370
                                                                        TRD00380
           J2=J1                                                        TRD00390
           K2=1                                                         TRD00400
           DO 30 I2=1,N2                                                TRD00410
              J3=J2                                                     TRD00420
              DO 20 I3=1,M3                                             TRD00430
                 K4=8001                                                TRD00440
                 DO 12,I4=1,20                                          TRD00450
                    READ(ND80,REC=LR,ERR=902)(ISQS(II1),II1=K4,K4+19)   TRD00460
                    LR=LR+1                                             TRD00470
                    K4=K4+20                                            TRD00480
   12            CONTINUE                                               TRD00490
                 K4=8001                                                TRD00500
                 DO 15 I4=J3,J3+19                                      TRD00510
                    WRITE(ND81,REC=I4,ERR=903)(ISQS(II1),II1=K4,K4+19)  TRD00520
                    K4=K4+20                                            TRD00530
   15            CONTINUE                                               TRD00540
                 J3=J3+400                                              TRD00550
   20         CONTINUE                                                  TRD00560
              K3=K2                                                     TRD00570
              DO 25 I3=1,N4                                             TRD00580
                 READ(ND80,REC=LR,ERR=904)(ISQS(II1),II1=K3,K3+19)      TRD00590
                 LR=LR+1                                                TRD00600
                 K3=K3+400                                              TRD00610
   25         CONTINUE                                                  TRD00620
              J2=J2+20                                                  TRD00630
              K2=K2+20                                                  TRD00640
   30      CONTINUE                                                     TRD00650
                                                                        TRD00660
              K3=0                                                      TRD00670
              DO 39 I3=1,N4                                             TRD00680
                 DO 36 I4=2,20                                          TRD00690
                    K5=K3+I4*20-19                                      TRD00700
                    DO 33 I5=K3+I4,K3+(21-I4)*20,21                     TRD00710
                       N=ISQS(I5)                                       TRD00720
                       ISQS(I5)=ISQS(K5)                                TRD00730
                       ISQS(K5)=N                                       TRD00740
                       K5=K5+21                                         TRD00750
   33               CONTINUE                                            TRD00760
   36            CONTINUE                                               TRD00770
                 K3=K3+400                                              TRD00780
   39         CONTINUE                                                  TRD00790
              K3=1                                                      TRD00800
              DO 40 I2=J1+M3*400,J1+I-1                                 TRD00810
                 WRITE(ND81,REC=I2,ERR=905)(ISQS(II1),II1=K3,K3+19)     TRD00820
                 K3=K3+20                                               TRD00830
   40         CONTINUE                                                  TRD00840
                                                                        TRD00850
           J2=J1                                                        TRD00860
           DO 65 I2=1,M3                                                TRD00870
              J3=J2                                                     TRD00880
              K3=1                                                      TRD00890
              DO 50 I3=J3,J3+399,20                                     TRD00900
                 K4=K3                                                  TRD00910
                 DO 45 I4=I3,I3+19                                      TRD00920
                    READ(ND81,REC=I4,ERR=906)(ISQS(II1),II1=K4,K4+19)   TRD00930
                    K4=K4+400                                           TRD00940
   45            CONTINUE                                               TRD00950
                 J3=J3+20                                               TRD00960
                 K3=K3+20                                               TRD00970
   50         CONTINUE                                                  TRD00980
                                                                        TRD00990
              K3=0                                                      TRD01000
              DO 59 I3=1,20                                             TRD01010
                 DO 56 I4=2,20                                          TRD01020
                    K5=K3+I4*20-19                                      TRD01030
                    DO 53 I5=K3+I4,K3+(21-I4)*20,21                     TRD01040
                       N=ISQS(I5)                                       TRD01050
                       ISQS(I5)=ISQS(K5)                                TRD01060
                       ISQS(K5)=N                                       TRD01070
                       K5=K5+21                                         TRD01080
   53               CONTINUE                                            TRD01090
   56            CONTINUE                                               TRD01100
                 K3=K3+400                                              TRD01110
   59         CONTINUE                                                  TRD01120
              K3=1                                                      TRD01130
              DO 60 I3=J2,J2+399                                        TRD01140
                 WRITE(ND81,REC=I3,ERR=909)(ISQS(II1),II1=K3,K3+19)     TRD01150
                 K3=K3+20                                               TRD01160
   60         CONTINUE                                                  TRD01170
              J2=J2+400                                                 TRD01180
                                                                        TRD01190
   65      CONTINUE                                                     TRD01200
                                                                        TRD01210
           J1=J1+N7                                                     TRD01220
   70   CONTINUE                                                        TRD01230
                                                                        TRD01240
        J1=1                                                            TRD01250
        L1=1                                                            TRD01260
        DO 90 I1=1,I,20                                                 TRD01270
           IF(I1+19.GT.I) N1=I-I1+1                                     TRD01280
           N2=20                                                        TRD01290
           J2=J1                                                        TRD01300
           L2=L1                                                        TRD01310
           DO 85 I2=1,M2,20                                             TRD01320
              IF(I2+19.GT.M2) N2=M2-I2+1                                TRD01330
              J3=J2                                                     TRD01340
              K3=1                                                      TRD01350
              DO 75 I3=1,N2                                             TRD01360
                 K4=K3                                                  TRD01370
                 DO 74 I4=J3,J3+N1-1                                    TRD01380
                    READ(ND81,REC=I4,ERR=908)(ISQS(II1),II1=K4,K4+19)   TRD01390
                    K4=K4+400                                           TRD01400
   74            CONTINUE                                               TRD01410
                 J3=J3+N7                                               TRD01420
                 K3=K3+20                                               TRD01430
   75         CONTINUE                                                  TRD01440
                                                                        TRD01450
              K3=1                                                      TRD01460
              L3=L2                                                     TRD01470
              DO 80 I3=1,N1                                             TRD01480
                 K4=K3                                                  TRD01490
                 DO 79 I4=L3,L3+N2-1                                    TRD01500
                    WRITE(ND80,REC=I4,ERR=909)(ISQS(II1),II1=K4,K4+19)  TRD01510
                    K4=K4+20                                            TRD01520
   79            CONTINUE                                               TRD01530
                 J3=J3+N7                                               TRD01540
                 K3=K3+400                                              TRD01550
                 L3=L3+M2                                               TRD01560
   80         CONTINUE                                                  TRD01570
              J2=J2+N5                                                  TRD01580
              L2=L2+20                                                  TRD01590
   85      CONTINUE                                                     TRD01600
           J1=J1+20                                                     TRD01610
           L1=L1+N6                                                     TRD01620
   90   CONTINUE                                                        TRD01630
                                                                        TRD01640
  100   RETURN                                                          TRD01660
                                                                        TRD01670
  909   IRC=IRC+1                                                       TRD01680
  908   IRC=IRC+1                                                       TRD01690
  907   IRC=IRC+1                                                       TRD01700
  906   IRC=IRC+1                                                       TRD01710
  905   IRC=IRC+1                                                       TRD01720
  904   IRC=IRC+1                                                       TRD01730
  903   IRC=IRC+1                                                       TRD01740
  902   IRC=IRC+1                                                       TRD01750
  901   IRC=IRC+1                                                       TRD01760
        GO TO 100                                                       TRD01770
                                                                        TRD01780
        END                                                             TRD01790
