*$      Main program PRDSM : Created 10/01/82 ; last update 03/09/92   *
*----------------------------------------------------------------------*
*       Author   : Philipp Bucher
*       Function : Listing of DNA sequence matrix
*       Version  : UNIX            
*----------------------------------------------------------------------*
C       Parameter        (NERR= 7)
        Parameter        (NERR= 0)
        Parameter        (NOUT= 6)
        Parameter        (NINP= 5)
        Parameter        (NFPS=10)
        Parameter        (NSCR=11)
        Parameter        (NPTR=12)
        Parameter        (NSEQ=13)
        Parameter        (NREF=14)

        Parameter        (LPWM=  256)
        Parameter        (IDM1=16384)

        Character*01      CSEQ(IDM1)

        Character*(LPWM)    LIN1
        Character*(LPWM)    LIN2
        Character*(LPWM)    LIN3


        Character*132     RCIN
        Character*64      RC64

        Character*55      FTIT
           Data              FTIT/' '/
        Character*10      FNAM
           Data              FNAM/' '/

        Character*33      CHFP

        Character*64      FDBE
        Character*64      FDBF
        Character*192     FFPS
        Character*64      FOUT

        Character*01      CDEV
        Character*01      CTRC
        Character*01      CSTR
        Character*01      CB

        Integer           Rindex

        Integer*8         IPOS
        Integer*8         LDBE
        Integer*8         MB1
        Integer*8         MB2
       
* Start program execution        

        IRC=0

* Interactive dialogue
      
        Call PRDSMI
     *    (NERR,NINP,NFPS,LPWM,CDEV,MB1,MB2,IC1,IC2,LPW,LPL,LY,CTRC,CB,
     *     LSP,IRC)
        If(IRC.NE.0) then
           IRC=IRC+900
           go to 900 
        End if

* Read title line of FPS file 

    1   Read(NFPS,'(A)',Err=901) RCIN
           If(RCIN( 1: 2).EQ.'FP') go to   5
           If(RCIN( 1: 2).NE.'TI') go to   1
        Read(RCIN,'(5x,A10,A55)',Err=902) FNAM,FTIT
    5   Rewind(NFPS,Err=903) 
        If(FTIT.EQ.' ') FTIT='Unnamed functional position set'

* Open output files

        Open(NSCR,Status='SCRATCH',Form='UNFORMATTED',Err=904)

        If(CDEV.EQ.'P') then

*  - Edit output file name

           If(FNAM.EQ.' ') then 
              Inquire(NFPS,Name=FFPS,Iostat=IOS)
              If(IOS.EQ.0) then
                 L1=Rindex(FFPS,'/')+1
                 L2=Index(FFPS(L1:64),'.')-1
                 If(L2.LE.0) then 
                    L2=Lblnk(FFPS)
                 Else
                    L2=L1+L2-1
                 End if  
                 FOUT=FFPS(L1:L2)
              End if
           Else 
              FOUT=FNAM
           End if 

           L2=Lblnk(FOUT)
           L1=MAX(1,Rindex(FOUT(1:L2),' '))
           RC64=FOUT(L1:L2) // '.lis'
           FOUT=RC64
      
           Open(NOUT,File=FOUT,Status='NEW',Iostat=IOS)
           If(IOS.NE.0) then 
              FOUT='PRDSM.lis'
              Open(NOUT,File=FOUT,Status='Unknown',Err=905)
           End if

        End if

* Initialize major loop variables

        IST=1
        MI=0
        FDBF=' '
        LCO=0
        LM=MB2-MB1+1
        IC3=IC1+IC2-1
        IC4=IC2+2
           If(IC4.LT.6) IC4=6
        LX=LPW-IC4+1
           If(LX.GT.LM) LX=LM

* Major loop: Print successive windows

        Do  99 N1=1,LM+LY-LX,LY

           MJ=0
           If(N1.GT.1) then
              If(MI.EQ.0) go to 906 
              Rewind(NSCR,Err=907)
              If(N1+LX-1.GT.LM) LX=LM-N1+1 
           End if

* Prepare LIN1, LIN2 (Sequence numbering)

           LIN1=' '
           LIN2=' '

           NP=MB1+N1-1
           NX=(NP/10)*10  
           If(NX.LT.NP) NX=NX+10
           NQ=NP+LX-1
           NY=(NQ/10)*10
           If(NY.GT.NQ) NY=NY-10
           If(NX.GT.NY) then
              NX=NP
              NY=NP
              end if

              J2=IC4+NX-NP
           Do  10 I2=NX,NY,10
              Write(LIN1(J2-5:J2),'(I6)',Err=908) I2
              Write(LIN2(J2:J2),'(A)',Err=909) ''''
              J2=J2+10
   10      Continue                


* Print title

           If(MI.EQ.0.OR.LCO+5+MI.GT.LPL) then

              If(CDEV.EQ.'T') then
                 Write(NOUT,'('' '')',Err=910) 
                 Write(NOUT,
     *            '(''Press RETURN to continue ...'',$)',Err=911)
                 Read(NINP,'(A)',Err=912) RCIN
                 Write(NOUT,'('' '')',Err=913)
              End if

              If(CDEV.EQ.'T') then
                 Write(NOUT,'(A)',Err=914) FTIT(1:MIN(55,LPW))     
              Else
                 Write(NOUT,'('''',A)',Err=915) FTIT(1:MIN(55,LPW))     
              End if

              LCO=1

           End if

* Print numbers

           Write(NOUT,'('' '')',Err=916) 
           Write(NOUT,'(A)',Err=917) LIN1( 1:LPW)
           Write(NOUT,'(A)',Err=918) LIN2( 1:LPW)
           LCO=LCO+3            

   20      If(N1.GT.1) go to  40

* Extract sequence segments from nucleotide sequence database 

   25      Read(NFPS,'(A)',Err=919,End= 99) RCIN
             If(RCIN( 1: 2).EQ.'YZ') go to  99 
             If(RCIN( 1: 2).NE.'FP') go to  25
             IRC=1
           Read(RCIN,"(30x,A33)",Err= 30) CHFP
             IRC=0
                                                                        
           Call GETFP 
     *       (NPTR,NSEQ,NREF,CHFP,MB1,MB2,CTRC,CB,FDBE,LDBE,ITY,CSTR,
     *        IPOS,CSEQ,IDM1,IST,IRC)
 
              If(IRC.NE.0) then
                 IRC=IRC+100
                 Go to  30
              End if

           If(LM.LE.LX) go to  30

* Write remaining part of sequence segment to scratch file

           Write(NSCR,Err=920) RCIN(IC1:IC3)
           Write(NSCR,Err=921)(CSEQ(ii1),ii1=LY+1,LM)
           MI=MI+1

* Prepare nulceotide sequence line

   30      If(CDEV.EQ.'P') then
              Write(NERR,'(A57,'' :RC='',I3)',Err=922)
     *        RCIN( 6:62),IRC
           End if
           LIN3( 1:IC4)=RCIN(IC1:IC3)
           If(IRC.NE.0) 
     *        Write(LIN3(IC4:LPW),'(''RC='',I3)',Err=923) IRC
           If(IRC.EQ.0) 
     *        Write(LIN3(IC4:LPW),'(256A)',Err=924)
     *           (CSEQ(ii1),ii1=1,LX)
           IRC=0   
           Go to  50

* Read from scratch file

   40      Read(NSCR,Err=925,End=926) LIN3( 1:IC2)
           N2=N1-LY+LX-1
           Read(NSCR,Err=927,End=928)(CSEQ(ii1),ii1=1,N2)
           MJ=MJ+1
           Write(LIN3(IC4:LPW),"(256A)",Err=929)(CSEQ(ii1),ii1=N1-LY,N2)       

* Print title

   50      If(LCO.GE.LPL) then 

              If(CDEV.EQ.'T') then
                 Write(NOUT,'('' '')',Err=930) 
                 Write(NOUT,
     *            '(''Press RETURN to continue ...'',$)',Err=931)
                 Read(NINP,'(A)',Err=932) RCIN
                 Write(NOUT,'('' '')',Err=933) 
              End if

              If(CDEV.EQ.'T') then
                 Write(NOUT,'(A)',Err=934) FTIT(1:MIN(55,LPW))     
              Else
                 Write(NOUT,'('''',A)',Err=935) FTIT(1:MIN(55,LPW))     
              End if

              LCO=1

* Print numbers

              Write(NOUT,'('' '')',Err=936)
              Write(NOUT,'(A)',Err=937) LIN1( 1:LPW)
              Write(NOUT,'(A)',Err=938) LIN2( 1:LPW)
              LCO=LCO+3            

           End if

* Print nucleotide sequence

           Write(NOUT,'(A)',Err=939) LIN3( 1:LPW)
           LCO=LCO+1
           If(LSP.EQ.2.AND.LCO.LT.LPL) then
              Write(NOUT,'('' '')',Err=940)
              LCO=LCO+1
           End if

           If(MJ.EQ.0.OR.MJ.LT.MI) go to  20

   99   Continue

* End of major loop

        If(CDEV.EQ.'P') then
           Write(NERR,'('' '')',Err=941)
           Write(NERR,'(A)',Err=942)
     *        'Output in ' // FOUT(1:Lblnk(FOUT)) // ' .'
           Write(NERR,'('' '')',Err=943)
        End if

  100   Stop

* Error handling

  943   IRC=IRC+1
  942   IRC=IRC+1
  941   IRC=IRC+1
  940   IRC=IRC+1
  939   IRC=IRC+1
  938   IRC=IRC+1
  937   IRC=IRC+1
  936   IRC=IRC+1
  935   IRC=IRC+1
  934   IRC=IRC+1
  933   IRC=IRC+1
  932   IRC=IRC+1
  931   IRC=IRC+1
  930   IRC=IRC+1
  929   IRC=IRC+1
  928   IRC=IRC+1
  927   IRC=IRC+1
  926   IRC=IRC+1
  925   IRC=IRC+1
  924   IRC=IRC+1
  923   IRC=IRC+1
  922   IRC=IRC+1
  921   IRC=IRC+1
  920   IRC=IRC+1
  919   IRC=IRC+1
  918   IRC=IRC+1
  917   IRC=IRC+1
  916   IRC=IRC+1
  915   IRC=IRC+1
  914   IRC=IRC+1
  913   IRC=IRC+1
  912   IRC=IRC+1
  911   IRC=IRC+1
  910   IRC=IRC+1
  909   IRC=IRC+1
  908   IRC=IRC+1
  907   IRC=IRC+1
  906   IRC=IRC+1
  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        
  900   Write(NERR,'(''Error termination, RC='',I3,''.'')') IRC 
        Go to 100

        End 
*----------------------------------------------------------------------*
        Include           'PRDSMI.f'
        Include           'GETFP.f'
        Include           'LOCSQ.f'
        Include           'PSQID.f'
