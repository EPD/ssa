        Program       OPROF

        Parameter     (IDM1=1000000)
        Parameter     (IDM4=64)

        Parameter     (NERR= 0)
        Parameter     (NINP= 5)
        Parameter     (NOUT= 6)

        Parameter     (NFPS=11)
        Parameter     (NLSQ=12)
        Parameter     (NWMX=13)
        Parameter     (NREF=14)
        Parameter     (NSEQ=15)
        Parameter     (NPTR=16)

        Byte          ISEQ(IDM1)
        Integer       ISF(IDM1)
        Integer       ISS(IDM1)
        Real          WMX(IDM4,4)
        Character     CSEQ(IDM1)
     
        Character*80  RC80
        Character*55  TDSM
        Character*22  FDBE
        Character*03  CIRC

        Byte          B
        Character*01  CBD
        Character*01  CTRC
        Character*01  CSTR
        Character*01  CC
        Character*01  CB

        Integer*8         IPOS
        Integer*8         LM
        Integer*8         LDBE

        Data ISF/IDM1*0/,ISS/IDM1*0/

        IRC=0

* Read input parameters 
      
        Call OPROFI(
     *     NERR,NINP,
     *     NFPS,TDSM,MB1,MB2, 
     *     NWMX,WMX,IDM4,IWM1,LWM,SCMI,CO,    
     *     CBD,LW,LD, 
     *     IRC) 
  
* Major loop: process sequence segments one by one

        LM=MB2-MB1+1
        CTRC='D'
        CB='N'
        CC='U'
        MI=0
        IST=1
        XCUT=(100-CO)/100*SCMI

C       DO I3=1,LWM
C          Write(6,'(4F10.5)')(WMX(I3,ii1),ii1=1,4)
C       End do

   20   Read(NFPS,"(A)",Err=906,End= 90) RC80
           If(RC80( 1: 2).EQ.'YZ') go to  90
           If(RC80( 1: 2).NE.'FP') go to  20
           IRC=1
        Read(RC80,"(30x,A21,I1,A1,I10)",Err=30) FDBE,ITY,CSTR,IPOS
           IRC=0

        If(CSTR.EQ.'+') IPOS=IPOS+MB1           
        If(CSTR.EQ.'-') IPOS=IPOS-MB2

        Call RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LDBE,ITY,IPOS,LM,CSEQ,IDM1,IST,CC,IRC)
        If(IRC.NE.0) then
           IRC=IRC+100
           Go to  89
        End if
        Write(NERR, *) 'After Call RETSQ FNAM = ', FNAM
        Write(NERR, *) 'After Call RETSQ SQID = ', SQID

* Upper case or lower case ?

        Do  27 I1=IST,IST+LM-1
           B=Ichar(CSEQ(I1))
           If     (B.GE.65.AND.B.LE. 90) then
              CC='U'
              Go to  28
           Else if(B.GE.97.AND.B.LE.122) then 
              CC='L'
              Go to  28
           End if
   27   Continue
   28   Continue
                      
        Call
     *     STREV(CSEQ,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  
        If(IRC.NE.0) then
           IRC=IRC+200
           Go to  30
        End if

   30   If(IRC.NE.0) go to  89

* Conversion into numbers   

        Do  40 I1=1,LM
              ISEQ(I1)=0
           IF(CSEQ(I1).EQ.'A') ISEQ(I1)=1
           IF(CSEQ(I1).EQ.'C') ISEQ(I1)=2
           IF(CSEQ(I1).EQ.'G') ISEQ(I1)=3
           IF(CSEQ(I1).EQ.'T') ISEQ(I1)=4
   40      Continue

        Do 60 I1=1,LM-LWM+1,LD
              
           Do  44 I2=I1,I1+LW-1
              If(ISEQ(I2).EQ.0) go to 60
   44      Continue               
           ISS(I1)=ISS(I1)+1

   45      Do  49 I2=I1,I1+LW-LWM
                 J1=0
                 X=0
              Do  47 I3=I2,I2+LWM-1
                 J1=J1+1
                 X=X+WMX(J1,INT(ISEQ(I3)))
                 If(X.LE.XCUT) go to 49
   47         Continue 
              ISF(I1)=ISF(I1)+1
              Go to 60
   49      Continue

           If(CBD.NE.'B') go to 60            


           Do  54 I2=I1,I1+LW-LWM
                 J1=LWM+1
                 X=0
              Do  52 I3=I2,I2+LWM-1
                 J1=J1-1
                 X=X+WMX(J1,5-INT(ISEQ(I3)))
                 If(X.LE.XCUT) go to 54
   52         Continue 
              ISF(I1)=ISF(I1)+1
              Go to 60
   54      Continue

   60   Continue 

           MI=MI+1

   89   Write(CIRC,'(I3)',Err=908) IRC
        Write(NERR, *) 'OPROF:: FNAM = ', FNAM
        Write(NERR,*) RC80(6:64),' : RC=',CIRC
        IRC=0
        Go to  20

  801   IRC=IRC+1
        Go to  89

   90   Continue

        Print *,'# h 0.4'
        Print *,'# u 0.4.5'
        Print *,'# r -0.1'
        Print *,'# ltx 0.5'
        Print *,'# lty 1.2'
        Print *,'# lt "Motif Occurrence Profile"'


        YMAX=0
        Do  91 I1=1,LM-LW+1,LD
           XF=Real(ISF(I1))/ISS(I1)*100
           If(XF.GT.YMAX) YMAX=XF
   91   Continue

        YMAX=YMAX*1.1
        Print *,'# x ',MB1,MB2
        Print *,'# y 0 ',YMAX

        LB2=IWM1+LWM-1 
        Do  95 I1=1,LM-LW+1,LD
           If(CBD.NE.'B') XPOS=MB1+Real(I1-IWM1+I1+LW-1-LB2)/2-1   
           If(CBD.EQ.'B') XPOS=MB1+I1-1+Real(LW-1)/2
           XF  =Real(ISF(I1))/ISS(I1)*100
           Write(NOUT,*,Err=909) 
     *        XPOS,XF
   95   Continue

        Print *,'# s'
        Print *,'# h 0.2'
        Print *,'# u 0.0'
        Print *,'# r -0.1'
        Print *,'# ltx 0.5'
        Print *,'# lty 1.2'
        Print *,'# lts 1.0'
        Print *,'# lns 1.0'
        Print *,'# lt "# of sequences per window"'

        YMAX=0
        Do  96 I1=1,LM-LW+1,LD
           If(ISS(I1).GT.YMAX) YMAX=ISS(I1)
   96   Continue

        YMAX=YMAX*1.2
        Print *,'# x ',MB1,MB2
        Print *,'# y 0 ',YMAX

        Do  99 I1=1,LM-LW+1,LD
           If(CBD.NE.'B') XPOS=MB1+Real(I1-IWM1+I1+LW-1-LB2)/2-1   
           If(CBD.EQ.'B') XPOS=MB1+I1-1+Real(LW-1)/2
           Write(NOUT,*,Err=909) 
     *        XPOS,ISS(I1)
   99   Continue

  100   If(IRC.NE.0) Print 1100,IRC
 1100   Format(' Error termination: RC=',I3,'.')

        Stop

* Error handling: 

  909   IRC=IRC+1
  908   IRC=IRC+1
  907   IRC=IRC+1
  906   IRC=IRC+1
  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 100

 4001   Format(30X,A21,I1,A1,I10)

        End

        Include 'OPROFI.f'
        Include 'STREV.f'
        Include 'RETSQ.f'
        Include 'rindex.f'
