*$      Subroutine PRDSMI  : Created 05/28/90 ; last update 03/09/92   *
*----------------------------------------------------------------------*
*       Author    : Philipp Bucher                                       
*       Function  : Interactive dialogue of program PRDSM         
*       Arguments : NERR (R-) : Logical unit for terminal output 
*                   NINP (R-) : Logical unit for terminal input 
*                   NFPS (R-) : Logical unit for FPS input file
*                   LPWM (R-) : Maximal page width 
*                   CDEV (-W) : Device-specific output format 
*                   MB1  (-W) : Left  (5'OH) border of sequence segments 
*                   MB2  (-W) : Right (3'OH) border of sequence segments 
*                   IC1  (-W) : Starting position of description 
*                   IC2  (-W) : Length of description
*                   LPW  (-W) : Page width
*                   LPL  (-W) : Page length
*                   LY   (-W) : Window shift
*                   CTRC (-W) : Translation code
*                   CB   (-W) : Character for unknown nucleotide
*                   LSP  (-W) : Line spacing
*                   IRC  (-W) : Return code
*----------------------------------------------------------------------*

        Subroutine PRDSMI
     *    (NERR,NINP,NFPS,LPWM,CDEV,MB1,MB2,IC1,IC2,LPW,LPL,LY,CTRC,CB,
     *     LSP,IRC)

        Character*192     RC80
        Character*192     FFPS
        Character*55      FTIT 

        Character*01      CDEV
        Character*01      CTRC
        Character*01      CB 

        Integer*8         MB1
        Integer*8         MB2
 
        IRC=0

* Get terminal dimensions

        Call GETLW(LPLT,LPWT)

* Beginning of interactive dialogue

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901)
     *    'Program PRDSM / Beginning of interactive dialogue:'
        Write(NERR,*,Err=901)
     *    'Press <Return> for default, enter < to go back to ',
     *    'previous question.'       
        Write(NERR,*,Err=901) ' '

* Open FPS file, read Title (FTIT)

   10   FFPS='Fps.Fps'
        Write(NERR,1010,Err=901) FFPS( 1: 7)
 1010   Format('  1. Input file (default=',A7,'): ',$)
        Read(NINP,'(A)',Err= 15) RC80
        If(RC80.EQ.' ') go to 11
        Read(RC80,'(A)',Err= 15) FFPS
   11   Open(NFPS,File=FFPS,Status='OLD',Err= 15)
        Go to 16
   15   Write(NERR,1199,Err=901)
        Go to 10

   16   FTIT=' '
   17   Read(NFPS,'(A)',Err=902,End= 18) RC80 
           If(RC80( 1: 2).EQ.'FP') go to 18
           If(RC80( 1: 2).EQ.'TI') go to 19
   18   RC80(16:70)='Unnamed functional position set'

   19   Rewind(NFPS,Err=903)
        FTIT=RC80(16:70)

        Write(NERR,*,Err=901) ' '
        Write(NERR,1019,Err=901) FTIT 
 1019   Format 
     *   ('     ',A55)
        Write(NERR,*,Err=901) ' '

* Output format (CDEV) 

   20   CDEV='T'
        Write(NERR,1020,Err=901) CDEV
 1020   Format
     *   ('  2. Output format (P=Print-file, T=Terminal, ',
     *    'default=',A1,'): ',$)
        Read(NINP,'(A)',Err= 25) RC80
        If(RC80.EQ.' ') go to 26
        If(RC80.EQ.'<') go to 10
        Read(RC80,'(A)',Err= 25) CDEV
        If(CDEV.EQ.'p') CDEV='P'
        If(CDEV.EQ.'t') CDEV='T'
        If(CDEV.EQ.'P'.OR.CDEV.EQ.'T') go to 26
   25   Write(NERR,1199,Err=901)
        Go to 20

* 5' border (MB1)

   26   If(CDEV.EQ.'P') LPW=79 
        If(CDEV.EQ.'T') LPW=MIN(LPWT-1,LPWM)
           
   30   If(CDEV.EQ.'P') MB1=-49
        If(CDEV.EQ.'T') MB1=(30-LPW)/10*10+1
        Write(NERR,1030,Err=901) MB1                                                  
 1030   Format
     *   ('  3. Left (5''OH/N-terminal) border (default=',I5,'): ',$)         
        Read(NINP,'(A)',Err= 35) RC80                               
        If(RC80.EQ.' ') go to 40                                   
        If(RC80.EQ.'<') go to 20
        Read(RC80,*,Err= 35) MB1                                      
        Go to 40
   35   Write(NERR,1199,Err=901)
        Go to 30 

* 3' border (MB2)

   40   MB2=MB1+LPW-22
        Write(NERR,1040,Err=901) MB2                                                  
 1040   Format
     *   ('  4. Right (3''OH/C-terminal) border ',
     *    '(default is',I5,'): ',$)
        Read(NINP,"(A)",Err= 45) RC80                               
        If(RC80.EQ.' ') go to 46
        If(RC80.EQ.'<') go to 30
        Read(RC80,*,ERR= 45) MB2                                      
        If(MB2.GT.MB1) go to 46                                        
   45   Write(NERR,1199,Err=901)                                                      
        Go to 40
                               
   46   LM=MB2-MB1+1
                         
* Starting position of description (IC1)

   50   IC1=6
        Write(NERR,1050,Err=901) IC1
 1050   Format
     *   ('  5. Starting position of description ',
     *    '(Default is ',I2,'): ',$)
        Read(NINP,'(A)',Err= 55) RC80
        If(RC80.EQ.' ') go to 60
        If(RC80.EQ.'<') go to 40
        Read(RC80,*,Err= 55) IC1    
        If(IC1.GE.1.AND.IC1.LE.80) go to 60 
   55   Write(NERR,1199,Err=901) 
        Go to 50

* Length of description (IC2)

   60   IC2=20
        If(IC1+IC2-1.GT.80) IC2=80-IC1+1
        Write(NERR,1060,Err=901) IC2
 1060   Format
     *   ('  6. Length of description (Default is ',I2,'): ',$)
        Read(NINP,'(A)',Err= 65) RC80
        If(RC80.EQ.' ') go to 66
        If(RC80.EQ.'<') go to 50
        Read(RC80,*,Err= 65) IC2
        If(IC2.GE.1.AND.IC1+IC2-1.LE.132.AND.IC2.LE.LPW-7) go to 66
   65   Write(NERR,1199,Err=901)
        Go to 60

   66   IC3=IC1+IC2-1
        IC4=IC2+2
        If(IC4.LT.6) IC4=6

* Page width (LPW)

   70   If(CDEV.EQ.'P') LPW=79 
        If(CDEV.EQ.'T') LPW=MIN(LPWT-1,LPWM)
        If(IC4+LM-1.LT.LPW) LPW=IC4+LM-1
        Write(NERR,1070,Err=901) LPW
 1070   Format
     *   ('  7. Page width (Default is ',I3,'): ',$)
        Read(NINP,'(A)',Err= 75) RC80
        If(RC80.EQ.' ') go to 76
        If(RC80.EQ.'<') go to 60
        Read(RC80,*,Err= 75) LPW
        If(LPW.GE.IC4.AND.LPW.LE.LPWM) go to 76
   75   Write(NERR,1199,Err=901)
        Go to 70

   76   LX=LPW-IC4+1
        If(LX.GT.LM) LX=LM

* Page length (LPL)

   80   If(CDEV.EQ.'P') LPL=66
        If(CDEV.EQ.'T') LPL=LPLT
        Write(NERR,1080,Err=901) LPL 
 1080   Format
     *   ('  8. Page length (Default is ',I3,'): ',$)
        Read(NINP,'(A)',Err= 85) RC80
        If(RC80.EQ.' ') go to 86
        If(RC80.EQ.'<') go to 70
        Read(RC80,*,Err= 85) LPL 
        If(LPL.GE.8) go to 86
   85   Write(NERR,1199,Err=901) 
        Go to 80

   86   If(CDEV.EQ.'T') LPL=LPL-2

* Window shift (LY)
 
   90   If(RC80(1:1).EQ.'<') Go to 80
        If(LX.LT.LM) go to 91
        LY=LX
        Go to 100

   91   LY=(LX-5)/10*10
        If(LY.LE.1) LY=5
        If(LX.LT.5) LY=LX
        Write(NERR,1091,Err=901) LX,LY
 1091   Format
     *   ('  9. Window shift (Window width is ',I3,', ',
     *    'default shift is ',I3,'): ',$)
        Read(NINP,'(A)',Err= 95) RC80
        If(RC80.EQ.' ') go to 100
        If(RC80.EQ.'<') go to  80
        Read(RC80,*,Err= 95) LY 
        If(LY.GE.1) go to 100
   95   Write(NERR,1199,Err=901)
        Go to 90

* Translation code (CTRC)

  100   CTRC='D'
        Write(NERR,1100,Err=901) CTRC
 1100   Format
     *   (' 10. Translation code (D, R, F, or P default=',A1,'): ',$)
        Read(NINP,'(A)',Err=105) RC80
        If(RC80.EQ.' ') go to 110 
        If(RC80.EQ.'<') go to  90
        Read(RC80,'(A)',Err=105) CTRC
        If(Index('drfp',CTRC).NE.0) go to 109
        If(Index('DRFP',CTRC).NE.0) go to 110
  105   Write(NERR,1199,Err=901)
        Go to 100
  
  109   If(CTRC.EQ.'d') CTRC='D'
        If(CTRC.EQ.'r') CTRC='R'
        If(CTRC.EQ.'f') CTRC='F'
        If(CTRC.EQ.'p') CTRC='P'

* Character for unknown nucleotide (CB)

  110   CB=' '
        Write(NERR,1110,Err=901) 
 1110   Format
     *   (' 11. Character for unknown residue (Default is blank): ',$)
        Read(NINP,'(A)',Err=115) RC80
        If(RC80.EQ.'<') go to 100
        Read(RC80,'(A)',Err=115) CB
        Go to 120
  115   Write(NERR,1199,Err=901)
        Go to 110

* Line spacing (LSP)

  120   LSP=1
        Write(NERR,1120,Err=901) LSP
 1120   Format(' 12. Line-spacing (1 or 2, default is ',I1,'): ',$)
        Read(NINP,'(A)',Err=125) RC80
        If(RC80.EQ.' ') go to 200 
        If(RC80.EQ.'<') go to 110
        Read(RC80,*,Err=125) LSP
        If(LSP.EQ.1.OR.LSP.EQ.2) go to 200 
  125   Write(NERR,1199,Err=901) 
        go to 120

* End of interactive dialogue

  200   Return

  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 200

 1199   Format('     Input not accepted; try again !')

        End
*----------------------------------------------------------------------*
        Subroutine GETLW(LPL,LPW)
        Call getlwc(LPL,LPW) 
        Return
        End
