        Logical Function
     *     LOCSQ(NPTR,FNAM,SQID,LSEQ,FMT,FSEQ,FDOC,RCL,IBS,IBD,CC)

        Character*(*)     FNAM
        Character*(*)     SQID 
        Character*(*)     FSEQ
        Character*(*)     FDOC 

        Integer           FStat64
        Integer*8         STATB(13)
        Integer*8         FTell

        Integer*8         IBS
        Integer*8         IBD
        Integer*8         IR
        Integer*8         JL
        Integer*8         JM
        Integer*8         J0
        Integer*8         JU
        Integer*8         LSEQ

	Real*8            D

        Character         FMT
        Character*64      DIR
        Character*64      SEQ
        Character*64      DOC
        Integer           KLN
        Integer           RCL
        Character*01      CAS
        Character*01      CC

        Character*32      DIV

        Character*64      CHRS
        Character*64      RCIN

        LOCSQ=.FALSE.
        IBS=-1
        IBD=-1

        Inquire(NPTR,Name=CHRS)
        If(CHRS.EQ.FNAM) go to  10 
        Open(NPTR,File=FNAM,Status='OLD',Err=901)
        Read(NPTR,'(8x,A1 )',Err=901) FMT 
        Read(NPTR,'(8x,A55)',Err=901) DIR 
        Read(NPTR,'(8x,A55)',Err=901) SEQ 
        Read(NPTR,'(8x,A55)',Err=901) DOC 
        Read(NPTR,*)
        Read(NPTR,'(8x,I4 )',Err=901) KLN   
        Read(NPTR,'(8x,I4 )',Err=901) RCL   
        Read(NPTR,'(8x,A1 )',Err=901) CAS 

        JL=FTell(NPTR)-64        
        I1=FStat64(NPTR,STATB)
        If(I1.NE.0) go to 901
        JU=STATB(8)-64
        J0=INT  (LOG(Real(JU-JL)/64)/0.69314718)
        D=(2**J0)*64     
        J0=D    

   10   CC=CAS
        IR=JL+J0 
        JM=J0

   15   If(JM.LT.64) go to 100 

        Call FSeek(NPTR,IR,0,*901) 
        JM=JM/2
           
        Read(NPTR,'(A)') RCIN
        If     (SQID.GT.RCIN(1:KLN)) then  
           IR=MIN(IR+JM,JU) 
           Go to  15
        Else if(SQID.LT.RCIN(1:KLN)) then 
           IR=IR-JM
           Go to  15
        End if
 
        Read(RCIN,'(A32,I10,I10,I11)') CHRS,LSEQ,IBS,IBD 
        DIV=RCIN(KLN+1:32)
        IBS=IBD+IBS

        L1=lblnk(DIR)
        L2=lblnk(DIV)
        L4=lblnk(SEQ)
        L3=lblnk(DOC)
        FSEQ=DIR(1:L1) // DIV(1:L2) // SEQ(1:L3)
        FDOC=DIR(1:L1) // DIV(1:L2) // DOC(1:L4)

        LOCSQ=.TRUE.
       
  100   Return

  901   Call Perror('[LOCSQ]:')
        Go to  100
        End 
