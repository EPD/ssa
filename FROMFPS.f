*$      Main program FROMFPS: Created 02/21/88; last update 03/09/92   *
*----------------------------------------------------------------------*
*       Author   : Philipp Bucher
*       Function : Extraction of DNA sequence matrix from database, 
*                  output in EMBL, FASTA, or GCG format 
*       Version  : UNIX 
*----------------------------------------------------------------------*
        Parameter         (NERR= 0)
        Parameter         (NINP= 5)
        Parameter         (NOUT= 6)
        Parameter         (NFPS=10)
        Parameter         (NSQF=11)
        Parameter         (NFIL=12)
        Parameter         (NPTR=13)
        Parameter         (NSEQ=14)
        Parameter         (NREF=15)

        Parameter         (IDM1=1024000)

        Character         CSEQ(IDM1)

        Character*80      RCIN
        Character*80      RCEX
        Character*80      RC80
        Character*80      RCFP

        Character*55      FTIT 
           Data              FTIT/' '/
        Character*10      FNAM 
           Data              FNAM/' '/
        Character*02      FCOD 
           Data              FCOD/' '/

        Character*33      CHFP
      
        Character*64      FOUT
        Character*16      FFIL
        Character*22      FDBE
        Character*16      FDBF
        Character*16      FSQF
        Character*08      FENT

        Character*01      CTRC
        Character*03      FTRC
        Character*01      CSTR
        Character*01      CFMT
        Character*01      CB
        Character*03      CDIV
        Character*06      ACCN 

        Integer           NFR(24)

        Integer*8         IPOS
        Integer*8         LDBE
        Integer*8         MB1
        Integer*8         MB2

* Start program exectution 

        IRC=0

* Interactive dialogue

        Call FROMFPSI
     *    (NERR,NINP,NFPS,NOUT,MB1,MB2,IC1,IC2,CTRC,CFMT,FOUT,IRC)
        If(IRC.NE.0) then 
           IRC=IRC+900
           Go to  100
        End if

* Read title line of FPS file 

    1   Read(NFPS,'(A)',Err=901) RCIN
           If(RCIN( 1: 2).EQ.'FP') go to   5
           If(RCIN( 1: 2).NE.'TI') go to   1
        Read(RCIN,'(5x,A10,A55,A2)',Err=902) FNAM,FTIT,FCOD 
    5   Continue

* Edit FPS name 

C       If(FNAM.EQ.' ') then 
C          Inquire(NFPS,Name=FFPS,Iostat=IOS)
C          If(IOS.EQ.0) then
C             L1=Rindex(FFPS,'/')+1
C             L2=Index(FFPS(L1:64),'.')-1
C             If(L2.LE.0) then 
C                L2=Lblnk(FFPS)
C             Else
C                L2=L1+L2-1
C             End if  
C             FOUT=FFPS(L1:L2)
C          Else 
C             FOUT='FROMFPS'
C          End if
C       Else 
C          FOUT=FNAM 
C       End if

C       L2=Lblnk(FOUT)
C       L1=MAX(1,Rindex(FOUT(1:L2),' '))
C       RC64=FOUT(L1:L2)
C       FOUT=RC64

* Edit FPS title

        If(FTIT.EQ.' ') FTIT='Unnamed functional position set.'

* Edit FPS code

        If(FCOD.EQ.' ') FCOD='FP'
        If(FCOD( 1: 1).EQ.' ') FCOD( 1: 1)='X'
        If(FCOD( 2: 2).EQ.' ') FCOD( 1: 2)='X'

* Edit first part of entry name, division code, acc-number, sequence type

        FENT=FCOD( 1: 2)
        CDIV=FNAM( 1: 3)
        Call UPCAS(CDIV)
        Write(0,*) CDIV
        If(CDIV.EQ.' ') CDIV='FPS'
        Write(0,*) CDIV
         
        If     (CTRC.EQ.'D') then 
           FTRC='DNA'
           CB='N'
        Else if(CTRC.EQ.'F') then 
           FTRC='NUC'
           CB='N'
        Else if(CTRC.EQ.'R') then 
           FTRC='RNA'
           CB='N'
        Else if(CTRC.EQ.'P') then 
           FTRC='PRT'
           CB='X'
        Else 
           FTRC='XXX'
           CB='X'
        End if

* Transform IC2 (length to position) 

        IC2=IC1+IC2-1 

* Open output file, do other preparations  

        If     (CFMT.EQ.'E') then 
           Open(NOUT,File=FOUT,Status='UNKNOWN',Err=903)
        Else if(CFMT.EQ.'F') then 
           Open(NOUT,File=FOUT,Status='UNKNOWN',Err=904)
        Else if(CFMT.EQ.'G') then 
           FFIL=FOUT
           Open(NFIL,File=FFIL,Status='UNKNOWN',Err=905)
        End if

* Initialize major loop variables

        MI=0                                                            
        IST=1                                                           
        FDBF=' '
        RCFP=' '
        LM=MB2-MB1+1

        Write(NERR,'('' '')') 

* Major loop: Sequential processing of FPS lines

   10      If(RCIN(1:2).EQ.'FP') go to  12 
           If(RCIN(1:2).EQ.'YZ') GO TO  99
   11   Read(NFPS,'(A)',Err=906,End= 99) RCIN 
        Go to 10

* Read from database or local nucleotide sequence data file

   12   RCFP=RCIN     
        Read(RCIN,'(30x,A33,1x,A6)',Err=801) CHFP,FENT( 3: 8)
        FSQF( 3: 8)=RCIN(65:70)

        If(CSTR.EQ.'+') IPOS=IPOS+MB1           
        If(CSTR.EQ.'-') IPOS=IPOS-MB2

        Call GETFP 
     *    (NPTR,NSEQ,NREF,CHFP,MB1,MB2,CTRC,CB,FDBE,LDBE,ITY,CSTR,
     *    IPOS,CSEQ,IDM1,IST,IRC)

           If(IRC.NE.0) then 
              IRC=IRC+100
              Go to  89
           End if   

* Edit entry name 
      
        Do  15 I1=1,8
           If(FENT(I1:I1).EQ.' ') FENT(I1:I1)='0'
   15   Continue

* Write sequence entry in FASTA format 

        If     (CFMT.EQ.'F') then 
           Write(RCEX,'(''>'',A8,'' '',A)',Err=802) FENT,RCFP(IC1:IC2)
           L=Lblnk(RCEX)
           If(Index('.,;',RCEX(L:L)).EQ.0) L=L+1
           Write(RCEX(L:80),'(''; range'',I6,'' to'',I6,''.'')',Err=803)
     *        MB1,MB2  
           Write(NOUT,'(A)',Err=804) RCEX(1:Lblnk(RCEX))
           Write(NOUT,'((60A))',Err=805)(CSEQ(ii1),ii1=1,LM)

           Read(NFPS,'(A)',Err=806,End= 79) RCIN
C	   RCIN=' '
	   GO TO 89
   79      RCIN(1:2)='YZ'
           Go to  89

* Define output unit for EMBL and GCG format  

        Else if(CFMT.EQ.'E') then
           ISQF=NOUT
        Else
           FSQF=FENT // '.seq'
           Open(NSQF,File=FSQF,Status='NEW',Err=807)
           ISQF=NSQF
        End if 

* Write sequence entry in EMBL/GCG format 

* - ID line
           
        If     (CTRC.NE.'P') then 
           Write(ISQF,
     *      '(''ID   '',A8,''   standard; '',A3,''; '',A3,''; '',I5,
     *        '' BP.'')',Err=808)
     *        FENT,FTRC,CDIV,LM
        Else 
           Write(ISQF,
     *      '(''ID   '',A8,''   standard; '',A3,''; '',I5,'' BP.'')',
     *        Err=809) FENT,FTRC,LM
        End if

* - AC line

        Write(ISQF,'(''XX'')',Err=810) 
        ACCN=FENT( 3: 8)
        If(ACCN( 1: 1).EQ.'0') ACCN( 1: 1)=FCOD( 1: 1)
        RCEX='AC   ' // ACCN // ';'
        Write(ISQF,'(A)',Err=810) RCEX(1:Lblnk(RCEX))

*  - DE line
 
        Write(ISQF,'(''XX'')',Err=811) 
        RCEX( 1: 5)='DE   '
        RCEX( 6:  )=RCFP(IC1:IC2)
        L=Lblnk(RCEX)
        If(Index('.,;',RCEX(L:L)).EQ.0) L=L+1
        Write(RCEX(L:80),'(''; range'',I6,'' to'',I6,''.'')',Err=812)
     *     MB1,MB2  
        Write(ISQF,'(A)',Err=813) RCEX(1:Lblnk(RCEX))

*  - CC   Source: line

        Write(ISQF,'(''XX'')',Err=814) 
        Write(ISQF,'(''CC   Source: '',A)',Err=815)
     *     FTIT(1:Lblnk(FTIT))
        Write(ISQF,'(''XX'')',Err=816) 

*  - FPS entry

        RCFP( 1: 2)='CC'
        Write(ISQF,'(A)',Err=817) RCFP(1:Lblnk(RCIN))     
        RCFP( 1: 2)='FP'

   80   Read(NFPS,'(A)',Err=818,End= 84) RCIN
           If(RCIN(1:2).EQ.'//') GO TO 85
           If(RCIN(1:2).EQ.'YZ') GO TO 85
           If(RCIN(1:2).EQ.'FP') GO TO 85
           RCIN( 1: 2)='CC'
           Write(ISQF,'(A)',Err=819) RCIN(1:Lblnk(RCIN))     
           Go to 80

   84   RCIN(1:2)='YZ'

*  - SQ line

   85   Write(ISQF,'(''XX'')',Err=820) 

        If(CTRC.EQ.'D') then   

           Do  86 I1=1,5
              NFR(I1)=0
   86      Continue

           Do  87 I1=IST,IST+LM-1
              If     (CSEQ(I1).EQ.'A') then 
                 CSEQ(I1)='a'
                 NFR(1)=NFR(1)+1 
              Else if(CSEQ(I1).EQ.'C') then 
                 CSEQ(I1)='c'
                 NFR(2)=NFR(2)+1 
              Else if(CSEQ(I1).EQ.'G') then 
                 CSEQ(I1)='g'
                 NFR(3)=NFR(3)+1 
              Else if(CSEQ(I1).EQ.'T') then 
                 CSEQ(I1)='t'
                 NFR(4)=NFR(4)+1 
              Else
                 CSEQ(I1)='n'
                 NFR(5)=NFR(5)+1 
              End if
   87      Continue

           Write(RC80,'(''SQ   Sequence '',I6,'' BP;'',
     *        I6,'' A; '',I6, '' C;'',I6, '' G; '',I6,'' T; '',
     *        I6, '' other; '')',Err=821) LM,(NFR(ii1),ii1=1,5)

           RCEX='SQ   '
              J1=5  
           Do  88 I1=6,80
              If(RC80(I1-1:I1).NE.'  ') J1=J1+1
              RCEX(J1:J1)=RC80(I1:I1)
   88      Continue
           Write(ISQF,'(A)',Err=821) RCEX(1:Lblnk(RCEX))
        Else 
           Write(ISQF,'(''SQ   Sequence; '',I5,'' BP;'',
     *        '' ..'')', Err=821) LM
        End if

        Write(ISQF,'((4x,6(1x,10A)))',Err=822)(CSEQ(ii1),ii1=IST,LM) 
        Write(ISQF,'(''//'')',Err=823) 

        MI=MI+1

* Add record to file of filenames in GCG format

        If(CFMT.EQ.'G') 
     *     Write(NFIL,
     *   '(A12,I4,'' +     1   1     3 '',I5,I4,''   1.00'')',Err=907)
     *     FSQF( 1:12),MI,MB1,16-MI 

* Done

   89   Write(NERR,'(A57,'' : RC='',I3)',Err=908) 
     *     RCFP(6:62),IRC
        If(IRC.NE.0) then
           IRC=0
           Go to  11
        Else 
           Go to  10
        End if 

* End of major loop

* Information on output files 

   99   If(MI.EQ.0) go to 100

        Write(NERR,*,Err=909) ' '
        If     (CFMT.EQ.'E') then 
           Write(NERR,*,Err=910) 
     *        'Output in ',FOUT(1:Lblnk(FOUT)),' .'
        Else if(CFMT.EQ.'F') then
           Write(NERR,*,Err=911) 
     *        'Output in ',FOUT(1:Lblnk(FOUT)),' .'
        Else if(CFMT.EQ.'G') then 
           Write(NERR,*,Err=912) 
     *        FFIL(1:Lblnk(FFIL)),' contains a list of sequence files.'
        End if
 
  100   If(IRC.NE.0) Write(NERR,1120) IRC
 1120   Format('Error termination (RC=',I3,').')
         
        Stop

* Error handling

  823   IRC=IRC+1
  822   IRC=IRC+1
  821   IRC=IRC+1
  820   IRC=IRC+1
  819   IRC=IRC+1
  818   IRC=IRC+1
  817   IRC=IRC+1
  816   IRC=IRC+1
  815   IRC=IRC+1
  814   IRC=IRC+1
  813   IRC=IRC+1
  812   IRC=IRC+1
  811   IRC=IRC+1
  810   IRC=IRC+1
  809   IRC=IRC+1
  808   IRC=IRC+1
  807   IRC=IRC+1
  806   IRC=IRC+1
  805   IRC=IRC+1
  804   IRC=IRC+1
  803   IRC=IRC+1
  802   IRC=IRC+1
  801   IRC=IRC+1

        If(CFMT.EQ.'G') then 
           Rewind(NSQF,Err= 89)
           Close(NSQF,Status='DELETE',Err= 89)
        End if

        Go to 89

  912   IRC=IRC+1
  911   IRC=IRC+1
  910   IRC=IRC+1
  909   IRC=IRC+1
  908   IRC=IRC+1
  907   IRC=IRC+1
  906   IRC=IRC+1
  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1

        Go to 100

        End
*----------------------------------------------------------------------*
        Include           'FROMFPSI.f'
        Include           'GETFP.f'
        Include           'UPCAS.f'
        Include           'PSQID.f'
        Include           'LOCSQ.f'
	Include           'RETSQ.f'
	Include           'STREV.f'
