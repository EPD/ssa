        Program FIND

* Sample input: *
*               *
*   EM:OCBGLOB  * 
*   6 'TATAAA'  *
*   6 'GGGCGG'  *
*               *
*---------------*

        Implicit       Character*01   (C)
        Implicit       Character*16   (F)
        Implicit       Character*80   (R)

        Parameter      (NSEQ=11)
        Parameter      (NPTR=12)
        Parameter      (NREF=13)
        Parameter      (NERR= 0)
*       Parameter      (NERR= 7)

        Parameter      (IDM1=250000000)

        Character      CSQ1(IDM1)
        Character      CSQ2(IDM1)

        Integer*8      ISEG
        Character*64   CSEG

        Character      COCC(10000)
        Integer*8      IOCC(10000) 
        Integer*8      IOC1(10000) 
        Integer*8      IOC2(10000) 
	Character*64   FDBE

        Character*80   RCEX

        Integer*8         IPOS
        Integer*8         LDBE
        Integer*8         LM

        IRC=0

           ITY=1
           IPOS=1
           IST=1 
           CSTR='+'
           LM=0
           LDBE=0

* Read seq, segments from std
      
C          Read(5,"(A)") FDBE
C          KSEG=0
C   1      Read(5,"(A)",End=10) RC80
C          KSEG=KSEG+1
C          Read(RC80,*) ISEG(KSEG),CSEG(KSEG)
C          Go to   1 

           N1=Iargc()
           If(N1.NE.2) then 
              Write(NERR,'(''Usage: newfind seq_id string '',
     *         ''( e.g. newfind VRL:SV40XX GGGCGG )'')') 
              Stop
           End if

           Call GetArg(1,FDBE)
           Call GetArg(2,CSEG)
           ISEG=Lblnk(CSEG)

C  10      Call
C    *        RETSQ(NSEQ,FDBE,LDBE,ITY,IPOS,LM,CSQ1,IDM1,IST,IRC)
   10   Call RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LDBE,ITY,IPOS,LM,CSQ1,IDM1,IST,CC,IRC)
           CB='B'
           CTRC='D'
           Call STREV(CSQ1,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  
C	Write(6, *) 'Length of requested sequence segment LM = ', LM
           Do  15 I1=1,LM
              CSQ2(I1)=CSQ1(I1)
   15      Continue

           CC='U'
           CSTR='-'
           Call STREV(CSQ2,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  

           KOCC=0
 
* Plus strand 

           Do  20 I2=0,LM-ISEG
              Do  19 I3=1,ISEG
              If(CSEG(I3:I3).NE.CSQ1(I2+I3)) Go to  20
   19         Continue
              KOCC=KOCC+1  
              COCC(KOCC)='+'
              IOCC(KOCC)=I2+1
              IOC1(KOCC)=-I2
              IOC2(KOCC)=-I2+LDBE
   20      Continue         

* Minus strand 

           Do  30 I2=0,LM-ISEG
              Do  29 I3=1,ISEG
                 If(CSEG(I3:I3).NE.CSQ2(I2+I3)) Go to  30
   29         Continue
              KOCC=KOCC+1  
              COCC(KOCC)='-'
              IOCC(KOCC)=LDBE-I2
              IOC1(KOCC)=LDBE-I2
              IOC2(KOCC)=-I2
   30      Continue         

* Print sequence positions

           If(KOCC.GE.1) then
              Do  50 I2=1,KOCC
                 Write(RCEX,'(''     '',A21,A1,I11,''   '' 
     *            ,''['',I12,'','',I12,'']'')')
     *              FDBE,COCC(I2),IOCC(I2),IOC1(I2)
     *             ,IOC2(I2)
                 IX=Index(RCEX,'[')+1
                 Do I3=IX,80
                    If(RCEX(I3-1:I3).NE.'  '.AND.
     *                 RCEX(I3-1:I3).NE.'[ ') then 
                       RCEX(IX:IX)=RCEX(I3:I3)
                       IX=IX+1
                    End if
                 End do 
                 RCEX(IX:)=' '
                 Write(6,'(132A)')(RCEX(ii1:ii1),ii1=1,Lblnk(RCEX))
                 
   50         Continue
           Else
              Print *,'     Not found' 
           End if

        Stop
        End

        Include           'RETSQ.f' 
        Include           'STREV.f' 
	Include           'PSQID.f'
	Include           'LOCSQ.f'
