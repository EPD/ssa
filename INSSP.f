C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : INTERACTIVE INPUT OF SIGNAL SEARCH PARAMETERS        
C       ****************************************************************
                                                                        
        SUBROUTINE INSSP(NINP,NERR,NFPS,NSSC,MB1,MB2,CCTY,LS,LE,I,RN,
     *     LC,LD,LF,LH,PR,VPR,IDM1,IRC)                                           
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        REAL         VPR(IDM1)                                          
        REAL         RN
        CHARACTER*55 TDSM                                                
        Character*192 FFPS
        Character*192 RC80
                                                                        
        IRC=0                                                           
        IR=0                                                            
        IC=0                                                            
        IDEV=0                                                          
        FISSC='    -  '                                                 
                                                                        
* Beginning of interactive dialogue

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901)
     *    'Program CRSSD / Beginning of interactive dialogue:'
        Write(NERR,*,Err=901)

* Open FPS file, read Title (TDSM)

   10   FFPS='Fps.fps'
        Write(NERR,1011,Err=901) FFPS( 1: 7)
 1011   Format
     *   ('FPS file (default is ',A7,'): ',$)
        Read(NINP,"(A)",Err= 15) RC80
        If(RC80.EQ.' ') go to 12
        Read(RC80,"(A)",Err= 15) FFPS
   12   Open(NFPS,File=FFPS,Status='OLD',Err= 15)
        Go to  16
   15   Write(NERR,1099,Err=901)
        Go to  10

   16   TDSM=' '
   17   Read(NFPS,"(A)",Err=902,End=18) RC80
           If(RC80( 1: 2).EQ.'FP') go to  18
           If(RC80( 1: 2).EQ.'TI') go to  19
           Go to  17
   18   RC80(16:70)='Unnamed functional position set' 

   19   Rewind(NFPS,Err=903)
        TDSM=RC80(16:70)
        If(IC.NE.0) Go to  90

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901) TDSM 
        Write(NERR,*,Err=901) ' '

* 5' border (MB1)
           
   20   MB1=-99
        Write(NERR,1021,Err=901) MB1
 1021   Format
     *   ('Left (5''OH) border (default is',I5,'): ',$)         
        Read(NINP,"(A)",Err= 29) RC80                               
        If(RC80.EQ.' ') go to 25                                  
        Read(RC80,*,Err= 24) MB1                                      

   22   Go to  25 

   24   Write(NERR,1099,Err=901)
        Go to  20 

* 3' border (MB2)

   25   MB2=MB1+199
        Write(NERR,1026,Err=901) MB2
 1026   Format
     *   ('Right (3''OH) border (default is',I5,'): ',$)      
        Read(NINP,"(A)",Err= 29) RC80                               
        If(RC80.EQ.' ') go to  27 
        Read(RC80,*,Err= 29) MB2                                      

   27   If(MB2.LE.MB1) go to  29                                        
        LM=MB2-MB1+1
        If(IC.NE.0) go to  90
        Go to  30 

   29   Write(NERR,1099,Err=901)
        Go to  25 

   30   CCTY='C'                                                        
        Write(NERR,1030) CCTY                                                 
 1030   FORMAT('Type of signal sequence collection ',/,           
     * '(C=complete, R=random, U=user-defined; default='           
     *,A1,'): ',$)                                                         
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 31                                   
        READ(RC80,2003,ERR=38) CCTY                                     
   31   IF(CCTY.EQ.'U') GO TO 32                                        
        IF(CCTY.EQ.'C'.OR.CCTY.EQ.'R') GO TO 40                         
   32   READ(NSSC,4000,END=37,ERR=37) RC80                              
        IF(RC80(1:2).NE.'C ') GO TO 32                                  
        READ(RC80,4002,ERR=37) FISSC,LS,LE,I                            
        GO TO 59                                                        
   37   Write(NERR,1037)                                                     
 1037   FORMAT('Error detected when reading SSC file; change type of ' 
     A,'signal sequence collection.')                                   
   38   Write(NERR,1099)                                                       
        GO TO 30                                                        
                                                                        
   40   IF(CCTY.NE.'U') GO TO 41                                        
        Write(NERR, 1040)                                                      
 1040   FORMAT('This parameter cannot be changed for user-defined'     
     A,'signal sequence collection.')                                
        GO TO 100                                                       
   41   IF(CCTY.EQ.'C') LS=2                                            
        IF(CCTY.EQ.'R') LS=6                                            
        Write(NERR,1041) LS                                                    
 1041   FORMAT('Specify signal sequence length (default is ',I3,'): ',$)  
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 42                                   
        READ(RC80,2002,ERR=48) LS                                       
   42   LE=LS                                                           
        IF(CCTY.EQ.'R') GO TO 50                                        
        IF(CCTY.EQ.'C') I=4**LS                                         
        GO TO 59                                                        
   48   Write(NERR,1099)                                                      
        GO TO 40                                                        
                                                                        
   50   IF(CCTY.EQ.'R') GO TO 51                                        
        Write(NERR,1050)                                                      
 1050   FORMAT('This parameter can only be changed for random'         
     A,' signal sequence collections !')                                
        GO TO 100                                                       
   51   I=200                                                           
        Write(NERR,1051) I                                                    
 1051   FORMAT ('Specify number of signal sequences (Default is ',I6   
     A,'): ',$)                                                            
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 55                                   
        READ(RC80,2002,ERR=53) I                                        
        GO TO 55                                                        
   53   Write(NERR,1099)                                                      
        GO TO 50                                                        
                                                                        
   55   RN=0.5                                                            
        Write(NERR,1055) RN                                                   
 1055   FORMAT('Enter a real number between 0 and 1 to start'      
     A,'random number generator ',/,' (Default is ',F6.4,'): ',$)            
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 59                                   
        RC80(80:80)='@'
        READ(RC80,*,ERR=58) RN                                       
        RN=Real(INT(RN*9999))/10000
        IF(RN.GE.0.AND.RN.LE.1) GO TO 59                             
   58   Write(NERR,1099)
                                                      
        GO TO 55                                                        
                                                                        
   59   IF(IC.EQ.0) GO TO 60                                            
        IF(LC.GE.LE.AND.LC.LE.LM) GO TO 90                              
   60   IF(LE.GT.LM) GO TO  20                                           
        LC=LE                                                           
        Write(NERR,1060) LC                                                   
 1060   FORMAT ('Window width (Default is ',I3,'): ',$)            
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 61                                   
        READ(RC80,2002,ERR=68) LC                                       
        IF(LC.LT.LE.OR.LC.GT.LM) GO TO 68                               
   61   IF(IC.NE.0) GO TO 90                                            
        GO TO 70                                                        
   68   Write(NERR,1099)                                                      
        GO TO 60                                                        
                                                                        
   70   LD=(LC-LE+1)/2                                                  
        IF(LD.EQ.0) LD=1                                                
        Write(NERR,1070) LD                                                   
 1070   FORMAT ('Window shift (Default is ',I3,'): ',$)           
        READ(NINP,4000,ERR=999,END=999) RC80                               
        IF(RC80.EQ.' ') GO TO 79                                   
        READ(RC80,2002,ERR=78) LD                                       
        IF(LD.GE.1) GO TO 79                                            
   78   Write(NERR,1099)                                                      
        GO TO 70                                                        
                                                                        
   79   IF(IC.EQ.0) GO TO 80                                            
        IF(LD/LF*LF.EQ.LD) GO TO 90                                     
   80   LF=1                                                            
        Write(NERR,1080) LF                                                   
 1080   FORMAT ('Signal shift (Default is ',I3,'): ',$)           
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 90                                   
        READ(RC80,2002,ERR=88) LF                                       
   81   IF(LD/LF*LF.EQ.LD) GO TO 90                                     
        Write(NERR,1081)                                                      
 1081   FORMAT('Window shift must be equal or multiple of signal '     
     A,'shift.')                                                        
   88   Write(NERR,1099)                                                      
        GO TO 80                                                        
                                                                        
                                                                        
   90   PR=0                                                            
        LH=0                                                            
        L=1+(LC-LE)/LF                                                  
        DO 91 I1=0,LS-1                                                 
           K=LS-I1                                                      
           W=0.25                                                       
           N=LS                                                         
           PK=0
           CALL MDBIN(K,N,W,PK)                                  
           PR=PR+PK                                                     
           VPR(I1+1)=1-(1-PR)**L                                        
           IF(VPR(I1+1).GE.0.5.AND.LH.EQ.0.AND.I1.NE.0) LH=LS+1-I1      
   91   CONTINUE                                                        
                                                                        
        VPR(LS+1)=1                                                     
        IF(LS.GE.2.AND.LH.LE.1) LH=2                                    
        IF(LS.EQ.1.) LH=1                                               
        Write(NERR,*) ' '
        Write(NERR,1092)                                                      
 1092   FORMAT ('Occurrence probabilities for different matching '     
     A,'criteria:',/)                                                    
                                                                        
        DO 94 I1=0,LS                                                   
           K=LS-I1                                                      
           IF(VPR(I1+1).LT.0.0001) GO TO 94                             
           Write(NERR, 1093) K,VPR(I1+1)                                       
 1093      FORMAT('Minimal # of matches: ',I3,'   Occ. pr. P=',F6.4)       
           IF(VPR(I1+1).GT.0.9999) GO TO 95                             
   94   CONTINUE                                                        
                                                                        
   95   Write(NERR,1095) LH                                                   
 1095   FORMAT(/,'Minimal # of matches (Default is ',I3,'): ',$)        
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80(1:1).EQ.' ') GO TO 99                                   
        READ(RC80,2002,ERR=98) LH                                       
        IF(LH.GE.1.AND.LH.LE.LS) GO TO 99                               
   98   Write(NERR,1099)                                                      
        GO TO 90                                                        
                                                                        
   99   PR=VPR(LS-LH+1)                                                 
  100   IF(CCTY.NE.'R') IR=0                                            
        CALL PISSP(FFPS,TDSM,MB1,MB2,CCTY,FISSC,LS,LE,I,RN,LC           
     A,LD,LF,LH,PR,IDEV,IRC)                                            
        IF(IRC.EQ.0) GO TO 101                                          
        IRC=IRC+9                                                       
        GO TO 120                                                       
                                                                        
  101   IC=0                                                            
        Write(NERR,1101)                                                      
 1101   FORMAT(/,'Enter number of item you want to change or <CR> if '  
     A,/,'all items are correct: ',$)                                    
        READ(NINP,4000,END=999,ERR=999) RC80                               
        IF(RC80.EQ.' ') GO TO 102                                  
        READ(RC80,2002,ERR=999) IC                                      
                                                                        
  102   IF(IC.EQ.0) GO TO 120                                           
        GO TO (10,20,30,40,50,60,70,80,90) IC                           
                                                                        
  120   RETURN                                                          
                                                                        
  904   IRC=IRC+1                                                       
  903   IRC=IRC+1                                                       
  902   IRC=IRC+1                                                       
  901   IRC=IRC+1                                                       
        GO TO 120                                                       
  999   IRC=999                                                         
        GO TO 120                                                       
                                                                        
 1099   FORMAT('Input not accepted; try again !')                      
                                                                        
 2001   FORMAT(A7)                                                      
 2002   FORMAT(BN,1I5)                                                  
 2003   FORMAT(A1)                                                      
 4000   FORMAT(A)                                                     
 4002   FORMAT(2X,A7,4X,I3,1X,I3,1X,I6)                                 
                                                                        
        END                                                             

        Include 'MDBIN.f'
