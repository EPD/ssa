#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main(){

const char fname[256] = "/db/embl/svid.ptr";
int fd;
FILE *fp;
int i = 0;
int buflen = 64;
char *tmpbuff;
char *res;
int BUF_SIZE=256;


printf("read_ptr: file name %s\n", fname);
printf("testIO: file len %d\n", strlen(fname));

  if ((fd = open(fname, O_RDONLY)) == -1) {
    perror("\nread_ptr open");
    return -1;
  }
printf("testIO: file descriptor  %d\n", fd);

  if ((fp = fopen(fname, "r")) == NULL) {
    perror("\nread_ptr fopen");
    return -1;
  }

  tmpbuff = malloc(sizeof(char) * (buflen + 2));
  while (((res = fgets(tmpbuff, BUF_SIZE, fp)) != NULL) && i < 10) {
    printf("line %d: %s", i, tmpbuff);
    i++;
  }

return 0;
}
