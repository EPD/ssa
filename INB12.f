C       ****************************************************************INB00020
C       AUTHOR   : PHILIPP BUCHER                                       INB00030
C       FUNCTION : INTERACTIVE INPUT OF BORDERS OF DNA SEQUENCE MATRIX  INB00040
C       ****************************************************************INB00050
                                                                        INB00060
        SUBROUTINE INB12(NFPS,TIT,NB1,NB2,MB1,MB2,ID1,ID2,IOPT,IRC)     INB00070
                                                                        INB00080
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) INB00090
                                                                        INB00100
        CHARACTER*50 TIT                                                INB00110
                                                                        INB00120
        IRC=0                                                           INB00130
    5   PRINT 1005,TIT                                                  INB00140
 1005   FORMAT(1H0,1A50)                                                INB00150
        IF(IOPT.EQ.0) GO TO 20                                          INB00160
        IF(IOPT.EQ.1) PRINT 1011,NB1,NB2                                INB00170
 1011   FORMAT(' Input file contains data from pos. ',I5,' to ',I5)     INB00180
        IF(IOPT.EQ.2) PRINT 1012,NB1,NB2                                INB00190
 1012   FORMAT(' Input file contains data from pos. ',I5,' to',I5       INB00200
     A,'. These limits must not',/,' be exceeded !')                    INB00210
                                                                        INB00220
   20   MB1=ID1                                                         INB00230
        PRINT 1020,MB1                                                  INB00240
 1020   FORMAT(' Specify left (5''OH) border (default=',I5,')')         INB00250
        READ(5,4000,END=999,ERR=999) RC80                               INB00260
        IF(RC80(1:1).EQ.'/') GO TO 30                                   INB00270
        IF(RC80(1:4).EQ.'EXIT') GO TO 901                               INB00280
        READ(RC80,2000,ERR=20) MB1                                      INB00290
                                                                        INB00300
   30   MB2=ID2                                                         INB00310
        PRINT 1030,MB2                                                  INB00320
 1030   FORMAT(' Specify right (3''OH) border (default is',I5,')')      INB00330
        READ(5,4000,END=999,ERR=999) RC80                               INB00340
        IF(RC80(1:1).EQ.'/') GO TO 40                                   INB00350
        IF(RC80(1:4).EQ.'EXIT') GO TO 901                               INB00360
        READ(RC80,2000,ERR=30) MB2                                      INB00370
                                                                        INB00380
   40   IF(MB1.GT.MB2) GO TO 50                                         INB00390
        IF(IOPT.LE.1) GO TO 100                                         INB00400
        IF(MB1.GE.NB1.AND.MB2.LE.NB2) GO TO 100                         INB00410
                                                                        INB00420
   50   PRINT 1050                                                      INB00430
 1050   FORMAT('0Bad borders specified. Try again !')                   INB00440
        GO TO 5                                                         INB00450
                                                                        INB00460
  100   RETURN                                                          INB00470
                                                                        INB00480
  901   IRC=1                                                           INB00490
        GO TO 100                                                       INB00500
  999   IRC=999                                                         INB00510
        GO TO 100                                                       INB00520
                                                                        INB00530
 2000   FORMAT(BN,1I6)                                                  INB00540
 4000   FORMAT(A80)                                                     INB00550
                                                                        INB00560
        END                                                             INB00570
