C$      Subroutine GETFP   : Created 02/18/92 ; last update 02/18/92    
C       *--------------------------------------------------------------*
C       Author    : Philipp Bucher                                       
C       Function  : Parsing of FP reference, call of RETSQ and STREV 
C       Included  : Subroutine RETSQ    
C       Arguments : NPTR (RW) : Logical unit number for pointer file
C                   NSEQ (RW) : Logical unit number of database file
C                   NREF (RW) : Logical unit number of database file
C                   CHFP (R-) : Unparsed functional position reference  
C                   MB1  (R-) : Left (5'OH) border of SQ segment 
C                   MB2  (R-) : Right (3'OH) border of SQ segment 
C                   CTRC (R-) : Translation code
C                   CB   (R-) : Character for unknown residue 
C                   FDBE (RW) : Database sequence identifier
C                   LDBE (RW) : Length of database sequence
C                   ITY  (-W) : Type of sequence (linear or circular)
C                   CSTR (-W) : DNA strand (+ or -)
C                   IPOS (-W) : Starting position of SQ segment in 
C                               in source sequence
C                   CSEQ (-W) : Character/Byte array for internal 
C                               sequence storage 
C                   IDM1 (R-) : Dimension of CSEQ
C                   IST  (R-) : Starting position of SQ segment in 
C                               CSEQ/ 
C                   IRC  (-W) : Return code                    
C       *--------------------------------------------------------------*
                                                                        
        Subroutine GETFP
     *    (NPTR,NSEQ,NREF,CHFP,MB1,MB2,CTRC,CB,FDBE,LDBE,ITY,CSTR,IPOS,
     *     CSEQ,IDM1,IST,IRC)
 
        Character         CSEQ(IDM1)

        Character*(*)     CHFP
        Character*(*)     FDBE

        Character*01      CSTR
        Character*01      CTRC
        Character*01      CC 
        Character*01      CB

        Integer*8         IPOS
        Integer*8         LDBE
        Integer*8         LM
        Integer*8         MB1
        Integer*8         MB2
      
        IRC=0

* Parse functional position reference 

        Read(CHFP,'(A21,I1,A1,I10)',Err=901) FDBE,ITY,CSTR,IPOS

        LM=MB2-MB1+1
        If(CSTR.EQ.'+') IPOS=IPOS+MB1           
        If(CSTR.EQ.'-') IPOS=IPOS-MB2
 
        Call RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LDBE,ITY,IPOS,LM,CSEQ,IDM1,IST,CC,IRC)
        If(IRC.NE.0) then
           IRC=IRC+100
           Go to 100 
        End if
C	Write (0,*) IST,LM
C	Write(*,'((60A))',Err=901)(CSEQ(ii1),ii1=1,LM)

        Call STREV(CSEQ,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  
        If(IRC.NE.0) then
           IRC=IRC+200
           Go to 100
        End if

  100   Return
  
  901   IRC=IRC+1
        Go to 100

        End   

*----------------------------------------------------------------------*
*        Include           'RETSQ.f'
*        Include           'STREV.f'
