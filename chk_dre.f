*----------------------------------------------------------------------*
*       Author   : Philipp Bucher
*       Function : Data integrity control EPD, compares EMBL DR lines
*                  with control string on SE line
*----------------------------------------------------------------------*
        Parameter         (NERR= 7)
        Parameter         (NOUT= 6)
        Parameter         (NINP= 5)
        Parameter         (NSQF=11)
        Parameter         (NPTR=13)
        Parameter         (NSEQ=14)
        Parameter         (NREF=15)

        Parameter         (IDM1=16384)

        Character         CSEQ(IDM1)

        Character*80      RCIN
        Character*80      RCFP

        Character*33      CHFP
      
        Character*16      FDBE
        Character*16      FDBF
        Character*16      FSQF
        Character*08      FENT

        Character*01      CTRC
        Character*01      CSTR
        Character*01      CB

	Integer*8         IPOS
	Integer*8         LDBE
	Integer*8 	  MB1
	Integer*8 	  MB2

* Variables for control string

        Character         CTRS(132)
        Character         CDIF(132)
        Integer           NCS1
        Integer           NCSB

* Variables for DR lines

        Character*33      DRFP(1024)
        Character*80      CHDR(1024)
        Integer           NDR

* Additional fields

        Character*8       CHAC
        Character*132     RCEX
        Logical           LERR

* Start program exectution 

        IRC=0

* initialize variables for FP line extraction

        CTRC='D'
        CB='N'
        IST=1                                                           
        FDBF=' '
        RCFP=' '

* major loop: Sequential processing of FP lines

   10   Read(NINP,'(A)',Err=900,End=100) RCIN 

        If     (RCIN(1:2).EQ.'AC') then
        
* new entry:

           NDR=0
           CHAC=RCIN(6:12)

        Else if(RCIN(1:9).EQ.'DR   EMBL') then
        
* new DR

           NDR=NDR+1
           CHDR(NDR)=RCIN
             Call dr2chfp(RCIN,DRFP(NDR),IRC) 
           If(IRC.NE.0) then
              RCEX='Fmt-error: ' // RCIN
              Write(NOUT,'(132A)')
     *          (RCEX(ii1:ii1),ii1=1,Lblnk(RCEX))
              IRC=0
              NDR=NDR-1
           End if   

        Else if(RCIN(1:2).EQ.'SE') then

* control string 

           Call se2chfp(RCIN,CTRS,MB1,MB2,IRC)
           If(IRC.NE.0) then
              RCEX='Fmt-error: ' // RCIN
              Write(NOUT,'(132A)')
     *          (RCEX(ii1:ii1),ii1=1,Lblnk(RCEX))
              NDR=0
           End if 
           LM=MB2-MB1+1

        Else if(RCIN(1:2).EQ.'FP') then

* FP line
           Do I1=1,NDR
           Call GETFP 
     *       (NPTR,NSEQ,NREF,DRFP(I1),MB1,MB2,CTRC,CB,FDBE,LDBE,
     *        ITY,CSTR,IPOS,CSEQ,IDM1,IST,IRC)
              If(IRC.NE.0) then 
                 IRC=IRC+100
                 Go to  89
              End if

   89         If(IRC.NE.0) 
     *         Write(NERR,'(A33,'' : EPD='',A,'' : RC='',I3)', Err=900) 
     *         DRFP(I1),CHAC,IRC
              If(IRC.NE.0) 
     *	       Write(NOUT,'(A33,'' : EPD='',A,'' : RC='',I3)', Err=900) 
     *        DRFP(I1),CHAC,IRC
              IRC=0

* - test identity 

                 LERR=.FALSE.
              Do I2=1,LM
                 If(     CTRS(I2).NE.'N'
     *              .AND.CSEQ(I2).NE.'N'
     *              .AND.CTRS(I2).NE.CSEQ(I2) ) then 
                    LERR=.TRUE. 
                    CDIF(I2)='*'
                 Else
                    CDIF(I2)=' '
                 End if
              End do


* - too many N's ? 

        	 K1=0
              Do I2=1,LM
        	 If(CSEQ(I2).NE.'N') K1=K1+1
              End do
              If(K1.LE.LM/2) LERR=.TRUE.

        	   

   95         If(LERR) then
                 RCEX='Ref-error: ' // CHDR(I1)
                 Write(NOUT,'(132A)')
     *          (RCEX(ii1:ii1),ii1=1,Lblnk(RCEX))
                 Write(NOUT,'('''')')
                 Write(NOUT,'(''   DR: '',132A)') (CSEQ(ii1),ii1=1,LM)
                 Write(NOUT,'(''       '',132A)') (CDIF(ii1),ii1=1,LM)
                 Write(NOUT,'(''   SE: '',132A)') (CTRS(ii1),ii1=1,LM)
                 Write(NOUT,'('''')')
                 RCEX='   FP: ' // RCIN(6:)
                 Write(NOUT,'(132A)')
     *             (RCEX(ii1:ii1),ii1=1,Lblnk(RCEX))
                 Write(NOUT,'('''')')
              End if
 
           End do 

        End if 

        Go to  10

* end of major loop

  100   If(IRC.NE.0) Write(NERR,1120) IRC
 1120   Format('Error termination (RC=',I3,').')
        Stop

  900   IRC=900        
        Go to 100

        End
*----------------------------------------------------------------------*
          Subroutine dr2chfp(RCIN,DRFP,IRC) 

        Character*(*)      RCIN
        Character*33       DRFP

        IRC=0

C       I1=Index(RCIN(11:),';')+10
C       I2=Index(RCIN(I1+1:),';')+I1
C       DRFP='EP:' // RCIN(I1+2:I2-1)

        I1=Index(RCIN(11:),';')+10
        I2=Index(RCIN(I1+1:),';')+I1
        DRFP='SV:' // RCIN(12:I1-1)

        I1=Index(RCIN,'[')
        I2=Index(RCIN(I1+1:),',')+I1
        I3=Index(RCIN(I2+1:),']')+I2

        If(I2.LE.I1.OR.I3.LE.I2) go to 900
        Read(RCIN(I1+1:I2-1),*,Err=900) N1
        Read(RCIN(I2+1:I3-1),*,Err=900) N2
        If(N1.LT.N2) then 
           Write(DRFP(24:33),'(I10)') 1-N1
           DRFP(23:23)='+'
        Else    
           Write(DRFP(24:33),'(I10)') N1
           DRFP(23:23)='-'
        End if            
           
        DRFP(22:22)='1'
        
        Return
  900   IRC=-1
        Return
        End
*----------------------------------------------------------------------*
        Subroutine se2chfp(RCIN,CTRS,MB1,MB2,IRC)

        Character*(*)      RCIN
        Character          CTRS(*)

        Character*4        ABCU
        Character*4        ABCL

        Data               ABCU /'ACGT'/
        Data               ABCL /'acgt'/
	Integer*8 	   MB1
	Integer*8 	   MB2

        NU=0
        NL=0
        Do I1=6,Lblnk(RCIN)
           K1=Ichar(RCIN(I1:I1))
           If     (RCIN(I1:I1).EQ.' ') then  
              Continue
           Else if(Index(ABCU,RCIN(I1:I1)).NE.0) then  
              NU=NU+1
              CTRS(NU+NL)=RCIN(I1:I1)
           Else if(K1.GE.65.AND.K1.LE. 90) then  
              NU=NU+1
              CTRS(NU+NL)='N'
           Else if(Index(ABCL,RCIN(I1:I1)).NE.0) then  
              If(NU.GT.0) go to 900
              NL=NL+1
              N1=Index(ABCL,RCIN(I1:I1))
              CTRS(NL)=ABCU(N1:N1)
           Else if(K1.GE.97.AND.K1.LE.122) then  
              If(NU.GT.0) go to 900
              NL=NL+1
              CTRS(NL)='N'
           Else
              Go to 900
           End if
        End do

        MB1=-NL
        MB2=NU-1

        Return
  900   IRC=-1
        Return
        End
*----------------------------------------------------------------------*
        Include           'GETFP.f'
        Include           'RETSQ.f'
        Include           'STREV.f'
        Include           'PSQID.f'
        Include           'LOCSQ.f'
        Include           'rindex.f'
        Include           'lblnk.f'
