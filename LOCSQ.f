        Logical Function
     *     LOCSQ(NPTR,FNAM,SQID,LSEQ,FMT,FSEQ,FDOC,RCL,IBS,IBD,CC)

        Character*(*)     FNAM
        Character*(*)     SQID 
        Character*(*)     FSEQ
        Character*(*)     FDOC 

        Integer*8         STATB8
	Integer*4	  Istatus
	Integer*8	  lsize

        Integer*8         IBS
        Integer*8         IBD
        Integer*8         JL
        Integer*8         IR
        Integer*8         JM
        Integer*8         J0
        Integer*8         JU
        Integer*8         LSEQ

	Real*8            D

        Character         FMT
        Character*64      DIR
        Character*64      SEQ
        Character*64      DOC
        Integer           KLN
        Integer           RCL
        Character*01      CAS
        Character*01      CC

        Character*32      DIV

        Character*64      CHRS
        Character*64      RCIN
	Character*64      CLINE
	Integer           INULL
        Character*03      Mode
        Character*02      CNULL
	Equivalence       (INULL,CNULL)

        CNULL=' '
	INULL=0
        LOCSQ=.FALSE.
        IBS=-1
        IBD=-1

        Inquire(NPTR,Name=CHRS)
        If(CHRS.EQ.FNAM) go to  10 
	Mode='r'//CNULL
	Call cOpen(NPTR, FNAM, Mode, Istatus)
	If (Istatus.NE.0) goto 901
	lsize=64 
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
        Read(CLINE,'(8x,A1 )') FMT 
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
	Read(CLINE,'(8x,A55)') DIR
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
	Read(CLINE,'(8x,A55)') SEQ
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
	Read(CLINE,'(8x,A55)') DOC
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
	Read(CLINE,'(8x,I4 )') KLN
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
	Read(CLINE,'(8x,I4 )') RCL
	Call cGetline(NPTR,CLINE,lsize,Istatus)
	If (Istatus.NE.0) goto 901
        Read(CLINE,'(8x,A1 )') CAS 

	Call cFtell(NPTR, JL, Istatus)
	If (Istatus.NE.0) goto 901
	JL=JL-64
	Call cFstat(NPTR, STATB8, Istatus)
	If (Istatus.NE.0) goto 901
        JU=STATB8-64
        J0=INT  (LOG(Real(JU-JL)/64)/0.69314718)
        D=(2**J0)*64     
        J0=D    

   10   CC=CAS
        IR=JL+J0 
        JM=J0

   15   If(JM.LT.64) go to 101 

	Call cFseek(NPTR,IR,0,Istatus)
	If (Istatus.NE.0) goto 901
        JM=JM/2
           
	Call cGetline(NPTR,RCIN,lsize,Istatus)
	If (Istatus.NE.0) goto 901
        If     (SQID.GT.RCIN(1:KLN)) then  
           IR=MIN(IR+JM,JU) 
           Go to  15
        Else if(SQID.LT.RCIN(1:KLN)) then 
           IR=IR-JM
           Go to  15
        End if
 
        Read(RCIN,'(A32,I10,I10,I11)') CHRS,LSEQ,IBS,IBD 
        DIV=RCIN(KLN+1:32)
        IBS=IBD+IBS

        L1=lblnk(DIR)
        L2=lblnk(DIV)
        L3=lblnk(SEQ)
        L4=lblnk(DOC)
        FSEQ=DIR(1:L1) // DIV(1:L2) // SEQ(1:L3)
        FDOC=DIR(1:L1) // DIV(1:L2) // DOC(1:L4)

	If (Istatus.NE.0) goto 901
	If(SQID(1:KLN).NE.CHRS(1:KLN)) goto 901
	
        LOCSQ=.TRUE.

  100   Call cClose(NPTR, Istatus)
  101   Return

  901   Call Perror('[LOCSQ]:')
        Go to  100
        End 
