C$      Main program PATOP : Created 11/01/88 ; last update 04/04/10   
*       *-------------------------------------------------------------*
*       Author   : Philipp Bucher
*       Function : "Pattern optimization" = Iterative refinement of a
*                  locally over-represented sequence motif
*                  (weight matrix + cut-off value + preferred region)      
*       *-------------------------------------------------------------*

        Parameter     (IDM1=201326592)
        Parameter     (IDM2=524288)
        Parameter     (IDM3=512)
        Parameter     (IDM4=64)

        Parameter     (NERR= 0)
        Parameter     (NINP= 5)         
        Parameter     (NOUT= 6)

        Parameter     (NFPS=10)
        Parameter     (NPTR=11)
        Parameter     (NSEQ=12)
        Parameter     (NREF=13)
        Parameter     (NDSM=14)
        Parameter     (NSCO=15)
        Parameter     (NDBF=16)
        Parameter     (NWMX=17)

        Integer*8         IPOS
        Integer*8         LDBE
        Integer*8         LM
        Integer*8         MB1
        Integer*8         MB2

        Logical       LICO
        Logical       LFPR


* Arrays corresponding to DNA sequence matrix:

        Byte          ISEQ(IDM1)
        Character     CSEQ(IDM2)
        Byte          IROW(IDM2)
        Byte          B

        Byte          ISCO(IDM1)
        Real          XPO(IDM2)
        Integer       IPO(IDM2)

        Byte          ISFM(IDM1)
        Integer       ISS(IDM2)
        Integer       ISF(IDM2)

* Arrays corresponding base frequency table and weight matrix: 

        Integer       ICR(IDM3,4)

        Integer       IBF(IDM4,4)
        Integer       IBFP(IDM4,4)
        Real          WMX(IDM4,4)

        Integer       IBC(4)
        Real          RBC(4)
        Character     CBC(4)
        Integer       IBCP(4)
        Integer       IBC0(4)
        Integer       IDC(4,4)

* Various character fields
  
        Character*80  RC80
        Character*03  CIRC
        Character*22  FDBE
        Character*55  TDSM 

        Character*33  CHFP

        Character*01  CSM
        Character*01  CBD
        Character*01  CNM
        Character*01  CSTR
        Character*01  CB
        Character*01  CC
        Character*01  CTRC

* Equivalence 
      
        Equivalence  (ISEQ(1),ISCO(1),ISFM(1))
        Equivalence  (ISS(1),IPO(1),ICR(1,1))

* Data
        Data          IBC/4*0/,IBCP/4*0/,IDC/16*1/
        Data          ISEQ/IDM1*0/
        Data          CBC/'A','C','G','T'/

*-----------------------------------------------------------------------*
* INPUT
*-----------------------------------------------------------------------*

        IRC=1

* Interactive Dialogue:

        Call PATOPI(
     *     NERR,NINP,
     *     NFPS,TDSM,MB1,MB2, 
     *     NWMX,WMX,IDM4,IBF1,LBF,SCMI,CO,   
     *     LW1,LW2,LW3,CO1,CO2,CO3,
     *     CBD,CSM,NCR1,LCR,IDM3,MGAP,XMIN,RMIN,SMP,FPRM,CNM,LICO,ICYM, 
     *     IRC) 

        If (IRC.NE.0) then 
           IRC=IRC+100
           Go to 110
           End if

        LM=  MB2 -MB1+1
        IBF2=IBF1+LBF-1
        NCR2=NCR1+LCR-1
        XCO =(1-(CO/100))*SCMI

* Input of DNA sequence matrix

* - Initialize work variables

        MI=0
        IST=1
        N1=1
        CTRC='D'
        CB='N'
        CC='U'
        RLN2=LOG(2.0)

        Write(NERR,*,Err=901) ' '
        
* - Extract sequence segment from nucleotide sequence database  

    1   Read(NFPS,'(A)',Err=901,End= 10) RC80
           If(RC80( 1: 2).EQ.'YZ') go to  10 
           If(RC80( 1: 2).NE.'FP') go to   1 

        IRC=1
        Read(RC80,'(30x,A33)',Err=  9) CHFP 

        Call GETFP
     *     (NPTR,NSEQ,NREF,CHFP,MB1,MB2,CTRC,CB,FDBE,LDBE,ITY,CSTR,
     *      IPOS,CSEQ,IDM2,IST,IRC)

        If(IRC.NE.0) then
           IRC=IRC+100
           Go to  9 
        End if

* - Characters -> numbers   

        Do   5 I1=1,LM
           If     (CSEQ(I1).EQ.'A') then 
              ISEQ(N1)=1
              IBC(1)=IBC(1)+1
           Else if(CSEQ(I1).EQ.'C') then 
              ISEQ(N1)=2
              IBC(2)=IBC(2)+1
           Else if(CSEQ(I1).EQ.'G') then 
              ISEQ(N1)=3
              IBC(3)=IBC(3)+1
           Else if(CSEQ(I1).EQ.'T') then 
              ISEQ(N1)=4
              IBC(4)=IBC(4)+1
           End if
           N1=N1+1
    5   Continue

        MI=MI+1

* - Print return code  
 
    9   Write(CIRC,'(I3)',Err=904) IRC
        Write(NERR,*,Err=901) RC80(6:62),' : RC=',CIRC
        IRC=0
        Go to   1 

   10   Continue

* Done

        Write(NERR,*,Err=901) ' '

* Open scratch and output files:

        Open(NDSM,Status='SCRATCH',Form='UNFORMATTED',Err=901)
        Open(NSCO,Status='SCRATCH',Form='UNFORMATTED',Err=901)

* Save DNA sequence matrix

        Do  11 I1=0,LM*MI-1,LM
           Write(NDSM,Err=903)(ISEQ(ii1),ii1=I1+1,I1+LM)
   11   Continue
        Rewind(NDSM,Err=904)

* Report input data:

        Write(NOUT,'(1H,''*** Input data:'')')
        Write(NOUT,*) ' '
        Write(NOUT,*) '  - DNA sequence matrix:'
        Write(NOUT,*) ' '
        Write(NOUT,*) '     - Title: ',TDSM
        Write(NOUT,1011,Err=905) MB1,MB2
 1011   Format('      - Range: from ',I5,' to ',I5)
        Write(NOUT,1012,Err=906) MI,LM      
 1012   Format('      - Dimension: ',I5,' segments of ',I5,
     *     ' nucleotides')
        Write(NOUT,*) ' '

        N1=IBC(1)+IBC(2)+IBC(3)+IBC(4)
        N2=MI*LM-N1

        Write(NOUT,1013,Err=907) N2
 1013   Format('      - Base composition: N = ',I6)
      
        Do 12 I1=1,4
           RBC(I1)=REAL(IBC(I1))/N1*100
           Write(NOUT,1014,Err=908) CBC(I1),IBC(I1),RBC(I1)     
 1014   Format('                          ',A,
     *     ' = ',I8,' (',F4.1,'%)')  
   12   Continue
  
        Write(NOUT,*) ' '
        Write(NOUT,*) '  - Initial weight matrix and cut-off value:'
        Write(NOUT,*) ' '
        Write(NOUT,'(''TI'')') 

        LP=IBF1
        Do 13 I1=1,LBF
           Write(NOUT,1015,Err= 13) LP,(WMX(I1,ii1),ii1=1,4)   
 1015   Format('WM   ',I4,4F8.2)   
           LP=LP+1
   13   Continue

        Write(NOUT,'(''XX'')')
        Write(NOUT,1016,Err=913) CO,XCO
 1016   Format('CO   ',F6.2,'% (cut-off value ',F6.2,')')
        Write(NOUT,'(''//'')')
        Write(NOUT,*) ' '

        Write(NOUT,*) '  - Optimization Parameters:'
        Write(NOUT,*) ' '

        Write(NOUT,*,Err=910)
     *    '     - Minimal window width:        ',LW1    
        Write(NOUT,*,Err=910)
     *    '     - Maximal window width:        ',LW2    
        Write(NOUT,*,Err=910)
     *    '     - Window width increment:      ',LW3    

        Write(NOUT,1017,Err=915) CO1
 1017   Format('      - Minimal cut-off percentage    ',F6.2,'%')
        Write(NOUT,1018,Err=915) CO2
 1018   Format('      - Maximal cut-off percentage    ',F6.2,'%')
        Write(NOUT,1019,Err=915) CO3
 1019   Format('      - Cut-off percentage increment: ',F6.2,'%')

        RC80='     - Search mode:                  '
        If(CBD.EQ.'U') RC80(40:80)='unidirectional'
        If(CBD.EQ.'B') RC80(40:80)='bidirectional'
        Write(NOUT,*,Err=915) RC80( 1:72) 

        RC80='     - Selection mode:               '
        If(CSM.EQ.'B') RC80(40:80)='best fit'
        If(CSM.EQ.'M') RC80(40:80)='multiple non-overlapping'
        If(CSM.EQ.'A') RC80(40:80)='all'
        Write(NOUT,*,Err=915) RC80( 1:72) 

        Write(NOUT,*,Err=916)
     *    '     - Context range:              ','[',NCR1,',',NCR2,']'
        Write(NOUT,*,Err=916)
     *    '     - Maximal gap length:          ',MGAP
        Write(NOUT,1020,Err= 14) XMIN
 1020   Format('      - Minimal chi-squared:          ',F6.2)
   14   Write(NOUT,1021,Err= 15) RMIN
 1021   Format('      - Minimal relative entropy:     ',F6.2)

   15   Write(NOUT,1022,Err=902) SMP
 1022   Format('      - Smoothing percentage:         ',F6.2,'%')
        Write(NOUT,1023,Err=902) FPRM 
 1023   Format('      - Maximal false-positive rate:  ',F6.2,'%')

        RC80='     - Normalization mode:           '
        If(CNM.EQ.'M') RC80(40:80)='mononucleotide'
        If(CNM.EQ.'D') RC80(40:80)='dinucleotide'
        Write(NOUT,*,Err=915) RC80( 1:72) 

                 RC80='     - Initial cut-off optimization:   No'
        If(LICO) RC80='     - Initial cut-off optimization:   Yes'
        Write(NOUT,*,Err=915) RC80( 1:72) 

        Write(NOUT,*,Err=916)
     *    '     - Maximal # iterations         ',ICYM

* Done

* Initialize a few more variables and parameters   

        ICY=1

        ICO=INT((CO -CO1)/CO3)-126
        NCO=INT((CO2-CO1)/CO3)-126 
        LW1=LW1-LBF
        LW2=LW2-LBF
	LFPR=.FALSE.

*-----------------------------------------------------------------------*
* STEP   I: Window optimization 
*-----------------------------------------------------------------------*

* Re-initialize cut-off value, window borders

   20   NB1P=0
        NB2P=0
        ICOP=0

* Progress report to terminal 

        Write(NERR,*) ' '
        Write(NERR,1024,Err=920) ICY
 1024   Format('*** Iteration number : ',I3)        
        Write(NOUT,*) ' '
        Write(NOUT,1025,Err=920) ICY
 1025   Format('*** Iteration number : ',I3)        
        Write(NERR,*) ' '
        Write(NERR,*) '   Parameter optimization:'

* Create score matrix (ISCO)

        Write(NERR,*,Err=901) '      Creating score matrix ...'

           LN=LM-LBF
        Do 25 I1=0,MI*LM-1,LM
           Do 24 I2=I1,I1+LN

                 X=0
                 J3=I2
              Do 21 I3=1,LBF
                 J3=J3+1
                 If(ISEQ(J3).EQ.0) then 
                    ISCO(I2+1)=-128
                    Go to 24
                    End if
                 X=X+WMX(I3,ISEQ(J3))
   21         Continue

              If(CBD.EQ.'U') go to 23

                 Y=0
                 J3=I2+LBF
              Do 22 I3=1,LBF
                 Y=Y+WMX(I3,5-ISEQ(J3))
                 J3=J3-1  
   22         Continue
                 X=MAX(X,Y)

   23         N1=INT((100-(X/SCMI*100)-CO1)/CO3)-126

              If(N1.LT.-126) then
                 ISCO(I2+1)=-127
                 Go to  24
                 End if
              If(N1.GE. NCO) then
                 ISCO(I2+1)=NCO
                 Go to  24
                 End if
              ISCO(I2+1)=N1
   24      Continue
   25   Continue

* Save score matrix 
        
        Write(NSCO,Err=921)(ISCO(ii1),ii1=1,LM*MI)
        Rewind(NSCO,Err=922)

* Optimize Window 

   30   Write(NERR,*,Err=901) '        Optimizing window ...'

           NW=0

* - Create initial signal occurrence matrix (ISS,ISFM)
          
           LN=LM-LBF+1
           Do 31 I2=1,LN
              ISS(I2)=0
              ISF(I2)=0
   31      Continue

        Do 33 I1=0,LM*MI-1,LM
              J2=0
           Do 32 I2=I1+1,I1+LN
              J2=J2+1
              If      (ISCO(I2).EQ.-128) then 
                 ISFM(I2)=-1
              Else if (ISCO(I2).GE. ICO) then
                 ISFM(I2)= 1
                 ISS(J2)=ISS(J2)+1
                 ISF(J2)=ISF(J2)+1
              Else
                 ISFM(I2)= 0
                 ISS(J2)=ISS(J2)+1
              End if
   32      Continue
   33   Continue      
                      
        MW=LBF

* - Smooth signal occurrence matrix by increasing window width 

              XMA=0
        Do 42 LW=LBF+LW1,LBF+LW2,LW3

           If(LW.EQ.MW) go to 38
              LN=LM-LW+1
                 N2=LW-MW
           Do 37 I1=0,LM*MI-1,LM
                 J2=0
              Do 36 I2=I1+1,I1+LN
                 J2=J2+1           
                 If(ISFM(I2).EQ.-1) go to 36
                 Do 34 I3=I2+1,I2+N2
                    If(ISFM(I3).EQ.-1) then
                       ISS(J2)=ISS(J2)-1
                       ISF(J2)=ISF(J2)-ISFM(I2)
                       ISFM(I2)=-1
                       Go to 36
                       End If  
   34            Continue
                 If(ISFM(I2).EQ. 1) go to 36
                 Do 35 I3=I2+1,I2+N2
                    If(ISFM(I3).EQ.1) then
                       ISF(J2)=ISF(J2)+1     
                       ISFM(I2)=1
                       Go to 36
                       End if
   35            Continue
   36         Continue
   37      Continue
           MW=LW


* - Find optimal window by analysing alternative series of non-overlapping
*   windows 

                 LN=LM-LW+1            
   38      Do 41 I1=1,LW-1

                 MS0=0
                 MF0=0
              Do 39 I2=I1,LN,LW 
                 MS0=MS0+ISS(I2)
                 MF0=MF0+ISF(I2)
   39         Continue
                 If(MS0.EQ.0.OR.MF0.EQ.0) go to 41
                 If(MS0.EQ.MF0)           go to 41

                 MS=MI
                 MF=0
                 XM=0
              Do 40 I2=I1,LN,LW
                 If(ISS(I2).EQ.0.OR.ISS(I2).EQ.MS0)  go to 40
                 F1=REAL(ISF(I2))/ISS(I2)
                 F2=(REAL(MF0)-ISF(I2))/(MS0-ISS(I2))
                 X=F1-F2
                 If(F2.NE.1.0.AND.F1.NE.0.0) then
C                   Y=100*(1-F1)*F2/(1-F2)/F1
                    Y=100*F2/F1
                 Else
                    Y=0
                 End if
                 If(LFPR) then
                    If(X.LE.XM.OR.Y.GT.FPRM) go to 40
                 Else if(X.GT.0.AND.Y.LE.FPRM) then
		    XMA=0
                    LFPR=.TRUE.
                 Else if(X.LE.XM) then
		    go to 40
                 End if

* - New inner loop maximum found 

                 MS=ISS(I2)
                 MF=ISF(I2)
                 XM=X
                 J2=I2

   40         Continue
              If(XM.LE.XMA) go to 41

* - New outer loop maximum found 

              XMA=XM
              NW=LW
              NB0=I1
              NB1=J2+MB1-1
              NS0=MS0
              NF0=MF0
              NS=MS
              NF=MF

   41      Continue
   42   Continue 

* Done 

        If(NW.EQ.0) then 
           Write(NERR,*) ' '
           Write(NERR,*,Err=901) 'Error: window optimization failed!'
	   Stop
        End if

* New window = old window ?
 
        NB2=NB1+NW-1
        If(NB1.EQ.NB1P.AND.NB2.EQ.NB2P.AND.ICO.EQ.ICOP) go to 50

        NB1P=NB1
        NB2P=NB2

* Report results of window optimization 

        Write(NOUT,*) ' '
        Write(NOUT,1041,Err=923) NB1,NB2
 1041   Format('        - New window: from ',I5,' to ',I5)
        X=REAL(NF)/NS*100 
        Write(NOUT,1042,Err=924) NF ,NS ,X 
 1042   Format('        - Occurrence Frequency:',I7,' (',I7,') / ',
     *     F6.2,'%') 
        MS0=NS0-NS
        MF0=NF0-NF
        X=REAL(MF0)/(MS0)*100        
        Write(NOUT,1043,Err=925) MF0,MS0,X 
 1043   Format('        - Background Frequency:',I7,' (',I7,') / ',
     *     F6.2,'%') 
        If(MF0.EQ.0) then
           X=0
        Else 
           X=MIN(999.99,REAL(NF)*MS0/NS/MF0)
        End If
        Y=MIN(99.99,100*(REAL(NF)/NS-Real(MF0)/MS0))
        Write(NOUT,1044,Err=926) X,Y
 1044   Format('        - Over-representation : ',F6.2,
     *     'x, ',F5.2,'%')

        If(.NOT.LFPR) Write(NOUT,'(/,
     *     ''      Warning: FPR-limitation could not be applied!'')') 

        
        If(ICY.EQ.1.AND..NOT.LICO) go to 50

        Write(NERR,*,Err=901) '        Optimizing cut-off value ...'

*-----------------------------------------------------------------------*
* STEP  II: Cut-off value optimization 
*-----------------------------------------------------------------------*
 
* Recover score matrix from scratch file 

        Read(NSCO,Err=927)(ISCO(ii1),ii1=1,LM*MI)
        Rewind(NSCO,Err=928)

* Smooth signal score matrix by increasing window size 

           If(NW.EQ.LBF) go to 46

              LN=LM-NW+1
                 N2=NW-LBF
           Do 45 I1=0,LM*MI-1,LM
              Do 44 I2=I1+NB0,I1+LN,NW
                 If(ISCO(I2).EQ.-128) go to 44
                 Do 43 I3=I2+1,I2+N2
                    If(ISCO(I3).EQ.-128) then
                       ISCO(I2)=-128
                       Go to 44
                       End If  
                    If(ISCO(I3).GT.ISCO(I2)) ISCO(I2)=ISCO(I3)
   43            Continue
   44         Continue
   45      Continue

* Find optimal cut-off value 

   46      XMA=0 
             J2=NB1-MB1+1
        Do 49 LCO=-126,NCO

              MF0=0
              MF=0
           Do 48 I1=0,LM*MI-1,LM
              Do 47 I2=I1+NB0,I1+LN,NW
                 If(ISCO(I2).GE.LCO) MF0=MF0+1
   47         Continue
              If(ISCO(I1+J2).GE.LCO) MF=MF+1
   48      Continue
           If(NS.EQ.0.OR.NS0-NS.EQ.0) go to 49
           F1=REAL(MF)/NS
           F2=(REAL(MF0)-MF)/(NS0-NS)
           X=F1-F2
           If(F2.NE.1.0.AND.F1.NE.0.0) then
C             Y=100*(1-F1)*F2/(1-F2)/F1
              Y=100*F2/F1
           Else
              Y=0
           End if
C          If(X.LE.XMA.OR.Y.GT.FPRM) go to 49
           If(LFPR) then
              If(X.LE.XMA.OR.Y.GT.FPRM) go to 49
           Else if(X.GT.0.AND.Y.LE.FPRM) then
              LFPR=.TRUE.
           Else if(X.LE.XMA) then
              Go to 49
           End if

* New maximum found 

           XMA=X
           ICO=LCO
           NF0=MF0
           NF=MF

   49   Continue

* New cut-off value = old cut-off value ?

        If(NB1.EQ.NB1P.AND.NB2.EQ.NB2P.AND.ICO.EQ.ICOP) go to 50

        ICOP=ICO

* Report results of cut-off value optimization:

        CO=CO1+(ICO+126)*CO3
        Write(NOUT,*) ' '
        Write(NOUT,1051,Err=929) CO
 1051   Format('        - New cut-off value   : ',F5.1,'%')
        X=REAL(NF)/NS*100 
        Write(NOUT,1052,Err=930) NF ,NS ,X 
 1052   Format('        - Occurrence Frequency:',I7,' (',I7,') / ',
     *     F6.2,'%') 
        MS0=NS0-NS
        MF0=NF0-NF
        X=REAL(MF0)/(MS0)*100        
        Write(NOUT,1053,Err=931) MF0,MS0,X 
 1053   Format('        - Background Frequency:',I7,' (',I7,') / ',
     *     F6.2,'%') 
        If(MF0.EQ.0) then
           X=0
        Else 
           X=MIN(999.99,REAL(NF)*MS0/NS/MF0)
        End if
        Y=MIN(99.99,100*(REAL(NF)/NS-Real(MF0)/MS0))
C       Y=MIN(99.99,XMA*100)
        Write(NOUT,1054,Err=932) X,Y
 1054   Format('        - Over-representation : ',F6.2,
     *     'x, ',F5.2,'%')

        If(.NOT.LFPR) Write(NOUT,'(/,
     *     ''      Warning: FPR-limitation could not be applied!'')') 

        Read(NSCO,Err=933)(ISCO(ii1),ii1=1,LM*MI)
        Rewind(NSCO,Err=934)

        Go to 30

*-----------------------------------------------------------------------*
* STEP III: Search for motif occurrences in optimized window / 
*           Generation of new motif+context base frequency table            
*-----------------------------------------------------------------------*

* Progress report to terminal 

   50   Write(NERR,*) '      Creating new base frequency table ...'
    
* Save previous base frequency table (IBFP)

        IBF1P=IBF1
        IBF2P=IBF2
        Do 52 I1=1,4
           IBCP(I1)=IBC(I1)
           Do 51 I2=1,LBF
              IBFP(I2,I1)=IBF(I2,I1)
   51      Continue
   52   Continue
         
* Search rows of DNA sequence matrix for motif occurrences
           
        XCO=(1-CO/100)*SCMI

           J1=0
           NI=0
        Do 70 I1=1,MI

           Read(NDSM,Err=935)(IROW(ii1),ii1=1,LM)

              J2=NB1-MB1
              CSTR='+' 
   56         NPO=0
              XPO(1)=SCMI
           Do 58 I2=J2,J2+NW-LBF
                 X=0
                 J3=0
              Do 57 I3=I2+1,I2+LBF
                 J3=J3+1
                 If(IROW(I3).EQ.0) go to 58                  
                 X=X+WMX(J3,IROW(I3))
                 If(X.LT.XCO) go to 58
   57         Continue
              
* - Record new motif 

              If(CSM.EQ.'B') then 
                 If(X.EQ.XPO(1)) NPO=0
                 If(X.GT.XPO(1)) then
                    NPO=1
                    IPO(1)=I2+1
                    XPO(1)=X
                    End if 
                 Go to 58
              End if

              NPO=NPO+1
              IPO(NPO)=I2+1
              XPO(NPO)=X

   58      Continue

           Do 66 I2=1,NPO 
              If(CSM.EQ.'M') then

* - Select non-overlapping motifs 

                    N2=IPO(I2)-LBF
                 Do 59 I3=I2-1,1,-1
                    If(IPO(I3).LE.N2) go to 60
                    If(XPO(I3).GE.XPO(I2)) go to 66
   59            Continue
   60               N2=IPO(I2)+LBF
                 Do 61 I3=I2+1,NPO
                    If(IPO(I3).GE.N2) go to 62
                    If(XPO(I3).GE.XPO(I2)) go to 66
   61            Continue
   62            End if
                    
* - Generate new row of motif+context DNA sequence matrix

              N1=IPO(I2)+NCR1-IBF1
              N2=IPO(I2)+NCR2-IBF1

* - If necessary, move zeroes to 5' portion  

                    J2=J1
              If(N1.LT.1) then 
                 Do 63 I3=J1+1,J1-N1+1
                    ISEQ(I3)=0
   63            Continue                     
                 J2=J2-N1+1
                 N1=1
                 End if

* - If necessary, move zeroes to 3' portion 

              If(N2.GT.LM) then 
                 Do 64 I3=J1+LCR-N2+LM+1,J1+LCR
                    ISEQ(I3)=0
   64            Continue
                 N2=LM
              End if

* - Transfer sequence segment 

              Do 65 I3=N1,N2
                 J2=J2+1                  
                 ISEQ(J2)=IROW(I3)
   65         Continue

              J1=J1+LCR
              If(J1+LCR.GT.IDM1) go to 936
              NI=NI+1

   66      Continue
   
           If(CBD.EQ.'U'.OR.CSTR.EQ.'-') go to 70

* - Generate complementary strand 
                
              J2=LM
           Do 67 I2=1,(LM+1)/2
              B=IROW(J2)
              IROW(J2)=IROW(I2)
              If(IROW(J2).NE.0) IROW(J2)=5-IROW(J2)    
              IROW(I2)=B
              If(IROW(I2).NE.0) IROW(I2)=5-IROW(I2)    
              J2=J2-1
   67      Continue

           J2=MB2-NB2
           CSTR='-'
           Go to 56
   
   70   Continue

* Done

        Rewind(NDSM,Err=937)

* Elimination of duplicates

        Write(NERR,*,Err=901) '      Elimination of duplicates ...'

              N1=0
        Do 73 I1=1,LCR*(NI-1),LCR
           If(ISEQ(I1).EQ.-1) go to 73
           Do 72 I2=I1+LCR,LCR*NI,LCR
           If(ISEQ(I2).EQ.-1) go to 72
                 J3=I2
              Do 71 I3=I1,I1+LCR-1
                 If(ISEQ(I3).NE.ISEQ(J3)) go to 72
                 J3=J3+1
   71         Continue
              ISEQ(I2)=-1
              N1=N1+1
   72      Continue
   73   Continue

* Report # of eliminated duplicates

        Write(NOUT,*) ' '
        Write(NOUT,1071,Err=938) N1
 1071   Format('     - Elimination of duplicates: ',I3)        
        Write(NOUT,*) ' '
        
        Write(NERR,*,Err=901) '      Defining new pattern ...'

* Generate context+motif base frequency table

        Do 75 I1=1,4
           IBC(I1)=0
           Do 74 I2=1,LCR
              ICR(I2,I1)=0
   74      Continue          
   75   Continue

        Do 77 I1=1,LCR*NI,LCR
           If(ISEQ(I1).EQ.-1) go to 77
              J2=0
           Do 76 I2=I1,I1+LCR-1
              J2=J2+1
              If(ISEQ(I2).EQ.0) go to 76
              IBC(   ISEQ(I2))=IBC(   ISEQ(I2))+1
              ICR(J2,ISEQ(I2))=ICR(J2,ISEQ(I2))+1
   76      Continue  
   77   Continue

*-----------------------------------------------------------------------*
* STEP  IV: Delineation of new motif from context area 
*-----------------------------------------------------------------------*

* Initialize context base composition and pattern borders:

        Do 78 I1=1,4
           IBC0(I1)=IBC(I1)-ICR(1-NCR1,I1)
   78   Continue

	If(MGAP.LT.0) then 
           LB1=IBF1
	   LB2=IBF2
	   Go to 93 
        End if

* Delineate motif borders 

        LB1=0    
        LB2=0 
        ICYP=0

   80   LB1P=LB1
        LB2P=LB2
        ICYP=ICYP+1
        
           N1=0
        Do 81 I1=1,4
           N1=N1+IBC0(I1)
   81   Continue

        Do 82 I1=1,4
           RBC(I1)=REAL(IBC0(I1))/N1
   82   Continue

* - Extend motif to the left side:

           N1=0
           J1=0
              J2=0
        Do 86 I1=-NCR1,MAX(1,-NCR1-15),-1
           J1=J1-1
              N2=0              
           Do 83 I2=1,4
              N2=N2+ICR(I1,I2)
   83      Continue

              X=0
           If(N2.GT.0) then
              Do 84 I2=1,4
                 Y=RBC(I2)*N2
                 X=X+(ICR(I1,I2)-Y)**2/Y
   84         Continue
           End if

              R=0
           If(N2.GT.0) then
              Do 85 I2=1,4
                 Z=Real(ICR(I1,I2))/N2
                 If(RBC(I2).NE.0.0.AND.Z.NE.0.0) then
                    R=R+Z*(LOG(Z)-LOG(RBC(I2)))
                 End if
   85         Continue
           End if
              R=R/RLN2

           If(X.GE.XMIN.AND.R.GE.RMIN) then
              N1=0
              J2=J1
           Else
             If(N1.EQ.MGAP) go to 87
             N1=N1+1
           End if
   86   Continue

   87   LB1=J2

* - Extend motif to the right side:

           N1=0
           J1=0
              J2=0
        Do 91 I1=-NCR1+2,MIN(LCR,-NCR1+16)
           J1=J1+1
              N2=0              
           Do 88 I2=1,4
              N2=N2+ICR(I1,I2)
   88      Continue
                 X=0
           If(N2.GT.0) then
              Do 89 I2=1,4
                 Y=RBC(I2)*N2
                 X=X+(ICR(I1,I2)-Y)**2/Y
   89         Continue
              End if

              R=0
           If(N2.GT.0) then
              Do 90 I2=1,4
                 Z=Real(ICR(I1,I2))/N2
                 If(RBC(I2).NE.0.0.AND.Z.NE.0.0) then
                    R=R+Z*(LOG(Z)-LOG(RBC(I2)))
                 End if
   90      Continue
           End if
              R=R/RLN2

           If(X.GE.XMIN.AND.R.GE.RMIN) then
              N1=0
              J2=J1
           Else
             If(N1.EQ.MGAP) go to 92
             N1=N1+1
           End if
   91   Continue

   92   LB2=J2

* New borders delineated 

   93   Continue

* Calculate new context base composition:

        If(LBF+IBF1-NCR1.GE.LCR.OR.IBF1-NCR1.LE.1) then
           Write(NERR,*) ' '
           Write(NERR,*,Err=901) "Error: context range too short!"
           Stop
        End if

        Do 95 I1=1,4
           IBC0(I1)=IBC(I1)
           Do 94 I2=1-NCR1+LB1,1-NCR1+LB2
              IBC0(I1)=IBC0(I1)-ICR(I2,I1)
   94      Continue
   95   Continue 

	If(MGAP.GE.0) then
           If(LB1.LT.LB1P.OR.LB2.GT.LB2P.AND.ICYP.LT.10) go to 80
        End if

* New motif define

        IBF1=LB1
        IBF2=LB2
        LBF=IBF2-IBF1+1

        Do 97 I1=1,4
           IBC(I1)=IBC0(I1)
              J2=IBF1-NCR1
           Do 96 I2=1,LBF
              J2=J2+1
              IBF(I2,I1)=ICR(J2,I1)
   96      Continue
   97   Continue

*-----------------------------------------------------------------------*
* STEP   V: Calculation of new weight matrix 
*-----------------------------------------------------------------------*

* New motif = old motif ?

        If(IBF1.NE.IBF1P.OR.IBF2.NE.IBF2P) go to 100 

        Do 99 I1=1,4
           If(IBC(I1).NE.IBCP(I1)) go to 100
           Do 98 I2=1,LBF
              If(IBF(I2,I1).NE.IBFP(I2,I1)) go to 100
   98      Continue
   99   Continue
        Go to 120

* Progress report to terminal 

  100   Write(NERR,*,Err=901) '      New motif defined.'

* Calculate new dinucleotide composition, if necessary 

        If(CNM.EQ.'D') then

           Do 102 I1=1,4
              Do 101 I2=1,4
                 IDC(I1,I2)=0
  101         Continue
  102      Continue          
       
           Do 104 I1=1,LCR*NI,LCR
              If(ISEQ(I1).EQ.-1) go to 104 
              Do 103 I2=I1,I1-NCR1+IBF1-2
                 If(ISEQ(I2).EQ.0.OR.ISEQ(I2+1).EQ.0) go to 103
                 IDC(ISEQ(I2),ISEQ(I2+1))=
     *           IDC(ISEQ(I2),ISEQ(I2+1))+1
  103         Continue
  104      Continue               

           Do 106 I1=1,LCR*NI,LCR
              If(ISEQ(I1).EQ.-1) go to 106 
              Do 105 I2=I1-NCR1+IBF2+1,I1+LCR-2
                 If(ISEQ(I2).EQ.0.OR.ISEQ(I2+1).EQ.0) go to 105
                 IDC(ISEQ(I2),ISEQ(I2+1))=
     *           IDC(ISEQ(I2),ISEQ(I2+1))+1
  105         Continue
  106      Continue               

        End if

* Caluclate weight matrix

        Call CAWMX(IBC,IDC,IBF,IDM4,LBF,SMP,CNM,WMX,SCMI)       

* Report new motif

        Write(NOUT,*) '       - Context base composition:'
        Write(NOUT,*) ' '
 
           N2=0
        Do 108 I1=1,4
           N2=N2+IBC(I1)
  108   Continue
        Do 109 I1=1,4
           RBC(I1)=Real(IBC(I1))/N2*100
           Write(NOUT,1109,Err=939) CBC(I1),IBC(I1),RBC(I1)     
 1109   Format('               ',A,' = ',I6,' (',F4.1,'%)')  
  109   Continue
  
        Write(NOUT,*) ' '
        Write(NOUT,*) '   - New motif:'
        Write(NOUT,*) ' '

        Write(NOUT,*)
     *    '                    weight matrix',
     *    '                base frequencies' 
        Write(NOUT,'(''TI'')')

        LP=IBF1
        Do I1=1,LBF
           Write(NOUT,1110,Err=940) LP,
     *        (WMX(I1,ii1),ii1=1,4),(IBF(I1,ii1),ii1=1,4)   
 1110   Format('WM    ',I4,4F8.2,'  ',4I6)   
           LP=LP+1
        End Do
  110   Continue
   
        Write(NOUT,'(''XX'')')
        Write(NOUT,1111,Err=941) CO,XCO
 1111   Format('CO   ',F6.2,'% (cut-off value ',F6.2,')')
        Write(NOUT,'(''//'')')
        Write(NOUT,*) ' '

* Recover DNA sequence matrix from scratch file

        Do 115 I1=0,LM*MI-1,LM
           Read(NDSM,Err=942)(ISEQ(ii1),ii1=I1+1,I1+LM)
  115   Continue
        Rewind(NDSM,Err=943)

* Start next cycle if not tired 
  
        ICY=ICY+1
	LFPR=.FALSE.
        If(ICY.LE.ICYM) go to  20

*-----------------------------------------------------------------------*
* THE END  
*-----------------------------------------------------------------------*

  120   If(IRC.NE.0) Write(NERR,1110) IRC
 1120   Format(' Error termination: RC=',I3,'.')
        If(IRC.EQ.0.AND.ICY.LE.ICYM) 
     *     Write(NOUT,*,Err=901) '*** Motif optimized.'

        Write(NOUT,*,Err=901) '*** End.'

        Write(NOUT,*)
        Write(NOUT,'(''TI'')')
        Write(NOUT,'(''XX'')')
        Write(NOUT,1121,Err=941) CO
 1121   Format('CO   ',F6.2,'%')

        Write(NOUT,'(''XX'')')

        LP=IBF1
        Do I1=1,LBF
           Write(NOUT,1122,Err=940) LP,
     *        (WMX(I1,ii1),ii1=1,4)
 1122   Format('WM    ',I4,4F8.2)   
           LP=LP+1
        End Do

        Write(NOUT,'(''//'')')
        Stop

* Error handling: 

  943   IRC=IRC+1
  942   IRC=IRC+1
  941   IRC=IRC+1
  940   IRC=IRC+1
  939   IRC=IRC+1
  938   IRC=IRC+1
  937   IRC=IRC+1
  936   IRC=IRC+1
  935   IRC=IRC+1
  934   IRC=IRC+1
  933   IRC=IRC+1
  932   IRC=IRC+1
  931   IRC=IRC+1
  930   IRC=IRC+1
  929   IRC=IRC+1
  928   IRC=IRC+1
  927   IRC=IRC+1
  926   IRC=IRC+1
  925   IRC=IRC+1
  924   IRC=IRC+1
  923   IRC=IRC+1
  922   IRC=IRC+1
  921   IRC=IRC+1
  920   IRC=IRC+1
  919   IRC=IRC+1
  918   IRC=IRC+1
  917   IRC=IRC+1
  916   IRC=IRC+1
  915   IRC=IRC+1
  914   IRC=IRC+1
  913   IRC=IRC+1
  912   IRC=IRC+1
  911   IRC=IRC+1
  910   IRC=IRC+1
  909   IRC=IRC+1
  908   IRC=IRC+1
  907   IRC=IRC+1
  906   IRC=IRC+1
  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 120

        End
*----------------------------------------------------------------------*
        Subroutine CAWMX(IBC,IDC,IBF,IDM4,LBF,SMP,CNM,WMX,SCMI)       

        Integer       IBC(4)
        Real          RBC(4)
        Integer       IDC(4,4)
        Real          XD1(4,4)
        Real          XD2(4,4)
        Character*01  CNM

        Integer       IBF(IDM4,4)
        Real          WMX(IDM4,4)

* Calculate relative base frequencies

        SMQ=SMP/100

           N=0
        Do  5 I1=1,4
           N=N+IBC(I1)            
    5   Continue

        Do 10 I1=1,4
           RBC(I1)=REAL(IBC(I1))/N
   10   Continue

        If(CNM.EQ.'D') go to  25

* Mononucleotide normalization

           SCMI=0
        Do  20 I1=1,LBF
              N=0
           Do  12 I2=1,4
              N=N+IBF(I1,I2)
   12      Continue
              X=0
           Do  15 I2=1,4
              Y=(IBF(I1,I2)+SMQ*RBC(I2)*N)/IBC(I2)
              If(Y.LE.X) go to  15  
              X=Y
   15      Continue
              X=LOG(X)
           Do  18 I2=1,4
              WMX(I1,I2)=LOG((IBF(I1,I2)+SMQ*RBC(I2)*N)/IBC(I2))-X  
   18      Continue
           SCMI=SCMI+MIN(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4))
   20   Continue
        Go to 100

* Calculation of 1st order Markov dependencies

   25   Do  27 I1=1,4
           Do  26 I2=1,4
              XD1(I1,I2)=0
              XD2(I1,I2)=0
   26      Continue
   27   Continue

        Do  29 I1=1,4
           Do  28 I2=1,4
              XD1(I1,4)=XD1(I1,4)+IDC(I1,I2)
              XD2(4,I1)=XD2(4,I1)+IDC(I2,I1)
   28      Continue
   29   Continue 
 
        Do  31 I1=1,4
           Do  30 I2=1,4
              XD1(I1,I2)=IDC(I1,I2)/XD1(I1,4)
              XD2(I2,I1)=IDC(I2,I1)/XD2(4,I1)
   30      Continue
   31   Continue

* Calculation of recognition matrix scores

* First Column

              N=0
              O=0
           Do  36 I2=1,4
              N=N+IBF(1,I2)
              O=O+IBF(2,I2)
   36      Continue

           Do  39 I2=1,4
                 Y=0
              Do  37 I3=1,4
                 Y=Y+IBF(2,I3)*XD2(I2,I3)            
   37         Continue
              WMX(1,I2)= LOG(IBF(1,I2)+SMQ*RBC(I2)*N)
     *                  -LOG(Y/O*N)
   39      Continue 

* Internal columns

        Do  45 I1=2,LBF-1 
              M=0                    
              N=0
              O=0
           Do  41 I2=1,4
              M=M+IBF(I1-1,I2)
              N=N+IBF(I1  ,I2)
              O=O+IBF(I1+1,I2)
   41      Continue

           Do  43 I2=1,4
                 X=0
                 Y=0
              Do  42 I3=1,4
                 X=X+IBF(I1-1,I3)*XD1(I3,I2)
                 Y=Y+IBF(I1+1,I3)*XD2(I2,I3)            
   42         Continue
              WMX(I1,I2)= LOG(IBF(I1,I2)+SMQ*RBC(I2)*N)
     *                   -LOG(X/M*N)*0.5
     *                   -LOG(Y/O*N)*0.5     
   43      Continue 
   45   Continue
                     
* Last Column

              M=0
              N=0
           Do  46 I2=1,4
              M=M+IBF(LBF-1,I2)
              N=N+IBF(LBF  ,I2)
   46      Continue

           Do  48 I2=1,4
                 X=0
              Do  47 I3=1,4
                 X=X+IBF(LBF-1,I3)*XD1(I3,I2)            
   47         Continue
              WMX(LBF,I2)= LOG(IBF(LBF,I2)+SMQ*RBC(I2)*N)
     *                    -LOG(X/M*N)
   48      Continue 
     *                            

* Subtract column maximum / calculate SCMI

        SCMI=0
        Do  50 I1=1,LBF
           SCMI=SCMI+MIN(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4))
           Y   =     MAX(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4))
           SCMI=SCMI-Y
           Do  49 I2=1,4
              WMX(I1,I2)=WMX(I1,I2)-Y
   49      Continue   
   50   Continue

  100   Return
        End
*----------------------------------------------------------------------*
*$      Subroutine PATOPI  : Created 06/08/90 ; last update 06/09/90   *
*       *--------------------------------------------------------------*
*       Author    : Philipp Bucher                                       
*       Function  : Interactive dialogue of program PATOP        
*       Arguments : NERR (R-) : I/O unit number for terminal output 
*                   NINP (R-) : I/O unit number for terminal input 
*                   NFPS (R-) : I/O unit number for FPS input file 
*                   TDSM (-W) : Title of FPS 
*                   MB1  (-W) : Left  (5'OH) border of sequence segments 
*                   MB2  (-W) : Right (3'OH) border of sequence segments 
*                   NWMX (R-) : I/O unit number for WMX input file  
*                   WMX  (-W) : Weight matrix 
*                   IDM1 (R-) : Dimension/maximal length of WMX
*                   IWM1 (-W) : Position of leftmost column with respect 
*                               to "center" of weight matrix 
*                   LWM  (-W) : Length of weight matrix                
*                   SCMI (-W) : Minimal score of weight matrix
*                   CO   (-W) : Cut-off percentage  
*                   LW1  (-W) : Minimal window width    
*                   LW2  (-W) : Maximal window width 
*                   LW3  (-W) : Window width increment 
*                   CO1  (-W) : Minimol cut-off percentage 
*                   CO2  (-W) : Maximal cut-off percentage 
*                   CO3  (-W) : Cut-off percentage increment 
*                   CBD  (-W) : Search mode  
*                   CSM  (-W) : Selection mode 
*                   NCR1 (-W) : Left  (5'OH) border of context range  
*                   NCR2 (-W) : Right (3'OH) border of context range 
*                   LCRM (R-) : Maximal length of context range 
*                   MGAP (-W) : Maximal gap length
*                   XMIN (-W) : Minimal chi-squared
*                   RMIN (-W) : Minimal relative entropy
*                   SMP  (-W) : Smoothing percentage
*                   FPRM (-W) : Maximal false-positive rate (%)
*                   CNM  (-W) : Normalization mode
*                   LICO (-W) : Initial cut-off optimization
*                   ICYM (-W) : Maximal number of iterations 
*                   IRC  (-W) : Return code
*       *--------------------------------------------------------------*

        Subroutine PATOPI(
     *     NERR,NINP,
     *     NFPS,TDSM,MB1,MB2, 
     *     NWMX,WMX,IDM1,IWM1,LWM,SCMI,CO,    
     *     LW1,LW2,LW3,CO1,CO2,CO3,
     *     CBD,CSM,NCR1,LCR,LCRM,MGAP,XMIN,RMIN,SMP,FPRM,CNM,LICO,ICYM, 
     *     IRC) 

  
        Character*192 RC80
        Character*192 FFPS
        Character*64  FWMX

        Character*55  TDSM 
 
        Character*55  TWMX
        Character*10  AWMX
        Character*02  CWMX

        Real          WMX(IDM1,4)

        Character*01  CBD
        Character*01  CSM
        Character*01  CNM

        Integer*8         MB1
        Integer*8         MB2

        Logical       LICO

        IRC=0

* Beginning of interactive dialogue

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901)
     *    'Program PATOP / Beginning of interactive dialogue:'
        Write(NERR,*,Err=901)
     *    'Press <Return> for default, enter < to go back to ',
     *    'previous question.'       
        Write(NERR,*,Err=901) ' '

* Open FPS file, read Title (TDSM)

   11   FFPS='Fps.fps'
        Write(NERR,1011,Err=901) FFPS( 1: 7)
 1011   Format
     *   (' FPS file (default=',A7,'): ',$)
        Read(NINP,'(A)',Err= 15) RC80
        If(RC80.EQ.' ') go to 12
        Read(RC80,'(A)',Err= 15) FFPS
   12   Open(NFPS,File=FFPS,Status='OLD',Err= 15)
        Go to  16
   15   Write(NERR,1199,Err=901)
        Go to  11

   16   TDSM=' '
   17   Read(NFPS,'(A)',Err=902,End=18) RC80
           If(RC80( 1: 2).EQ.'FP') go to  18
           If(RC80( 1: 2).EQ.'TI') go to  19
           Go to  17 
   18   RC80(16:70)='Unnamed functional position set' 

   19   Rewind(NFPS,Err=903)
        TDSM=RC80(16:70)

        Write(NERR,*,Err=901) ' '
        Write(NERR,*,Err=901) TDSM 
        Write(NERR,*,Err=901) ' '


* 5' border (MB1)

           
   21   MB1=-99
        Write(NERR,1021,Err=901) MB1
 1021   Format
     *   (' Left (5''OH) border (default=',I5,'): ',$)         
        Read(NINP,'(A)',Err= 29) RC80                               
        If(RC80.EQ.' ') go to 22                                  
        If(RC80.EQ.'<') go to 11
        Read(RC80,'(BN,I8)',Err= 29) MB1                                      

   22   Go to  31

   29   Write(NERR,1199,Err=901)
        Go to  21 

* 3' border (MB2)

   31   MB2=MB1+199
        Write(NERR,1031,Err=901) MB2
 1031   Format
     *   (' Right (3''OH) border (default =',I5,'): ',$)      
        Read(NINP,'(A)',Err= 39) RC80                               
        If(RC80.EQ.' ') go to  32
        If(RC80.EQ.'<') go to  21
        Read(RC80,'(BN,I8)',Err= 39) MB2                                      

   32   If(MB2.LE.MB1) go to  39                                        
        LM=MB2-MB1+1
        Go to  41 

   39   Write(NERR,1199,Err=901)
        Go to  31

* Weight matrix (AWMX,TWMX,CWMX,WMX,IWM1,LWM)

   41   FWMX='Wmx.wmx'
        Write(NERR,1041,Err=901) FWMX( 1: 7)
 1041   Format
     *   (' WMX file (default=',A7,' enter @ for terminal iput): ',$)
        Read(NINP,'(A)',Err= 49) RC80               
        If(RC80.EQ.' ') go to  42
        If(RC80.EQ.'<') go to  31

        If(RC80( 1: 1).EQ.'@') then
           NWMY=5
           Go to  45
        End if
        FWMX=RC80( 1:64) 

   42   NWMY=NWMX
        Open(NWMY,File=FWMX,Status='OLD',Err= 49)

   45   Call INWMX
     *    (NWMY,AWMX,TWMX,CWMX,WMX,IDM1,IWM1,LWM,SCMI,CO,XCO,IRC)
        If(IRC.EQ.0) go to  51       

 
   49   Write(NERR,1199,Err=901)
        Go to  41

* Cut-off % (CO)
 
   51   Write(NERR,1051,Err=901) CO
 1051   Format
     *   (' Initial cut-off % (default=',F4.1,'%): ',$)
        Read(NINP,'(A)',Err= 59) RC80               
        If(RC80.EQ.' ') go to  52
        If(RC80.EQ.'<') go to  41

        RC80(80:80)='@'
        Read(RC80,*,Err= 52) CO
   52   If(CO.LE.0.OR.CO.GT.100) go to  51
        Go to  61

   59   Write(NERR,1199,Err=901) 
        Go to  51

* Window optimization parameters (LW1,LW2,LW2)

   61   LW1=LWM
        LW2=LWM+25
        LW3=1
        Write(NERR,1061,Err=901) LW1,LW2,LW3
 1061   Format
     *   (' Window optimization parameters (mimimum, maximum,',/,
     *    ' increment, defaults =',I3,',',I5,',',I3,').',/,
     *    ' Enter three integers on one line: ',$) 
        Read(NINP,'(A)',Err= 69) RC80         
        If(RC80.EQ.' ') go to  62 
        If(RC80.EQ.'<') go to  51

        RC80(80:80)='@'
        Read(RC80,*,Err= 69) LW1,LW2,LW3 
       
   62   If(LW1.LT.LWM)     LW1=LWM
        If(LW2.LT.LWM)     LW2=LWM
        If(LW2.LT.LW1)     go to  69
        If(LW2.GT.LM/3)    go to  69
        If(LW3.LE.0)       go to  69
        Go to  71

   69   Write(NERR,1199,Err=901)
        Go to  61

* Cut-off % optimization parameters (CO1,CO2,CO3)
 
   71   CO1=REAL(INT(CO/2))
        CO2=100
        CO3=0.5
        Write(NERR,1071,Err=901) CO1,CO2,CO3
 1071   Format
     *   (' Cut-off % optimization parameters (minimum, maximum, ',/,
     *    ' increment, defaults =',F5.1,'%,',F6.1,'%,',F4.1,'%).',/,
     *    ' Enter three (integer or real) numbers: ',$)
        Read(NINP,'(A)',Err= 79) RC80         
        If(RC80.EQ.' ') go to  72
        If(RC80.EQ.'<') go to  61

        RC80(80:80)='@'
        Read(RC80,*,Err= 79) CO1,CO2,CO3 
       
   72   If(CO1.LT.0)   go to  79
        If(CO2.GT.100) go to  79
        If(CO2.LT.CO)  go to  79
        If(CO1.GE.CO2) go to  79
        NCO=(CO2-CO1)/CO3+1 
        If(NCO.GT.254) go to  79        
        Go to  81

   79   Write(NERR,1199,Err=901)
        Go to  71

* Search mode (CBD)
 
   81   CBD='U'
        Write(NERR,1081,Err=901) CBD
 1081   Format
     *   (' Search mode (B or U, default=',A1,'): ',$) 
        Read(NINP,'(A)',Err= 89) RC80               
        If(RC80.EQ.' ') go to  82
        If(RC80.EQ.'<') go to  71

        Read(RC80,'(A)',Err= 81) CBD
   82   If(CBD.EQ.'u') CBD='U'
        If(CBD.EQ.'b') CBD='B'
        If(CBD.EQ.'U'.OR.CBD.EQ.'B') go to  91 

   89   Write(NERR,1199,Err=901)
        Go to  81

* Selection mode 

   91   CSM='M'
        Write(NERR,1091,Err=901) CSM
 1091   Format
     *   (' Selection mode (B, M, or A, default=',A1,'): ',$)
        Read(NINP,'(A)',Err= 99) RC80               
        If(RC80.EQ.' ') go to  92
        If(RC80.EQ.'<') go to  81

        Read(RC80,'(A)',Err= 99) CSM
   92   If(CSM.EQ.'b') CSM='B' 
        If(CSM.EQ.'m') CSM='M' 
        If(CSM.EQ.'a') CSM='A' 
        If(CSM.EQ.'B'.OR.CSM.EQ.'M'.OR.CSM.EQ.'A') go to 101

   99   Write(NERR,1199,Err=901)
        Go to  91

* Context range (NCR1,NCR2)

  101   NCR1=MIN(IBF1,-50)
        NCR2=MAX(IBF2, 50)
        Write(NERR,1101,Err=901) NCR1,NCR2
 1101   Format
     *   (' Context range (default = [',I3,',',I3,']).',/,
     *    ' Enter two integers on one line: ',$) 
        Read(NINP,'(A)',Err=109) RC80               
        If(RC80.EQ.' ') go to 102
        If(RC80.EQ.'<') go to  91

        RC80(80:80)='@'
        Read(RC80,*,Err=109) NCR1,NCR2 

  102   If(NCR1.GT.IBF1) go to 109
        If(NCR2.LT.IBF2) go to 109
        LCR=NCR2-NCR1+1
        If(LCR.GT.LCRM)  go to 109 
        Go to 111

  109   Write(NERR,1199,Err=901)
        Go to 101

* Maximal gap length (MGAP)

  111   MGAP=3
        Write(NERR,1111,Err=901) MGAP
 1111   Format
     *   (' Maximal gap length (default=',I2,'): ',$)
        Read(NINP,'(A)',Err=119) RC80               
        If(RC80.EQ.' ') go to 112
        If(RC80.EQ.'<') go to 101 

        Read(RC80,'(BN,I8)') MGAP 
  112   If(MGAP.LT.-1) go to 119
        Go to 121

  119   Write(NERR,1199,Err=901)
        Go to 111
 
* Minimal chi-squared (XMIN)

  121   XMIN=10.0
        Write(NERR,1121,Err=901) XMIN
 1121   Format
     *   (' Minimum chi-squared (default=',F4.1,'): ',$)
        Read(NINP,'(A)',Err=124) RC80               
        If(RC80.EQ.' ') go to 122
        If(RC80.EQ.'<') go to 111 

        RC80(80:80)='@'
        Read(RC80,*,Err=124) XMIN 

  122   If(XMIN.LE.0) go to 124
        Go to 126 

  124   Write(NERR,1199,Err=901)
        Go to 121 

* Minimal relative entropy (RMIN)

  126   RMIN=0.1
        Write(NERR,1126,Err=901) RMIN
 1126   Format
     *   (' Minimum relative entropy (default=',F5.2,'): ',$)
        Read(NINP,'(A)',Err=129) RC80               
        If(RC80.EQ.' ') go to 127
        If(RC80.EQ.'<') go to 121 

        RC80(80:80)='@'
        Read(RC80,*,Err=129) RMIN 

  127   If(XMIN.LE.0) go to 129
        Go to 131

  129   Write(NERR,1199,Err=901)
        Go to 126 

* Smoothing % (SMP)

  131   SMP=10
        Write(NERR,1131,Err=901) SMP 
 1131   Format
     *   (' Smoothing % (default =',F4.1,'%): ',$)
        Read(NINP,'(A)',Err=134) RC80               
        If(RC80.EQ.' ') go to 132    
        If(RC80.EQ.'<') go to 121
       
        RC80(80:80)='@'
        Read(RC80,*,Err=134) SMP 

  132   If(SMP.LE.0.OR.SMP.GT.100) go to 134
        Go to 136

  134   Write(NERR,1199,Err=901)
        Go to 131 

* Maximal false-positive rate

  136   FPRM=10.0
        Write(NERR,1136,Err=901) FPRM
 1136   Format
     *   (' Maximal false-positive rate % (default =',F4.1,'%): ',$)
        Read(NINP,'(A)',Err=139) RC80               
        If(RC80.EQ.' ') go to 137    
        If(RC80.EQ.'<') go to 131
       
        RC80(80:80)='@'
        Read(RC80,*,Err=139) FPRM

  137   If(FPRM.LE.0.OR.FPRM.GT.100) go to 139
        Go to 141

  139   Write(NERR,1199,Err=901)
        Go to 136 

* Normalization mode 

  141   CNM='M'
        Write(NERR,1141,Err=901) CNM
 1141   Format
     *   (' Normalization mode (M or D, default=',A1,'): ',$)
        Read(NINP,'(A)',Err= 149) RC80               
        If(RC80.EQ.' ') go to 142
        If(RC80.EQ.'<') go to 136

        Read(RC80,'(A)') CNM

  142   If(CNM.EQ.'m') CNM='M'
        If(CNM.EQ.'d') CNM='D'
        If(CNM.EQ.'M'.OR.CNM.EQ.'D') go to  146 

  144   Write(NERR,1199,Err=901)
        Go to 141

* Inititial cut-off optimization (LICO) 

  146   LICO=.TRUE. 
        Write(NERR,1146,Err=901) 
 1146   Format
     *   (' Initial cut-off optimization (Y or N, default=Y): ',$)

        Read(NINP,'(A)',Err= 149) RC80               

        If(RC80.EQ.' ') go to 147
        If(RC80.EQ.'<') go to 131

  147   If(RC80(1:1).EQ.'Y'.OR.RC80(1:1).EQ.'y') LICO=.TRUE.
        If(RC80(1:1).EQ.'N'.OR.RC80(1:1).EQ.'n') LICO=.FALSE. 
        Go to 151

  149   Write(NERR,1199,Err=901)
        Go to 146

* Maximal number of iterations

  151   ICYM=25
        Write(NERR,1151,Err=901) ICYM
 1151   Format
     *   (' Maximal # cycles (default=',I3,'): ',$)
        Read(NINP,'(A)',Err=101) RC80               
        If(RC80.EQ.' ') go to 152
        If(RC80.EQ.'<') go to 141

        Read(RC80,'(BN,I8)') ICYM 

  152   If(ICYM.LT.1) go to 159
        Go to 161

  159   Write(NERR,1199,Err=901)
        Go to 151

  161   Continue

* End of interactive dialogue 

  200   Return 
      
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 200
        
 1199   Format('Input not accepted. Try again !')
 
        End
*----------------------------------------------------------------------*
        Subroutine INWMX
     *    (NWMX,AWMX,TWMX,CWMX,WMX,IDM1,IWM1,LWM,SCMI,CO,XCO,IRC)

        Parameter        (IDM2= 64)

        Character*80      RC80

        Character*55      TWMX
        Character*10      AWMX
        Character*02      CWMX

        Real              WMX(IDM1,4)
        Real              WMY(IDM2,4)
        Integer           IPOS(IDM2)

        Logical           PERC

* Initialize variables, define defaults

        IRC=0

        PERC=.true.
        CO=50

        AWMX='WMX'
        TWMX='Unnamed weight matrix'
        CWMX='WM'

* Read Input file (-> WMY)

           K1=1
    1   Read(NWMX,'(A)',Err=901,End= 10) RC80

* TI-line

        If     (RC80( 1: 2).EQ.'TI') then

            Read(RC80,'(5x,A10,A55,A2)',Err=902) AWMX,TWMX,CWMX
            If(AWMX.EQ.' ') AWMX='WMX'
            If(TWMX.EQ.' ') TWMX='Unnamed weight matrix'
            If(CWMX.EQ.' ') CWMX='WM'
            K1=1
* WM-line

        Else if(RC80( 1: 2).EQ.'WM') then

           If(K1.GT.IDM2) go to 903
           RC80(80:80)='@'
           Read(RC80( 3:80),*,Err=  2)
     *        IPOS(K1),(WMY(K1,ii1),ii1=1,4)
    2      Continue 
           K1=K1+1

* CO-line

        Else if(RC80( 1: 2).EQ.'CO') then 

           RC80(80:80)='@'
           IP=Index(RC80,'%')    
           If(IP.EQ.0) then 
              Read(RC80( 3:80),*,Err=  3) XCO
              PERC=.false.
           Else
              RC80(IP:IP)=' '
              Read(RC80( 3:80),*,Err=  3) CO
              PERC=.true.
           End if
    3      Continue 

* End of file

        Else if(RC80( 1: 2).EQ.'//'.OR.
     *          RC80( 1: 2).EQ.'YZ'.OR.
     *          RC80( 1: 1).EQ.'.' ) then
           Go to  10
        Else
           Go to   1 
        End if
           Go to   1

   10   If(K1.LE.1) go to 904 
        K1=K1-1

* Define range of weight matrix

           IWM1=IPOS(1) 
           IWM2=IPOS(1) 

        Do  19 I1=2,K1        
           If(IPOS(I1).LT.IWM1) IWM1=IPOS(I1)  
           If(IPOS(I1).GT.IWM2) IWM2=IPOS(I1)  
   19   Continue

        LWM=IWM2-IWM1+1
        If(LWM.GT.IDM1) go to 905

* re-initialize WMX

        Do  29 I1=1,K1
           Do  28 I2=1,4
              WMX(I1,I2)=0
   28      Continue
   29   Continue
 
* Move matrix WMY -> WMX  

        Do  39 I1=1,K1
           J1=IPOS(I1)-IWM1+1
           Do  38 I2=1,4
              WMX(J1,I2)=WMY(I1,I2)
   38      Continue
   39   Continue

* Normalize weight matrix

        SCMI=0
        Do  49 I1=1,LWM
           SCMI=SCMI+MIN(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4)) 
           Y=MAX(WMX(I1,1),WMX(I1,2),WMX(I1,3),WMX(I1,4)) 
           SCMI=SCMI-Y 
           Do  48 I2=1,4
              WMX(I1,I2)=WMX(I1,I2)-Y
   48      Continue
   49   Continue

* Calculate CO from XCO

        If(PERC) then
 
           If(CO.LT.0.OR.CO.GT.100) CO=50
           XCO=SCMI-(SCMI*CO/100)

* Calculate XCO from CO

        Else

           XCO=XCO-SCMA
           If(XCO.LT.SCMI.OR.XCO.GT.0) XCO=SCMI/2
           CO=100-(XCO/SCMI)*100

        End if 

* Done 

  100   Return

  905   IRC=IRC+1 
  904   IRC=IRC+1 
  903   IRC=IRC+1 
  902   IRC=IRC+1 
  901   IRC=IRC+1 
        Go to 100     
        
        End
*----------------------------------------------------------------------*
        Include       'GETFP.f'
