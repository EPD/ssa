C@      ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : GENERATION OF SIGNAL SEQUENCE COLLECTION             
C       ****************************************************************
                                                                        
        SUBROUTINE CESSC(NSSC,ND80,MSEQ,IDM1,CCTY,LS,LE,I,IRAN,IRC)       
                                                                        
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        CHARACTER    MSEQ(IDM1)                                         
        INTEGER      MIND(64)                                           
        REAL         RN
        REAL         RAN2
    
        IRC=0                                                           
        JD80=1                                                          
        If(IRAN.GE.0) IRAN=-1-IRAN 
        IF(CCTY.NE.'C') GO TO 10                                        
        I=4**LS                                                         
                                                                        
        DO 9 I1=1,I                                                     
           N1=I                                                         
           N2=I1-1                                                      
           DO 6 I2=1,LS                                                 
              N1=N1/4                                                   
              N3=N2/N1                                                  
              N4=N3+1                                                   
              GO TO (1,2,3,4) N4                                        
    1         MSEQ(I2)='A'                                              
              GO TO 5                                                   
    2         MSEQ(I2)='C'                                              
              GO TO 5                                                   
    3         MSEQ(I2)='G'                                              
              GO TO 5                                                   
    4         MSEQ(I2)='T'                                              
    5         N2=N2-(N3*N1)                                             
    6      CONTINUE                                                     
           WRITE(ND80,REC=JD80,ERR=901)(MSEQ(II1),II1=1,LS)             
           JD80=JD80+1                                                  
                                                                        
    9   CONTINUE                                                        
        GO TO 100                                                       
                                                                        
   10   IF(CCTY.NE.'R') GO TO 20                                        
        DO 19  I1=1,I                                                   
                                                                        
           DO 18 I2=1,LS                                                
              RN=RAN2(IRAN)
              NB=INT(RN*4)+1                                         
              GO TO (11,12,13,14) NB                                    
   11         MSEQ(I2)='A'                                              
              GO TO 18                                                  
   12         MSEQ(I2)='C'                                              
              GO TO 18                                                  
   13         MSEQ(I2)='G'                                              
              GO TO 18                                                  
   14         MSEQ(I2)='T'                                              
   18      CONTINUE                                                     
           WRITE(ND80,REC=JD80,ERR=902)(MSEQ(II1),II1=1,LS)             
           JD80=JD80+1                                                  
   19   CONTINUE                                                        
        Go to 100
                                                                        
   20   READ(NSSC,4000,END=903,ERR=904) RC80                            
           IF(RC80(1:2).NE.'C ') GO TO 20                               
        READ(RC80,4002,ERR=905) FISSC,LS,LE                             
                                                                        
   25   READ(NSSC,4000,END=40,ERR=906) RC80                             
           IF(RC80(1:2).EQ.'J ') GO TO 40                               
           IF(RC80(1:2).NE.'I ') GO TO 25                               
        READ(RC80,4001,END=100,ERR=907)(MSEQ(II1),II1=1,LE)             
           J1=0                                                         
           K1=0                                                         
        DO 26 I1=1,LE                                                   
           IF(MSEQ(I1).NE.'N') K1=K1+1                                  
           IF(MSEQ(I1).NE.'X') GO TO 26                                 
           J1=J1+1                                                      
           MIND(J1)=I1                                                  
   26   CONTINUE                                                        
                                                                        
        IF(K1.NE.LS) GO TO 908                                          
        IF(J1.NE.0)  GO TO  30                                          
        WRITE(ND80,REC=JD80,ERR=909)(MSEQ(II1),II1=1,LE)                
        JD80=JD80+1                                                     
        GO TO 25                                                        
                                                                        
   30      K=J1                                                         
           J=4**J1                                                      
        DO 38 I1=1,J                                                    
                                                                        
              N1=J                                                      
              N2=I1-1                                                   
           DO 37 I2=1,K                                                 
              J1=MIND(I2)                                               
              N1=N1/4                                                   
              N3=N2/N1                                                  
              N4=N3+1                                                   
              GO TO (31,32,33,34) N4                                    
   31         MSEQ(J1)='A'                                              
              GO TO 35                                                  
   32         MSEQ(J1)='C'                                              
              GO TO 35                                                  
   33         MSEQ(J1)='G'                                              
              GO TO 35                                                  
   34         MSEQ(J1)='T'                                              
   35         N2=N2-(N3*N1)                                             
   37      CONTINUE                                                     
                                                                        
           WRITE(ND80,REC=JD80,ERR=910)(MSEQ(II1),II1=1,LE)             
           JD80=JD80+1                                                  
   38   CONTINUE                                                        
                                                                        
        GO TO 25                                                        
                                                                        
   40   I=JD80-1                                                        
        BACKSPACE(NSSC,ERR=911)                                         
                                                                        
  100   RETURN                                                          
                                                                        
  911   IRC=IRC+1                                                       
  910   IRC=IRC+1                                                       
  909   IRC=IRC+1                                                       
  908   IRC=IRC+1                                                       
  907   IRC=IRC+1                                                       
  906   IRC=IRC+1                                                       
  905   IRC=IRC+1                                                       
  904   IRC=IRC+1                                                       
  903   IRC=IRC+1                                                       
  902   IRC=IRC+1                                                       
  901   IRC=IRC+1                                                       
                                                                        
        GO TO 100                                                       
                                                                        
 4000   FORMAT(A80)                                                     
 4001   FORMAT(2X,78A)                                                  
 4002   FORMAT(2X,A7,4X,I3,1X,I3)                                       
                                                                        
        END                                                             
