        Program       FINDM 

        Parameter     (IDM1=201326592)
        Parameter     (IDM2=524288)
        Parameter     (IDM3=64)

        Parameter     (NERR= 0)
        parameter     (NINP= 5)
        Parameter     (NFPS=11)
        Parameter     (NWMX=12)
        Parameter     (NPTR=13)
        Parameter     (NDBF=14)
        Parameter     (NSEQ=15)
        Parameter     (NREF=16)

        Parameter     (NLIS= 6)

        Byte          ISEQ(IDM1)
        Integer       IPO(IDM2)
        Real          XPO(IDM2)        
        Real          RCM(IDM3,4)

        Character     CSEQ(IDM1)
     
        Byte          B

        Integer*8     LM
        Integer*8     LDBE
        Integer*8     IPOS
	Integer*8     MB1
        Integer*8     MB2


        Character*80  RC80
        Character*80  RCEX
        Character*22  FDBE
        Character*03  CIRC

        Character*33  CHFP

        Character*55  TFPS
        Character*10  AFPS
        Character*02  CFPS

        Character*01  CSM
        Character*01  CSL
        Character*01  CTRC
        Character*01  CSTR
        Character*01  CSTS
        Character*01  CC
        Character*01  CB

        IRC=0
	INUM=0

* Read input parameters 
      
        Call FINDMI(
     *     NERR,NINP,
     *     NFPS,TFPS,AFPS,CFPS,MB1,MB2, 
     *     NWMX,RCM,IDM3,LB1,LPAT,SCMI,CO,    
     *     CSM,CSL, 
     *     IRC) 

* Major loop: process sequence segments one by one

        LM=MB2-MB1+1
        CTRC='D'
        CB='N'
        CC='U'
        MI=0
        IST=1
        XCO=(100-CO)/100*SCMI
        LSP=LPAT

* Write title line

        RCEX( 1:72) = 'TI   ' // AFPS // TFPS // CFPS
        Write(NLIS,'(A)',Err=906) RCEX( 1:72)
	Write(NLIS,'(''XX'')',Err=906)
       
* Sequential processing of FP-lines       

   20   Read(NFPS,"(A)",Err=907,End= 90) RC80
           If(RC80( 1: 2).EQ.'YZ') go to  90
           If(RC80( 1: 2).NE.'FP') go to  20

* Retrieve segment from database   

        IRC=1
        Read(RC80,'(30x,A33)',Err=  30) CHFP

           IRC=0
        Call GETFP
     *     (NPTR,NSEQ,NREF,CHFP,MB1,MB2,CTRC,CB,FDBE,LDBE,ITY,CSTR,
     *      IPOS,CSEQ,IDM2,IST,IRC)

   30   If(IRC.NE.0) go to  89

* Conversion into numbers   

        Do  40 I1=1,LM
              ISEQ(I1)=0
           IF(CSEQ(I1).EQ.'A') ISEQ(I1)=1
           IF(CSEQ(I1).EQ.'C') ISEQ(I1)=2
           IF(CSEQ(I1).EQ.'G') ISEQ(I1)=3
           IF(CSEQ(I1).EQ.'T') ISEQ(I1)=4
   40      Continue

        CSTS='+' 
   41   NPO=0
        XPO(1)=XCO-1
        Do 43 I1=0,LM-LPAT
              X=0
              J2=0
           Do 42 I2=I1+1,I1+LPAT
              J2=J2+1
              If(ISEQ(I2).EQ.0) go to 43                  
              X=X+RCM(J2,INT(ISEQ(I2)))
              If(X.LT.XCO) go to 43
   42      Continue
              
           If(CSL.EQ.'B') then 
              If(X.EQ.XPO(1)) NPO=0
              If(X.GT.XPO(1)) then
                 NPO=1
                 IPO(1)=I1+1
                 XPO(1)=X
                 End if 
              Go to 43
              End if

              NPO=NPO+1
              IPO(NPO)=I1+1
              XPO(NPO)=X

   43   Continue

* First deal with selection modes P and N: positive or negative
* selection of input FP records

	If     (CSL.EQ.'P') then 
	   If  (NPO.GT.0) then 
              Write(NLIS,'(A)',Err=909) RC80(1:Lblnk(RC80))
	      Go to  60
           Else
	      Go to  50
           End if
	Else if(CSL.EQ.'N') then 
	   If  (CSM.EQ.'U'.OR.CSTS.EQ.'-') then
              If(NPO.EQ.0)
     *           Write(NLIS,'(A)',Err=909) RC80(1:Lblnk(RC80))
	      Go to  60
           Else if(NPO.GT.0) then
	      Go to  60
	   Else
	      Go to  50
           End if
        End if

* Now deal with the site selection modes B, M, and A

        Do 48 I1=1,NPO 
           If(CSL.EQ.'M') then

* Select non-overlapping pattern:

                 N1=IPO(I1)-LSP
              Do 44 I2=I1-1,1,-1
                 If(IPO(I2).LE.N1) go to 45
                 If(XPO(I2).GE.XPO(I1)) go to 48
   44         Continue
   45            N1=IPO(I1)+LSP
              Do 46 I2=I1+1,NPO
                 If(IPO(I2).GE.N1) go to 47
                 If(XPO(I2).GE.XPO(I1)) go to 48
   46         Continue
   47      End if

           If(CSTR.EQ.'+') IPOT=IPOS-1+IPO(I1)-LB1                          
           If(CSTR.EQ.'-') IPOT=IPOS+LM-IPO(I1)+LB1
           If(ITY.EQ.0) then
              IPOT=IPOT-(IPOT/LDBE)*LDBE
              If(IPOT.LE.0) IPOT=IPOT+LDBE
              end if 

	   INUM = INUM + 1
           If(XPO(I1).GE.1000.OR.XPO(I1).LE.-100) then
              Write(RCEX( 1:80),4002,Err=908) FDBE,ITY,CSTR,IPOT,
     *        INUM,XPO(I1)
           Else
              Write(RCEX( 1:80),4001,Err=908) FDBE,ITY,CSTR,IPOT,
     *        INUM,XPO(I1)
           End if
                 RCEX( 1:30)=RC80( 1:30)
           Write(NLIS,'(A)',Err=909) RCEX

   48   Continue
   
   50   If(CSM.EQ.'U'.OR.CSTS.EQ.'-') go to 60

* Generate complementary strand:
                
           J1=LM
        Do 51 I1=1,(LM+1)/2
           B=ISEQ(J1)
           ISEQ(J1)=ISEQ(I1)
           If(ISEQ(J1).NE.0) ISEQ(J1)=5-ISEQ(J1)    
           ISEQ(I1)=B
           If(ISEQ(I1).NE.0) ISEQ(I1)=5-ISEQ(I1)   
           J1=J1-1
   51      Continue

        If(CSTR.EQ.'+') then 
           CSTR='-'
           go to 52
           end if
        If(CSTR.EQ.'-') CSTR='+'

   52   CSTS='-'
        Go to 41

   60   Continue 

   89   Write(CIRC,'(I3)',Err=910) IRC
        Write(NERR,*) RC80(6:62),' : RC=',CIRC
        IRC=0
        Go to  20

  801   IRC=IRC+1
        Go to  89

   90   Continue

  100   If(IRC.NE.0) Write(NERR,1100) IRC
 1100   Format('Error termination: RC=',I3,'.')

        Stop

* Error handling: 

  910   IRC=IRC+1
  909   IRC=IRC+1
  908   IRC=IRC+1
  907   IRC=IRC+1
  906   IRC=IRC+1
  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 100

 4001   Format(30X,A21,I1,A1,I10,I7,F8.4)
* 4001   Format(30X,A21,I1,A1,I10,I7,' ',E7.1)
 4002   Format(30X,A21,I1,A1,I10,I7,F8.1)

        End

        Include 'GETFP.f'
	Include 'FINDMI.f'
	Include 'PSQID.f'
	Include 'LOCSQ.f'
