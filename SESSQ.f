C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : SIGNAL SEARCH OF DNA SEQUENCE MATRIX                 
C       ****************************************************************
                                                                        
        SUBROUTINE SESSQ(IDM1,IDSM,ISF1,ISF2,IDM3,MSEQ,MI,LM,NI,LS,LE   
     A,LC,LD,LF,LH,IOPT,IRC)                                            
                                                                        
        CHARACTER IDSM(IDM1)                                            
        INTEGER   ISF1(IDM1)                                            
        INTEGER   ISF2(IDM1)                                            
        CHARACTER MSEQ(IDM3)                                            
        INTEGER   MIND(64)                                              
                                                       
	Integer*8  LM
                                                                        
        IRC=0                                                           
        LN=LM-LE+1                                                      
        LO=1+(NI-1)*LD                                                  
        LJ=LC-LE                                                        
        K=LS-LH                                                         
                                                                        
           J1=0                                                         
        DO 10 I1=1,LE                                                   
           IF(MSEQ(I1).EQ.'N') GO TO 10                                 
           J1=J1+1                                                      
           MIND(J1)=I1                                                  
   10   CONTINUE                                                        
        IF(J1.NE.LS) GO TO 901                                          
                                                                        
        DO 90 I1=0,(MI-1)*LM,LM                                         
           DO 40 I2=I1+1,I1+LN,LF                                       
              ISF1(I2)=0                                                
              J=0                                                       
              DO 25 I3=1,LS                                             
                 J3=MIND(I3)                                            
                 IF(MSEQ(J3).EQ.'N') GO TO 25                           
                 IF(MSEQ(J3).EQ.'Y')GO TO 18                            
                 IF(MSEQ(J3).EQ.'R') GO TO 19                           
                 IF(MSEQ(J3).EQ.'K')GO TO 20                            
                 IF(MSEQ(J3).EQ.'M') GO TO 21                           
                 IF(IDSM(I2+J3-1).EQ.MSEQ(J3)) GO TO 25                 
                 IF(J.GE.K) GO TO 40                                    
                 J=J+1                                                  
                 GO TO 25                                               
   18            IF(IDSM(I2+J3-1).EQ.'C'.OR.IDSM(I2+J3-1).EQ.'T')       
     A           GO TO 25                                               
                 IF(J.GE.K) GO TO 40                                    
                 J=J+1                                                  
                 GO TO 25                                               
   19            IF(IDSM(I2+J3-1).EQ.'A'.OR.IDSM(I2+J3-1).EQ.'G')       
     A           GO TO 25                                               
                 IF(J.GE.K) GO TO 40                                    
                 J=J+1                                                  
                 GO TO 25                                               
   20            IF(IDSM(I2+J3-1).EQ.'G'.OR.IDSM(I2+J3-1).EQ.'T')       
     A           GO TO 25                                               
                 IF(J.GE.K) GO TO 40                                    
                 J=J+1                                                  
                 GO TO 25                                               
   21            IF(IDSM(I2+J3-1).EQ.'A'.OR.IDSM(I2+J3-1).EQ.'C')       
     A           GO TO 25                                               
                 IF(J.GE.K) GO TO 40                                    
                 J=J+1                                                  
   25         CONTINUE                                                  
              ISF1(I2)=1                                                
   40      CONTINUE                                                     
           IF(IOPT.EQ.0) GO TO 100                                      
                                                                        
           DO 80 I2=I1+1,I1+LO,LD                                       
              IF(ISF2(I2).EQ.-1) GO TO 79                               
              DO 60 I3=I2,I2+LJ,LF                                      
                 IF(ISF1(I3).NE.1) GO TO 60                             
                 ISF1(I2)=1                                             
                 GO TO 80                                               
   60         CONTINUE                                                  
   79         ISF1(I2)=0                                                
   80      CONTINUE                                                     
                                                                        
   90   CONTINUE                                                        
                                                                        
  100   RETURN                                                          
                                                                        
  901   IRC=IRC+1                                                       
        GO TO 100                                                       
                                                                        
        END                                                             
