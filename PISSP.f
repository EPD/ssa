C       **************************************************************  
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : PRINTOUT OF SIGNAL SEARCH PARAMETERS                 
C       **************************************************************  
                                                                        
        SUBROUTINE PISSP(FFPS,TDSM,MB1,MB2,CCTY,FISSC,LS,LE,I,RN,LC      
     A,LD,LF,LH,PR,IDEV,IRC)                                            
                                                                        
        IMPLICIT CHARACTER*1  (C) , CHARACTER*7  (F) , CHARACTER*80 (R) 
                                                                        
        CHARACTER*55 TDSM                                                
        Character*80 FFPS
        Character*64 FFPT
        Real RN

        N1=Lnblnk(FFPS)
        FFPT=' '
        FFPT(64-N1:64)=FFPS( 1:N1)
        N1=Lnblnk(FISSC)
        FISSD=' '
        FISSD( 8-N1:7)=FISSC( 1:N1)

                        
        WRITE(IDEV,1000,ERR=901) FFPT,TDSM,MB1,MB2,CCTY,FISSD,LS,LE,I   
     A,RN,LC,LD,LF,LH,PR                                                
 1000   FORMAT(/,'Input data of signal search analysis :',//,            
     A  '   (1)   FPS file                '         ,A64,/,           
     A  '         ',1A55,/,                                       
     A  '   (2)   Left  (5''OH) border                ',I5,/,            
     A  '   (")   Right (3''0H) border                ',I5,/,            
     A  '   (3)   Type of signal seq. collection         ',A1,/,        
     A  '         Name of signal seq. collection   ',A7,/,            
     A  '   (4)   Coding level                       ',I5,/,            
     A  '         Signal sequence length             ',I5,/,            
     A  '   (5)   Size of signal seq. collection     ',I5,/,            
     A  '   (")   Random number generator       ',F10.4,/,            
     A  '   (6)   Window width                       ',I5,/,            
     A  '   (7)   Window shift                       ',I5,/,            
     A  '   (8)   Signal shift                       ',I5,/,            
     A  '   (9)   Min. # of matches                  ',I5,/,           
     A  '         Occurrence probability        ',F10.4,/)           
                                                                        
    1     RETURN                                                       
  901     IRC=1                                                       
          GO TO 1                                                    
          END                                                       
