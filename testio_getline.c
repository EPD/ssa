#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main (){

char errmsg[132];
char *tmpbuff = NULL;

size_t buflen = 64;
tmpbuff = (char *) calloc((buflen + 2),sizeof(char));

if (tmpbuff == NULL) {
     sprintf(errmsg,"cgetline: error allocating memory (len=%u)\n",(unsigned int)(buflen+2));
     perror(errmsg);
     return -1;
}

printf ("memory (len=%u) allocated at address 0x%x\n", (unsigned int)(buflen+2), (unsigned int) tmpbuff);

return 0;
}
