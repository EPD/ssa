* Program EPDtoFA.f 
*
* This program extracts promoter sequences from the EMBL 
* data library and generates and FASTA database file. 
*
* Compile: f77 EPDtoFA.f -o EPDtoFA
* Run:     EPDtoFA -l-499 -r100 < epd.dat > epd.seq
*
* Written by Philipp Bucher, December 1991 (modified ?)
* 

        Parameter        (NSEQ=11)
        Parameter        (NFPS=12)
        Parameter        (NSCR=13)
        Parameter        (NPTR=14)
        Parameter        (NREF=15)
        Parameter        (NERR= 0)

        Parameter        (IDM1=1024000)
        Character        CSEQ(IDM1)

        Character*80      ARG
        Character*03      CIRC 
        Character*80      RCIN 
        Character*80      RCEX

        Character*80      FDBE
        Character*01      CSTR
        Character*01      CTRC
        Character*01      CB
        Character*01      CC
        Character*20      CDES
        Character*01      CISS
        Character*05      CENT

	Integer*8         IPOS
	Integer*8         LM
	Integer*8         LDBE
	Integer*8         MB1
	Integer*8         MB2
					 
* Initialiations 

        IRC=0
        CB   = 'N'
        CTRC = 'D'
        IST  =  1
	LDBE =  0
       
* Input parameter defaults: 

        MB1  = -499
        MB2  =  100

        N1=Iargc()
        Do   5 I1=1,N1
           Call GetArg(I1,ARG)
           If(ARG(1:2).EQ.'-l') Read(ARG(3:80),*) MB1
           If(ARG(1:2).EQ.'-r') Read(ARG(3:80),*) MB2
    5   Continue

        LM=MB2-MB1+1

* Open files 

* Extract sequence segments from nucleotide sequence database 
 
   25      Read(5,"(A)",Err=902,End= 99) RCIN
              If(RCIN( 1: 2).EQ.'YZ') go to  99 
              If(RCIN( 1: 2).NE.'FP') go to  25
              IRC=1
           Read(RCIN,"(5x,A20,1x,A1,3x,A21,I1,A1,I10,2x,A5)",Err= 50)
     *        CDES,CISS,FDBE,ITY,CSTR,IPOS,CENT
              IRC=0

           If(CSTR.EQ.'+') IPOS=IPOS+MB1           
           If(CSTR.EQ.'-') IPOS=IPOS-MB2

C          Call
C    *        RETSQ(NSEQ,FDBE,LDBE,ITY,IPOS,LM,CSEQ,IDM1,IST,IRC)
        Call RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LDBE,ITY,IPOS,LM,CSEQ,IDM1,IST,CC,IRC)
           If(IRC.NE.0) then
              IRC=IRC+200
              go to  50
           End if

* Upper case or lower case ?

           Do  27 I1=IST,IST+LM-1
              B=Ichar(CSEQ(I1))
              If     (B.GE.65.AND.B.LE. 90) then
                 CC='U'
                 Go to  28
              Else if(B.GE.97.AND.B.LE.122) then 
                 CC='L'
                 Go to  28
              End if
   27      Continue
   28      Continue
                      
           Call STREV(CSEQ,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  
           If(IRC.NE.0) then
              IRC=IRC+300
              Go to  50
           End if

* Write EPD sequence entry>

           Write(RCEX( 1:80),
     *        "('>EP',A5,' (',A1,') ',A20)")
     *        CENT,CISS,CDES
           Call SBLNK(RCEX)
           IC=Lnblnk(RCEX)+1 
           Write(RCEX(IC:80),
     *        "('; range',I6,' to ',I5,'.')")
     *        MB1,MB2   
           Call SBLNK(RCEX)
           Write(6,'(132A)')(RCEX(ii1:ii1),ii1=1,Lblnk(RCEX))
 
           Write(6,"((60A))")(CSEQ(ii1),ii1=1,LM)

   50      Write(CIRC,"(I3)",Err=903) IRC
           Write(NERR,*,Err=904) RCIN( 6:70),' : RC=',CIRC
           
* Next entry

           Go to  25

* Done
 
   99   Continue
  100   Stop

* Error handling

  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
  900   Write(NERR,"('Error termination, RC=',I3,'.')") IRC 
        Go to 100

        End 

*----------------------------------------------------------------------*
C       Include           'RETSQ.f' 
        Include           'RETSQ.f' 
        Include           'STREV.f' 
        Include           'SBLNK.f' 
        Include           'PSQID.f' 
        Include           'LOCSQ.f' 
        Include           'rindex.f' 
        Include           'lblnk.f' 
