        Program FIND

* Sample input: *
*               *
*   EM:OCBGLOB  * 
*   6 'TATAAA'  *
*   6 'GGGCGG'  *
*               *
*---------------*

        Implicit       Character*01   (C)
        Implicit       Character*16   (F)
        Implicit       Character*80   (R)

        Parameter      (NSEQ=11)
        Parameter      (NPTR=12)
        Parameter      (NREF=13)

        Parameter      (IDM1=262144)

        Character      CSQ1(IDM1)
        Character      CSQ2(IDM1)

        Integer        ISEG(64)  
        Character*64   CSEG(64) 

        Character      COCC(256)
        Integer        IOCC(256) 
	Character*64   FDBE

        Integer*8         IPOS
        Integer*8         LDBE
        Integer*8         LM

        IRC=0

           ITY=1
           IPOS=1
           IST=1 
           CSTR='+'
           LM=0
           LDBE=0

* Read seq, segments from std
      
           Read(5,"(A)") FDBE
           KSEG=0
    1      Read(5,"(A)",End=10) RC80
           KSEG=KSEG+1
           Read(RC80,*) ISEG(KSEG),CSEG(KSEG)
           Go to   1 

C  10      Call
C    *        RETSQ(NSEQ,FDBE,LDBE,ITY,IPOS,LM,CSQ1,IDM1,IST,IRC)
   10   Call RETSQ
     *    (NPTR,NSEQ,NREF,FDBE,LDBE,ITY,IPOS,LM,CSQ1,IDM1,IST,CC,IRC)
           CB='B'
           CTRC='D'
           Call STREV(CSQ1,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  

           Do  15 I1=1,LM
              CSQ2(I1)=CSQ1(I1)
   15      Continue

           CC='U'
           CSTR='-'
           Call STREV(CSQ2,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  

           Do  90 I1=1,KSEG
              KOCC=0
 
* Plus strand 

              Do  20 I2=0,LM-ISEG(I1)
                 Do  19 I3=1,ISEG(I1)
                    If(CSEG(I1)(I3:I3).NE.CSQ1(I2+I3)) Go to  20
   19            Continue
                 KOCC=KOCC+1  
                 COCC(KOCC)='+'
                 IOCC(KOCC)=I2+1
   20         Continue         

* Minus strand 

              Do  30 I2=0,LM-ISEG(I1)
                 Do  29 I3=1,ISEG(I1)
                    If(CSEG(I1)(I3:I3).NE.CSQ2(I2+I3)) Go to  30
   29            Continue
                 KOCC=KOCC+1  
                 COCC(KOCC)='-'
                 IOCC(KOCC)=LDBE-I2
   30         Continue         

* Print sequence positions

                 Print *,' '
                 Print *,CSEG(I1)( 1:ISEG(I1)),':'
                 Print *,' '
              If(KOCC.GE.1) then
                 Do  50 I2=1,KOCC
                    Write(6,"('     ',A16,A1,I8)")
     *                 FDBE,COCC(I2),IOCC(I2)
   50            Continue
              Else
                 Print *,'     Not found' 
              End if

   90      Continue

        Stop
        End

        Include           'RETSQ.f' 
        Include           'STREV.f' 
	Include           'LOCSQ.f'
	Include           'PSQID.f'
