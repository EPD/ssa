#include <stdlib.h>
#include <curses.h>
#include <term.h>

void
getlwc_ (int *LPL, int *LPW )
{
  char	buf[1024];
  char	*term;

  if ((term = getenv("TERM")) != 0 && tgetent(buf, term) > 0) {
    *LPL = tgetnum("li");
    *LPW = tgetnum("co");
  }
}
