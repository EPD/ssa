C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : CALCULATION OF MEAN AND VARIANCE                     
C       ****************************************************************
                                                                        
        SUBROUTINE MEVAR(ISFS,IDM1,I,XM,XV)                             
                                                                        
        DIMENSION ISFS(IDM1)                                            
                                                                        
        XSUM=0                                                          
        DO 5 I1=1,I                                                     
           XSUM=XSUM+ISFS(I1)                                           
    5   CONTINUE                                                        
        XM=XSUM/I                                                       
        XSUM=0                                                          
        DO 10 I1=1,I                                                    
           XSUM=XSUM+(XM-ISFS(I1))**2                                   
   10   CONTINUE                                                        
        XV=XSUM/I                                                       
                                                                        
        RETURN                                                          
                                                                        
        END                                                             
