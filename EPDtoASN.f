* Program EPDtoASN.f 
*
* This program converts EPD tape release format into 
* NCBI transcription initiation site feature asn1 format.
*
* Compile: f77 EPDtoASN.f -o EPDtoASN
* Run:     EPDtoASN < epd.dat > epd.asn
*
* Written by Philipp Bucher, August  1991
* 
*----------------------------------------------------------------------*
* Data 
*----------------------------------------------------------------------*

* Work fields 

	Character*80      RCIN
	Character*80      ROUT
	Character*80      STRI 
        Logical           ONOF

* id general

	Character*05      ECOD

* txinit

        Character*20      PNAM 
        Character*64      EXPR
        Character*20      TXDE
	Character*12      TYPE

* tx-evidence 

        Integer           KEVI
        Integer           IECO(16)
	Integer           IESY(16)
        Logical           LELP(16)
	Logical           LEFH(16)

	Character*16      CECO(0:255)
	Data              CECO(  0)/'unknown'/
	Data              CECO(  1)/'rna-seq'/
	Data              CECO(  2)/'rna-size'/
	Data              CECO(  3)/'np-map'/
	Data              CECO(  4)/'np-size'/
	Data              CECO(  5)/'pe-seq'/
	Data              CECO(  6)/'cDNA-seq'/
	Data              CECO(  7)/'pe-map'/
	Data              CECO(  8)/'pe-size'/
	Data              CECO(  9)/'pseudo-seq'/
	Data              CECO( 10)/'rev-pe-map'/
	Data              CECO(255)/'other'/
       
	Character*16      CESY(0:255)
	Data              CESY(  0)/'unknown'/
	Data              CESY(  1)/'physiological'/
	Data              CESY(  2)/'in-vitro'/
	Data              CESY(  3)/'oocyte'/
	Data              CESY(  4)/'transfection'/
	Data              CESY(  5)/'transgenic'/
	Data              CESY(255)/'other'/

* location

	Integer*8         IPOS
	Character*10      CPOS
	Character*05      CSTR
	Character*10      EMID 
	Character*02      EMRL

* ext

        Character*03      HOMG
	Logical           LISS
	Character*03      TXUN
	Character*01      ININ

* cit pub

	Integer           KREF
	Character*08      CJTA(4)
	Character*08      CVOL(4)
        Character*08      CPAG(4)

*----------------------------------------------------------------------*
* Input Section
*----------------------------------------------------------------------*

* Start reading input

    1   Read(5,"(A)",End= 99) RCIN

* embl release (from title line)

        If(RCIN( 1: 2).EQ.'TI') then
	   EMRL=RCIN( 9:10)
        Else
           Go to   1
        End if

* Write header 

        Print *,'Seq-annot ::= {'
        Print *,'    db {'
        Print *,'        db "EPD" ,'
        Write(6,'(''        tag str "release '',A2,''" } ,'')') EMRL
        Print *,'    data ftable {'

        ONOF=.TRUE.
     
    2   Read(5,"(A)",End= 99) RCIN

* txdescr (from classification line)

        If(RCIN( 1: 5).EQ.'*    ') then
	   IC1=Index(RCIN,'.')+1
           If(RCIN(IC1:IC1).EQ.' ') then
	      IC1=IC1+1
	      IC2=IC1+Index(RCIN(IC1:IC1+19),' ')-1
	      TXDE=RCIN(IC1:IC2)
           End if
        End if

* name, inittype, location, ext (from FP line)

	If(RCIN( 1: 2).EQ.'FP') then
	   If(.NOT.ONOF) Print *,'   ,' 

	   PNAM=RCIN(6:25)

	   If(RCIN(27:27).EQ.'+') then
              LISS=.TRUE.
           Else
	      LISS=.FALSE.
           End if 

	   If     (RCIN(28:28).EQ.'S') then
	      TYPE='single'
           Else if(RCIN(28:28).EQ.'M') then
	      TYPE='multiple'
           Else if(RCIN(28:28).EQ.'R') then
	      TYPE='region'
           Else
	      TYPE='unknown'
           End if

           EMID=RCIN(34:51)

	   If(RCIN(53:53).EQ.'+') then
	      CSTR='plus'
           Else
	      CSTR='minus'
           End if

           Read(RCIN(54:63),"(I10)") IPOS
	   IPOS=IPOS-1
	   Write(CPOS,"(I10)") IPOS
	   ECOD=RCIN(66:70)
	   HOMG=RCIN(72:74) 
	      If(HOMG( 1: 1).EQ.'0') then 
		 HOMG( 1: 1)=HOMG( 2: 2)
		 HOMG( 2: 2)=HOMG( 3: 3)
		 HOMG( 3: 3)=' '
              End if 
	      If(HOMG( 1: 1).EQ.'0') then 
		 HOMG( 1: 1)=HOMG( 2: 2)
		 HOMG( 2: 2)=HOMG( 3: 3)
		 HOMG( 3: 3)=' '
              End if 
           TXUN=RCIN(76:78)
	      If(TXUN( 1: 1).EQ.'0') then 
		 TXUN( 1: 1)=TXUN( 2: 2)
		 TXUN( 2: 2)=TXUN( 3: 3)
		 TXUN( 3: 3)=' '
              End if 
	      If(TXUN( 1: 1).EQ.'0') then 
		 TXUN( 1: 1)=TXUN( 2: 2)
		 TXUN( 2: 2)=TXUN( 3: 3)
		 TXUN( 3: 3)=' '
              End if 
	   ININ=RCIN(80:80)

           ONOF=.TRUE.

        End if

* evidence (from 1st DO line)

        If(RCIN( 1:15).EQ.'DO        Exper') then
           L=Lblnk(RCIN)
           STRI=RCIN(34: L)
           Call ParseExp(STRI,KEVI,IECO,IESY,LELP,LEFH)
        End if
        
* expression/regulation (from 2nd DO line) 

        If(RCIN( 1:15).EQ.'DO        Expre') then
	   L=Lblnk(RCIN) 
           EXPR=(RCIN(34: L))
        End if

* citations (from RF line)

	If(RCIN( 1: 2).EQ.'RF') then
           L=Lblnk(RCIN)
           STRI=RCIN(11: L)
           Call ParseRef(STRI,KREF,CJTA,CVOL,CPAG)
        End if

	If(RCIN( 1: 2).NE.'//') go to   2 
        If(.NOT.ONOF)           go to   2

*----------------------------------------------------------------------*
* Output Section
*----------------------------------------------------------------------*

        Print *,'{'

* id general

        Print *,'    id general {'
        Print *,'        db "EPD" ,'
           ROUT( 1:17)='        tag str "'
           ROUT(18:22)=ECOD
           ROUT(23:27)='" } ,'
        Print *,ROUT( 1:27)

* data txinit

        Print *,'        data txinit {'
           ROUT( 1:14)='        name "'
           ROUT(15:34)=PNAM
           IC1=Lblnk(ROUT( 1:34))+1
           IC2=IC1+2
           ROUT(IC1:IC2)='" ,'
        Print *,ROUT(1:IC2)
           ROUT( 1:20)='        expression "'
           L=Lblnk(EXPR)
           IC1=20+L
           ROUT(21:IC1)=EXPR( 1: L)
           IC2=IC1+3
           ROUT(IC1+1:IC2)='" ,'
        Print *,ROUT(1:IC2)
        Print *,'        txsystem pol2 ,'
           ROUT( 1:17)='        txdescr "'
           L=Lblnk(TXDE)
           IC1=17+L 
           ROUT(18:IC1)=TXDE( 1: L)
           IC2=IC1+3
           ROUT(IC1+1:IC2)='" ,'
        Print *,ROUT(1:IC2)
           ROUT( 1:17)='        inittype '
           L=Lblnk(TYPE)
           IC1=17+L 
           ROUT(18:IC1)=TYPE( 1: L)
           IC2=IC1+2
           ROUT(IC1+1:IC2)=' ,'
        Print *,ROUT(1:IC2)

* evidence

        Print *,'        evidence {'

        Do  59 I1=1,KEVI

        J1=1
        If(IESY(I1).NE.1) J1=J1+1
        If(LELP(I1)) J1=J1+1
        If(LEFH(I1)) J1=J1+1
        K1=1

        Print *,'                {'
           ROUT( 1:33)='                        exp-code '
           L=Lblnk(CECO(IECO(I1)))
           IC1=33+L 
           ROUT(34:IC1)=CECO(IECO(I1))( 1: L)
           If(K1.EQ.J1) then
              If(I1.EQ.KEVI) then 
                 IC2=IC1+8
	         ROUT(IC1+1:IC2)=' } } } ,'
              Else 
                 IC2=IC1+4
                 ROUT(IC1+1:IC2)=' } ,'
              End if
           Else
              IC2=IC1+2
              ROUT(IC1+1:IC2)=' ,'
           End if
        Print *,ROUT(1:IC2)

        If(IESY(I1).NE.1) then
           K1=K1+1
           ROUT( 1:42)='                        expression-system '
           L=Lblnk(CESY(IESY(I1)))
           IC1=42+L 
           ROUT(43:IC1)=CESY(IESY(I1))( 1: L)
           If(K1.EQ.J1) then
	      If(I1.EQ.KEVI) then 
                 IC2=IC1+8
	         ROUT(IC1+1:IC2)=' } } } ,'
	      Else 
                 IC2=IC1+4
	         ROUT(IC1+1:IC2)=' } ,'
              End if
           Else
              IC2=IC1+4
              ROUT(IC1+1:IC2)=' ,'
           End if
        Print *,ROUT(1:IC2)
        End if

        If(LELP(I1)) then
           K1=K1+1
           If(K1.EQ.J1) then
              If(I1.EQ.KEVI) then  
                 Print *,
     *        '                        low-prec-data TRUE } } } ,'
              Else
	         Print *,
     *        '                        low-prec-data TRUE } ,'
              End if 
           Else
	         Print *,
     *        '                        low-prec-data TRUE ,'
           End if
        End if

        If(LEFH(I1)) then
           K1=K1+1
           If(K1.EQ.J1) then
              If(I1.EQ.KEVI) then   
                 Print *,
     *        '                        from-homolog TRUE } } } ,'
              Else
	         Print *,
     *        '                        from-homolog TRUE } ,'
              End if
           Else
                 Print *,
     *        '                        from-homolog TRUE ,'
           End if
        End if

   59   Continue  

* location 

        Print *,'        location pnt {'
           ROUT( 1:16)='           point'
           ROUT(17:26)=CPOS
           ROUT(27:28)=' ,'
        Print *,ROUT( 1:28)
           ROUT( 1:18)='           strand '
	   L=Lblnk(CSTR) 
	   IC1=18+L
	   ROUT(19:IC1)=CSTR( 1: L)
	   IC2=IC1+2
	   ROUT(IC1+1:IC2)=' ,'
        Print *,ROUT(1:IC2)
        Print *,'           id embl {'
           ROUT( 1:17)= '           name "'
	   L=Lblnk(EMID)
	   IC1=17+L
	   ROUT(18:IC1)=EMID( 1: L)
	   IC2=IC1+3
	   ROUT(IC1+1:IC2)='" ,' 
        Print *,ROUT(1:IC2)
	   ROUT( 1:20)='           release "'
	   ROUT(21:22)=EMRL
	   ROUT(23:29)='" } } ,'
        Print *,ROUT( 1:29)

* EPD extensions

        Print *,'    ext {'
        Print *,'        class "EPD" ,'
        Print *,'        type str "Txinit extension" ,'
        Print *,'        data {'

	If(HOMG.NE.'   ') then
        Print *,'            {'
        Print *,'                label str "homgroup" ,'
	   ROUT( 1:25)='                data int '
	   L=Lblnk(HOMG)
	   IC1=25+L
	   ROUT(26:IC1)=HOMG( 1: L)
	   IC2=IC1+4
	   ROUT(IC1+1:IC2)=' } ,'
        Print *,ROUT(1:IC2)
	End if
       
        Print *,'            {'
        Print *,'                label str "iss" ,'
	If     (LISS.AND.TXUN.EQ.'   ') then
           Print *,'                data bool TRUE } } } ,'
        Else if(LISS.AND.TXUN.NE.'   ') then
           Print *,'                data bool TRUE } ,'
        Else if(.NOT.LISS.AND.TXUN.EQ.'   ') then
           Print *,'                data bool FALSE } } } ,'
        Else if(.NOT.LISS.AND.TXUN.NE.'   ') then
           Print *,'                data bool FALSE } ,'
        End if

	If(TXUN.NE.'   ') then 
        Print *,'            {'
        Print *,'                label str "txunitnum" ,'
	   ROUT( 1:25)='                data int '
	   L=Lblnk(TXUN)
	   IC1=25+L
	   ROUT(26:IC1)=TXUN( 1: L)
	   IC2=IC1+4
	   ROUT(IC1+1:IC2)=' } ,'
        Print *,ROUT(1:IC2)
        Print *,'            {'
       
        Print *,'                label str "initnum" ,'
           ROUT='                data int   } } } ,'
           ROUT(26:26)=ININ
        Print *,ROUT( 1:34)
	End if

* citations 

        Print *,'    cit pub {'

	Do  69 I1=1,KREF
        Print *,'        gen {'
           ROUT( 1:27)='            journal { jta "'
           L=Lblnk(CJTA(I1))
           IC1=27+L
           ROUT(28:IC1)=CJTA(I1)( 1: L)
           IC2=IC1+5
           ROUT(IC1+1:IC2)='" } ,'
        Print *,ROUT(1:IC2)
           ROUT( 1:20)='            volume "'
           L=Lblnk(CVOL(I1))
           IC1=20+L
           ROUT(21:IC1)=CVOL(I1)( 1: L)
           IC2=IC1+3
           ROUT(IC1+1:IC2)='" ,'
        Print *,ROUT(1:IC2)
           ROUT( 1:19)='            pages "'
           L=Lblnk(CPAG(I1))
           IC1=19+L
           ROUT(20:IC1)=CPAG(I1)( 1: L)
           If(I1.NE.KREF) then
              IC2=IC1+5
              ROUT(IC1+1:IC2)='" } ,'
           Else
              IC2=IC1+7
              ROUT(IC1+1:IC2)='" } } }'
           End if
           Print *,ROUT(1:IC2)
   69   Continue

        ONOF=.FALSE.

	Go to   2

   99   Continue

* Last line 

	Print *,'}   }'

  100   Stop
	End

*----------------------------------------------------------------------*
	 
        Subroutine ParseExp(STRI,KEVI,IECO,IESY,LELP,LEFH)

        Character*(*)     STRI 
        Integer           KEVI
        Integer           IECO(*)
        Integer           IESY(*)
        Logical           LELP(*)
        Logical           LEFH(*)

        Logical           LELPL
        Logical           LEFHL 

        L=Lblnk(STRI)

           KEVI=1
           IESY(1)=1
           LELPL=.FALSE.
           LEFHL=.FALSE.
        Do  49 I1=1,L
           If     (STRI(I1-1:I1).EQ.'10') then
              IECO(KEVI)=10
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'0') then
              IECO(KEVI)=0
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'1') then
              IECO(KEVI)=1
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'2') then 
              IECO(KEVI)=2
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'3') then 
              IECO(KEVI)=3
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'4') then 
              IECO(KEVI)=4
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'5') then 
              IECO(KEVI)=5
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'6') then 
              IECO(KEVI)=6
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'7') then 
              IECO(KEVI)=7
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'8') then 
              IECO(KEVI)=8
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'9') then 
              IECO(KEVI)=9
              LELP(KEVI)=LELPL
              LEFH(KEVI)=LEFHL
           Else if(STRI(I1:I1).EQ.'*') then 
              IESY(KEVI)=2
           Else if(STRI(I1:I1).EQ.'o') then 
              IESY(KEVI)=3
           Else if(STRI(I1:I1).EQ.'#') then 
              IESY(KEVI)=4
           Else if(STRI(I1:I1).EQ.'!') then 
              IESY(KEVI)=5
           Else if(STRI(I1:I1).EQ.'<') then 
              LELPL=.TRUE.
           Else if(STRI(I1:I1).EQ.'>') then 
              LELPL=.FALSE.
           Else if(STRI(I1:I1).EQ.'(') then 
              LEFHL=.TRUE.
           Else if(STRI(I1:I1).EQ.')') then 
              LEFHL=.FALSE.
           Else if(STRI(I1:I1).EQ.',') then 
              KEVI=KEVI+1
              IESY(KEVI)=1
           End if
   49   Continue
 
	Return 
        End

*----------------------------------------------------------------------*

        Subroutine ParseRef(STRI,KREF,CJTA,CVOL,CPAG)

        Character*(*)     STRI   
	Integer           KREF
	Character*08      CJTA(*)
	Character*08      CVOL(*)
        Character*08      CPAG(*)

        IC1=1
	Do  9 I1=1,4
           If(STRI(IC1:IC1+14).EQ.' ') go to  10 
           Do   4 I2=IC1,IC1+14
	      If(Index('0123456789',STRI(I2:I2)).NE.0)
     *           go to   5
    4      Continue 
    5      Continue

	   IC2=I2-1
	   IC3=IC1+Index(STRI(IC1:IC1+14),':')-1
	   IC4=IC1+Lblnk(STRI(IC1:IC1+14))-1

           CJTA(I1)=STRI(IC1  :IC2  )
	   CVOL(I1)=STRI(IC2+1:IC3-1)
	   CPAG(I1)=STRI(IC3+1:IC4  )

           IC1=IC1+15
    9   Continue
   10   Continue

	KREF=I1-1

        Return
        End

