C       ****************************************************************
C       AUTHOR   : PHILIPP BUCHER                                       
C       FUNCTION : INPUT OF A COLUMN PORTION OF A DNA SEQUENCE MATRIX   
C       ****************************************************************
                                                                        
	SUBROUTINE
     *     CEDSM(NLSQ,NFPS,NPTR,NSEQ,NREF,CSEQ,IDM1,MI,LM,MB1,CC,IRC)

        Implicit          Character*01 (C)
     
        Character*80      RC80
	Character         CSEQ(IDM1)
C        Character*01      CSEQ(*)
        Character*22      FDBE
        Character*03      CIRC

	Integer*8         IPOS
	Integer*8         LDBE 
	Integer*8         LM

        
        MI=0
        IST=1
        N1=1
        CTRC='D'
        CB='N'
        MB2=MB1+LM-1

        NERR=0

        Write(NERR,*,Err=901) ' '
        
* - Extract sequence segment from nucleotide sequence database  

    1   Read(NFPS,"(A)",Err=901,End= 10) RC80
           If(RC80( 1: 2).EQ.'YZ') go to  10 
           If(RC80( 1: 2).NE.'FP') go to   1 
        
        IRC=1
        Read(RC80,"(30x,A21,I1,A1,I10)",Err=  9) FDBE,ITY,CSTR,IPOS

        If(CSTR.EQ.'+') IPOS=IPOS+MB1           
        If(CSTR.EQ.'-') IPOS=IPOS-MB2
 
C       Call RETSQ
C    *     (NDBF,          FDBE,LDBE,ITY,IPOS,LM,CSEQ,IDM1,IST,   IRC)
	Call RETSQ
     *     (NPTR,NSEQ,NREF,FDBE,LDBE,ITY,IPOS,LM,CSEQ,IDM1,IST,CC,IRC)
        If(IRC.NE.0) then
           IRC=IRC+100
           Go to  9 
        End if
C	Write (*,*) IST,LM
C	Write(*,'((60A))',Err=905)(CSEQ(ii1),ii1=1,LM)
C	Write(*,'((60A))',Err=905)(CSEQ(ii1),ii1=IST,IST+LM-1)
         
        Call STREV(CSEQ,IDM1,IST,LM,CTRC,CB,CSTR,CC,IRC)                  
        If(IRC.NE.0) then
           IRC=IRC+200
           Go to  9 
        End if

        IRC=0
        MI=MI+1
        IST=IST+LM

* - Print return code  
 
    9   Write(CIRC,"(I3)",Err=904) IRC
        Write(NERR,*,Err=901) RC80(6:62),' : RC=',CIRC
        IRC=0
        Go to   1 

   10   Continue

* Done

  100   Return 

* Error handling: 

  905   IRC=IRC+1
  904   IRC=IRC+1
  903   IRC=IRC+1
  902   IRC=IRC+1
  901   IRC=IRC+1
        Go to 100

        End
*----------------------------------------------------------------------*
