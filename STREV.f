*$      Subroutine STREV   : Created 06/11/88 ; last update 06/01/88   *
*       *--------------------------------------------------------------*
*       Author    : Philipp Bucher                                       
*       Function  : Translation of nucleotide sequence into standard
*                   alphabet, optionally combined with replacement
*                   by complementary strand
*       Arguments : CSEQ (RW) : Character array containing nucleotide 
*                               sequence
*                   IDM1 (RR) : Dimension of CSEQ
*                   IST  (RR) : Starting position of sequence in CSEQ
*                   L    (RR) : Length of sequence
*                   CTRC (RR) : Translation code
*                   CB   (RR) : Character for unknown nucleotide
*                   CSTR (RR) : Requested DNA strand (+ or -)
*                   CC   (RR) : Input character case (U or L)
*                   IRC  (WO) : Return code            
*       *--------------------------------------------------------------*
                                                                        
        Subroutine     STREV(CSEQ,IDM1,IST,L,CTRC,CB,CSTR,CC,IRC)             
                                                                        
        Implicit       Character*01(C)
                                                                        
        Character      CSEQ(IDM1)                                            

        Character*32   CHIN
        Character*32   CHOUT

        Integer*8         L

        Character*10   CBIN 
        Byte           BBIN(10)
        Equivalence   (CBIN,BBIN)
        Data           BBIN/1,2,3,4,4,4,3,2,1,1/
        
 
        IRC=0                                                           

        If(IST.LT.1.OR.IST+L-1.GT.IDM1) go to 901                               

* Define input alphabet

        If(CC.EQ.'L') GO TO 5
        If(CC.NE.'U') GO TO 902

        If     (Index('DRB',CTRC).NE.0) then
           CHIN=' ACGTU                  '       
           ICH=6
        Else if(CTRC.EQ.'F') then
           CHIN=' ACGTURYSWAKBDHV        '     
           ICH=16
        Else if(CTRC.EQ.'P') then
           CHIN=' ABCDEFGHIKLMNPQRSTVWXYZ'     
           ICH=24
        Else 
           Go to 903
        End if
        Go to  10

   5   If      (Index('DRB',CTRC).NE.0) then
           CHIN=' acgtu                  '       
           ICH=6
       Else if(CTRC.EQ.'F') then
           CHIN=' acgturyswakbdhv        '     
           ICH=16
       Else if(CTRC.EQ.'P') then
           CHIN=' abcdefghiklmnpqrstvwxyz'     
           ICH=24
       Else
          Go to 903
       End if

* Define output alphabet

  10    CHOUT(1:1)=CB
        If(CSTR.EQ.'+') then 
           If(CTRC.EQ.'D') CHOUT(2:24)='ACGTT                  '
           If(CTRC.EQ.'R') CHOUT(2:24)='ACGUU                  '
           If(CTRC.EQ.'F') CHOUT(2:24)='ACGTTRYSWAKBDHV        '
           If(CTRC.EQ.'P') CHOUT(2:24)='ABCDEFGHIKLMNPQRSTVWXYZ'  
           If(CTRC.EQ.'B') CHOUT(2:24)=CBIN( 1: 5) 
           Go to 20
        End if
        If(CSTR.EQ.'-') then 
           If(CTRC.EQ.'D') CHOUT(2:24)='TGCAA                  '
           If(CTRC.EQ.'R') CHOUT(2:24)='UGCAA                  '
           If(CTRC.EQ.'F') CHOUT(2:24)='TGCAAYRSWKAVHDB        '
           If(CTRC.EQ.'P') CHOUT(2:24)='                       '  
           If(CTRC.EQ.'B') CHOUT(2:24)=CBIN( 6:10) 
           Go to 50
        End if 
        Go to 904

* ( + Strand )

   20   Do 30 I1=IST,IST+L-1    
           Do 25 I2=1,ICH 
              If(CSEQ(I1).EQ.CHIN(I2:I2)) go to 29
   25      Continue
           CSEQ(I1)=CB
           Go to 30

   29      CSEQ(I1)=CHOUT(I2:I2)             
   30   Continue
        Go to 100

* ( - Strand )
                                            
   50      J1=IST+L                                                   
        Do 80 I1=IST,IST+(L+1)/2-1                                      
           J1=J1-1
           CH=CSEQ(I1)
           Do 65 I2=1,ICH       
              If(CSEQ(J1).EQ.CHIN(I2:I2)) go to 69
   65      Continue
           CSEQ(I1)=CB
           Go to 70

   69      CSEQ(I1)=CHOUT(I2:I2) 

   70      Do 75 I2=1,ICH      
              If(CH.EQ.CHIN(I2:I2)) go to 79
   75      Continue
           CSEQ(J1)=CB
           Go to 80

   79      CSEQ(J1)=CHOUT(I2:I2) 
   80      Continue

  100   Return                                                          
                                                                        
  904   IRC=IRC+1                                  
  903   IRC=IRC+1                                  
  902   IRC=IRC+1
  901   IRC=IRC+1                                                            

        End                                                             
