#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>


#define MAXLUN  50
static  FILE *file[MAXLUN];
static  int filedes[MAXLUN];
static  int open_flag[MAXLUN];
static  char filename[250][MAXLUN];


void copen_(lun, FileName, mode, status, FileLen)
     int *lun;
     char *FileName;
     int *status;
     char *mode;
     int FileLen;
{
  char errmsg[800];
  char fname[512];
  int len1;
  if(*lun < 0 || *lun > MAXLUN-1) {
    printf("copen: invalid file unit %d\n", *lun);
    *status = -1;
    return;
  }
  open_flag[*lun] = 0;
  if(( strlen(FileName) == 0 ) || ((len1=strcspn(FileName," ")) == 0 )) {
    printf("copen: File Name %s is wrong\n", FileName);
    *status = 2;
    return;
  }
  strncpy(fname, FileName, len1);
  fname[len1] = '\0';
  if( (file[*lun] = fopen(fname,mode)) == NULL) {
    sprintf(errmsg, "copen: error opening file %s, unit %d ", fname, *lun);
    perror(errmsg);
    *status = 2;
    return;
  } else {
    *status = 0;
  }
  if( (filedes[*lun] = open(fname,O_RDONLY)) == -1) {
    perror("copen");
    *status = 2;
    return;
  } else {
    *status = 0;
  }
  open_flag[*lun] = 1;
  strcpy(filename[*lun], fname);
}

void cclose_(lun,status)
     int *lun;
     int *status;
{
  if(*lun < 0 || *lun > MAXLUN-1) {
    printf("cclose: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
    *status = -1;
    return;
  }
  if(open_flag[*lun] == 1) {
    if(fclose(file[*lun]) == 0) {
       *status = 0;
    } else {
      perror("cclose");
      *status=2;
      return;
    }
    if(close(filedes[*lun]) == 0) {
       *status = 0;
    } else {
      perror("cclose");
      *status=2;
      return;
    }
  } else {
    printf("cclose: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
    *status=3;
    return;
  }
  open_flag[*lun] = 0;
  strcpy(filename[*lun], "");
}

void cgetbuf_(lun, buffer, lsize, buflen, eof, status)
  int   *lun;
  char *buffer;
  size_t  *lsize;
  size_t  *buflen;
  int   *eof;
  int   *status;
{
   int lenbufl;
   char errmsg[132];
   if(*lun < 0 || *lun > MAXLUN-1) {
     printf("cgetbuf: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
     *status = -1;
     return;
   }
   /*fprintf(stderr,"cgetbuf: buff size %d\n",(*buflen) * (*lsize));*/
   memset(buffer, 0, (*buflen) * (*lsize));
   lenbufl = fread(buffer,*lsize,*buflen,file[*lun]);
   *eof = feof(file[*lun]);
   /*
   for (i=0; i<(*buflen) * (*lsize);i++) {
   	fprintf(stderr,"%c", buffer[i]);
	}
   fprintf(stderr,"%c", '\n');
   */
   if( (lenbufl == 0) && (*eof == 0) )  {
     sprintf(errmsg,"cgetbuf: error reading file unit %d (file name %s)\n",*lun, filename[*lun]);
     perror(errmsg);
     *status=2;
   } else
     *status=0;
}

void cgetline_(lun, buffer, buflen, status)
  int   *lun;
  char *buffer;
  size_t  *buflen;
  int   *status;
{
   char errmsg[132];
   int BUF_SIZE = 1024;
   char *tmpbuff = NULL;
   char *res;
   int wlen;
   /*
   char tbuff[512];
   unsigned int blen = *buflen; */
   size_t blen = *buflen;

   /*printf ("cgetline_: file unit = %d , name = %s , buflen = %u\n", *lun, filename[*lun],(unsigned int)blen); */

   if(*lun < 0 || *lun > MAXLUN-1) {
     printf("cgetline_: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
     *status = -1;
     return;
   }
   tmpbuff = (char *) calloc((blen + 2),sizeof(char));
   if (tmpbuff == NULL) {
     sprintf(errmsg,"cgetline_: error allocating memory (len=%u)\n",(unsigned int)(blen + 2));
     perror(errmsg);
     *status=1;
     return;
   }
   /*
   memset(tbuff, 0, strlen(tbuff));
   if ((res=fgets(tbuff,BUF_SIZE,file[*lun])) == NULL) {
   */
   if ((res=fgets(tmpbuff,BUF_SIZE,file[*lun])) == NULL) {
     sprintf(errmsg,"cgetline_: error reading file unit %d (file name %s)",*lun, filename[*lun]);
     perror(errmsg);
     *status=2;
     return;
   }
   size_t cLen = strlen(res);
   if (cLen > blen) {
     fprintf(stderr,"cgetline_: error reading line %s (file name %s): actual size (%u bytes) exceeds %u bytes\n", res, filename[*lun], (unsigned int)cLen, (unsigned int)blen);
     exit(1);
   }
   wlen = strcspn(tmpbuff,"\n");
   strncpy(buffer, tmpbuff, wlen);
   while (wlen < blen) {
     buffer[wlen++] = ' ';
   }
   free(tmpbuff);
   /*
   wlen = strcspn(tbuff,"\n");
   printf("cgetline_: wlen = %u\n", (unsigned int)wlen);
   printf("cgetline_: buflen = %u\n", (unsigned int)blen);
   strncpy(buffer, tbuff, wlen);
   while (wlen < blen) {
     buffer[wlen++] = ' ';
   }
   */
   *status=0;
}

void cfseek_(lun, offset, whence, status)
	int *lun;
	off_t *offset;
	int *whence;
	int *status;
{
  off_t ret_val;

  if(*lun < 0 || *lun > MAXLUN-1) {
    printf("cfseek: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
    *status = -1;
    return;
  }
  if ((ret_val=fseeko(file[*lun], (off_t)*offset, *whence)) == -1) {
    perror("\ncfseek");
    *status = -2;
    return;
  }
  *status = 0;
  return;
}

void cftell_(lun, offset, status)
	int *lun;
	off_t *offset;
	int *status;
{
   if(*lun < 0 || *lun > MAXLUN-1) {
    printf("cftell: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
    *status = -1;
    return;
  }
  if ((*offset=ftello(file[*lun])) == -1) {
    perror("\ncftell");
    *status = -2;
    return;
  }
  *status = 0;
  return;
}

void cfstat_(lun, statb, status)
	int *lun;
	unsigned long long *statb;
	int *status;
{
  struct stat st;
  if(*lun < 0 || *lun > MAXLUN-1) {
    printf("cfstat: invalid file unit %d (file name %s)\n", *lun, filename[*lun]);
    *status = -1;
    return;
  }
  if (fstat(filedes[*lun], &st) == -1) {
    perror("\ncfstat");
    *status = -2;
    return;
  }
  *statb = st.st_size;
  *status = 0;
  return;
}
